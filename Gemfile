source 'https://rubygems.org'
ruby '2.3.1'

gem 'dotenv-rails', :groups => [:development, :test]
gem 'dotenv', :groups => [:development, :test]

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '~> 5.0.0', '>= 5.0.0.1'

# Use postgres as the database for Active Record
gem 'pg'

# Use Puma as the app server
gem 'puma', '~> 3.0'
gem 'puma_worker_killer'

# Error tracking
gem 'sentry-raven'

# Authentication
gem 'devise'
gem 'doorkeeper'
gem 'doorkeeper-jwt'

# Sorting
gem 'acts_as_list'

# AWS
gem 'aws-sdk', '~> 2.0'

# Permissions
# gem 'cancancan', '~> 1.10'

# API
gem 'grape', '~> 0.18.0'
gem 'grape_logging', '~> 1.3.0'
gem 'wine_bouncer', '~> 1.0.1'
gem 'active_model_serializers', '~> 0.10.2'
gem 'grape-active_model_serializers', '~> 1.5.0'

# Background Jobs
gem 'sidekiq'

# Disable validations in favor of Grape
gem 'hashie-forbidden_attributes'

# Use Rack CORS for handling Cross-Origin Resource Sharing (CORS), making cross-origin AJAX possible
gem 'rack-cors', require: 'rack/cors'

# Validators
gem 'validates_email_format_of', '~> 1.6.3'

# Monitoring
gem 'newrelic_rpm'
gem 'tunemygc'

# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
# gem 'jbuilder', '~> 2.5'
# Use Redis adapter to run Action Cable in production
# gem 'redis', '~> 3.0'
# Use ActiveModel has_secure_password
# gem 'bcrypt', '~> 3.1.7'

# Use Capistrano for deployment
# gem 'capistrano-rails', group: :development

group :development do
  gem 'rails-erd'
  gem 'listen', '~> 3.0.5'
  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'
  gem 'letter_opener'
  gem 'guard-rspec', require: false
  gem 'derailed_benchmarks'
  gem 'stackprof'
end

group :development, :test do
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'byebug', platform: :mri
  gem 'rspec-rails'
end

group :test do
  gem 'database_cleaner'
  gem 'shoulda-matchers'
  gem 'factory_girl_rails'
  gem 'json_spec'
  gem 'vcr'
  gem 'webmock', require: 'webmock/rspec'
  gem 'timecop'
  gem 'simplecov', require: false
  gem 'reqres_rspec'
end

group :production, :staging do
  gem 'rails_12factor' # Heroku Deployments
end

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw, :jruby]
