# This file is used by Rack-based servers to start the application.
require 'rack'
require 'rack/cors'

use Rack::Cors
require_relative 'config/environment'

run Rails.application
