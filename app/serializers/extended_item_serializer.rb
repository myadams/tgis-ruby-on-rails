class ExtendedItemSerializer < ItemSerializer
  has_many :events
  belongs_to :applicable_style, key: :style
end
