class ItemSerializer < ActiveModel::Serializer
  belongs_to :applicable_style, key: :style
  attributes :id, :name, :active, :category_id, :is_deleted,
    :calendar_id, :applicable_style_id, :style_id, :active, :locked,
    :use_custom_styles, :snapshot_id, :interrupted_by_holidays,
    :interrupted_by_weekends
end
