class ExtendedCategorySerializer < CategorySerializer
  has_many :items, serializer: ExtendedItemSerializer
  has_many :categories, serializer: ExtendedCategorySerializer
end
