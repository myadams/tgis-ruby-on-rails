class OrganizationSerializer < ActiveModel::Serializer
  attributes :id, :name, :uses_granular_permissions, :color, :subdomain, :is_deleted
end
