class StyleSerializer < ActiveModel::Serializer
  attributes :id, :fill_color, :outline_color, :font,
    :font_style, :font_size, :font_weight, :text_alignment,
    :text_color, :text_decoration
end
