class ReportSerializer < ActiveModel::Serializer
  belongs_to :organization
  attributes :id, :name, :is_deleted, :organization_id
end
