class ExtendedSnapshotSerializer < SnapshotSerializer
  belongs_to :calendar
  has_many :styles
  has_many :events
  has_many :items
  has_many :categories
  has_many :holidays
end
