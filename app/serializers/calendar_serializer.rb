class CalendarSerializer < ActiveModel::Serializer
  belongs_to :applicable_style, key: :style
  attributes :id, :name, :is_deleted, :interrupted_by_holidays,
    :interrupted_by_weekends, :organization, :applicable_style_id,
    :style_id, :pending
end
