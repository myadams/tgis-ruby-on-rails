class GranularPermissionSerializer < ActiveModel::Serializer
  belongs_to :invitation
  belongs_to :calendar
  attributes :id, :read, :write, :calendar_id
end
