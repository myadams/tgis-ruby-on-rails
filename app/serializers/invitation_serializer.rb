class InvitationSerializer < ActiveModel::Serializer
  belongs_to :organization
  attributes :id, :name, :email, :accepted, :user_id, :organization_id,
    :is_deleted, :access_level, :updated_at
end
