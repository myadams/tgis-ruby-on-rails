class ExtendedUserSerializer < ActiveModel::Serializer
  attributes :id, :email
  has_many :granular_permissions
end
