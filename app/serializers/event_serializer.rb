class EventSerializer < ActiveModel::Serializer
  belongs_to :applicable_style, key: :style
  attribute :applicable_style_id
  attributes :id, :is_deleted, :starts_at, :ends_at, :calendar_id,
    :applicable_style_id, :style_id, :position, :item_id,
    :name, :use_custom_styles, :partition, :snapshot_id, :length,
    :interruptions, :interrupted_by_holidays, :interrupted_by_weekends
end
