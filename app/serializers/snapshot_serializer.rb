class SnapshotSerializer < ActiveModel::Serializer
  attributes :id, :name, :notes, :calendar_id, :is_deleted, :created_at, :created_by, :pending
end
