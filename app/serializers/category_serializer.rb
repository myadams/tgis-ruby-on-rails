class CategorySerializer < ActiveModel::Serializer
  belongs_to :applicable_style, key: :style
  attributes :id, :name, :is_deleted, :category_id, :calendar_id,
    :active, :open, :category_id, :use_custom_styles, :snapshot_id,
    :applicable_style_id, :style_id, :interrupted_by_holidays,
    :interrupted_by_weekends, :partition
end
