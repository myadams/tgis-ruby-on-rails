class ReportCalendarSerializer < ActiveModel::Serializer
  attributes :id, :calendar_id, :report_id, :is_deleted
end
