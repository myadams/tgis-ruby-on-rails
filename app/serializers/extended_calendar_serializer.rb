class ExtendedCalendarSerializer < CalendarSerializer
  belongs_to :organization
  has_many :styles
  has_many :events
  has_many :items
  has_many :categories
  has_many :holidays
end
