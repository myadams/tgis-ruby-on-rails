class HolidaySerializer < ActiveModel::Serializer
  attributes :id, :name, :starts_at, :ends_at, :length, :calendar_id, :is_deleted, :snapshot_id
end
