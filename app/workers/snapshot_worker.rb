class SnapshotWorker
  include Sidekiq::Worker

  def perform(snapshot_id)
    transaction = SnapshotTransaction.new(snapshot_id: snapshot_id)
    transaction.run!
    transaction.result.pending = false
    transaction.result.save!
  end
end
