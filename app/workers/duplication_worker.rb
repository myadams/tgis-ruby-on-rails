class DuplicationWorker
  include Sidekiq::Worker

  def perform(organization_id, calendar_id, template_id, snapshot_id, include_events)
    organization = Organization.find(organization_id)
    calendar = Calendar.find(calendar_id)
    if snapshot_id.blank?
      template_calendar = Calendar.includes(:styles, events: [:style], categories: [:style, items: [:style, events: [:style]]], items: [:style]).where(id: template_id, organization_id: organization.id).first
    else 
      template_calendar = Snapshot.includes(:styles, events: [:style], categories: [:style, items: [:style, events: [:style]]], items: [:style]).joins(:calendar).where(id: snapshot_id, calendar_id: template_id, calendars: {organization_id: organization.id}).first
    end
    
    transaction = DuplicateTransaction.new(
      targetCalendar: calendar,
      sourceCalendar: template_calendar,
      includeEvents: include_events
    )
    transaction.run!
    calendar.update_attributes!(pending: false)
  end
end
