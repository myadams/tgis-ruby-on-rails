class CalendarFormatter

  def initialize(calendar:)
    @calendar = calendar
  end

  def format_as_event_list
    attributes = %w{calendar_name item_name event_name event_start_date event_end_date calendar_id item_id event_id}
    CSV.generate(headers: true) do |csv|
      csv << attributes
      events.not_deleted.each do |event|
        csv << attributes.map do |attr|
          parts = attr.split('_',2)
          target = case parts[0]
          when 'calendar'
            self
          when 'item'
            event.item
          when 'event'
            event
          end
          target.nil? ? nil : target.send(parts[1])
        end
      end
    end
  end

  def format_as_grid
    events = @calendar.events.includes(item: [:category]).order('categories.name').not_deleted
    dates = events.map{|e| [e.starts_at.to_i, e.ends_at.to_i]}.flatten.uniq.sort.map{|ts| Time.at(ts).strftime("%m-%d-%Y")}
    headers = ['calendar', 'category'].append(dates).flatten
    CSV.generate(headers: true) do |csv|
      csv << headers
      events.each do |event|
        start_index = headers.index(event.start_date).to_i
        end_index = headers.index(event.end_date).to_i
        category_name = (event.item.nil? || event.item.category.nil?) ? '' : event.item.category.name
        row = [@calendar.name, category_name]
        row.fill(nil, start_index)
        row.fill(nil, end_index, headers.length-end_index)
        name = event.name.blank? ? event.item.name : event.name
        row.fill(name, start_index, 1+end_index-start_index)
        csv << row
      end
    end
  end

  def format_as_weekly_grid
    events = @calendar.events.includes(item: [:category]).order('starts_at ASC').not_deleted
    current_week = events.first.starts_at.beginning_of_week
    final_week = events.last.ends_at.beginning_of_week
    dates = []
    while current_week <= final_week do
      dates << current_week
      (current_week += 7.days).beginning_of_week
    end
    headers = ['calendar', 'category'].append(dates.each_with_index.map{|d, i| "Week #{i+1} (#{d.strftime("%m-%d-%Y")})"}).flatten
    CSV.generate(headers: true) do |csv|
      csv << headers
      events.each do |event|
        possible_start_index = (dates.find_index{|d| event.starts_at.beginning_of_week <= d})
        next if possible_start_index.nil?
        start_index = possible_start_index + 2
        end_index = (dates.find_index{|d| event.ends_at.beginning_of_week <= d} || (dates.length - 1)) + 2
        category_name = (event.item.nil? || event.item.category.nil?) ? '' : event.item.category.name
        row = [@calendar.name, category_name]
        row.fill(nil, start_index)
        row.fill(nil, end_index, headers.length-end_index)
        name = event.name.blank? ? event.item.name : event.name
        row.fill(name, start_index, 1+end_index-start_index)
        csv << row
      end
    end
  end

end
