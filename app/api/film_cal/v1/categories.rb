module FilmCal
  class V1::Categories < Grape::API
    include FilmCal::V1::Defaults
    resource :categories do


      helpers do
        def find_organization(subdomain)
          if current_user.super_admin?
            organization = Organization.where(subdomain: subdomain).first!
          else
            organization = current_user.organizations.where(subdomain: subdomain).first!
          end
          organization
        end

        def get_calendar(organization_subdomain:, calendar_id:, writeable: false)
          query = CalendarQuery.new(
            user: current_user,
            organization: find_organization(organization_subdomain)
          )
          if writeable
            return query.writeable_calendars.find(calendar_id)
          else
            return query.readable_calendars.find(calendar_id)
          end
        end
      end


      desc 'Returns a list of available categories'
      oauth2
      params do
        requires :organization_subdomain, type: String, desc: 'The organization the calendars belong to.'
        requires :calendar_id, type: String, desc: 'The calendar the categories belong to.'
      end
      get do
        get_calendar(organization_subdomain: params[:organization_subdomain], calendar_id: params[:calendar_id]).categories.all
      end


      desc 'Return a specific category.'
      oauth2
      params do
        requires :id, type: Integer, desc: 'Category id.'
        requires :organization_subdomain, type: String, desc: 'The organization the calendar belongs to.'
        requires :calendar_id, type: String, desc: 'The calendar the categories belong to.'
      end
      route_param :id do
        get do
          get_calendar(organization_subdomain: params[:organization_subdomain], calendar_id: params[:calendar_id]).categories.find(params[:id])
        end
      end


      desc 'Posts a new category.'
      oauth2
      params do
        requires :organization_subdomain, type: String, desc: 'The organization the calendar belongs to.'
        requires :calendar_id, type: String, desc: 'The calendar the category belongs to.'
        requires :name, type: String, desc: 'The name of the category.'
        requires :use_custom_styles, type: Boolean, desc: 'If true this category uses custom styles.'
        optional :position, type: Integer, desc: 'The line ranking for the calendar category on the client. Lower values appear on top.'
        optional :category_id, type: Integer, desc: 'An optional parent category ID.'
        given :use_custom_styles do
          requires :style, type: Hash do
            requires :fill_color, type: String, desc: 'The color of the fill for the category.'
            requires :outline_color, type: String, desc: 'The color of the stroke around the category.'
            requires :text_alignment, type: String, desc: 'The alignment of the text.'
            requires :text_color, type: String, desc: 'The color of the text.'
            requires :text_decoration, type: String, desc: 'The decoration of the text.'
            requires :font, type: String, desc: 'The font used to render the category.'
            requires :font_size, type: String, desc: 'The size of the font used to render the category.'
            requires :font_style, type: String, desc: 'The style of the font used to render the category.'
            requires :font_weight, type: String, desc: 'The weight of the font used to render the category.'
          end
        end
      end
      post do
        calendar = get_calendar(organization_subdomain: params[:organization_subdomain], calendar_id: params[:calendar_id], writeable: true)
        attributes = params.slice(:name, :use_custom_styles, :category_id)
        category = Category.new(attributes)
        category.use_custom_styles = params[:use_custom_styles]
        if category.uses_custom_styles?
          style_params = params[:style].merge(calendar_id: params[:calendar_id])
          category.style = Style.create!(style_params)
        end
        calendar.categories << category
        category.save!
        category.insert_at(params[:position]) unless params[:position].blank?
        category
      end


      desc 'Updates an existing category.'
      oauth2
      params do
        requires :id, type: Integer, desc: 'Category id.'
        requires :organization_subdomain, type: String, desc: 'The organization the calendar belongs to.'
        requires :calendar_id, type: Integer, desc: 'The calendar the category belongs to.'
        requires :name, type: String, desc: 'The name of the category.'
        requires :active, type: Boolean, desc: 'Determines whether or not the calendar items should be considered visible.'
        requires :use_custom_styles, type: Boolean, desc: 'If true this category uses custom styles.'
        optional :partition, type: Integer, desc: 'The default day partition to apply to the category.'
        optional :position, type: Integer, desc: 'The line ranking for the calendar category on the client. Lower values appear on top.'
        given :use_custom_styles do
          requires :style, type: Hash do
            requires :fill_color, type: String, desc: 'The color of the fill for the category.'
            requires :outline_color, type: String, desc: 'The color of the stroke around the category.'
            requires :text_alignment, type: String, desc: 'The alignment of the text.'
            requires :text_color, type: String, desc: 'The color of the text.'
            requires :text_decoration, type: String, desc: 'The decoration of the text.'
            requires :font, type: String, desc: 'The font used to render the category.'
            requires :font_size, type: String, desc: 'The size of the font used to render the category.'
            requires :font_style, type: String, desc: 'The style of the font used to render the category.'
            requires :font_weight, type: String, desc: 'The weight of the font used to render the category.'
          end
        end
      end
      route_param :id do
        put do
          calendar = get_calendar(organization_subdomain: params[:organization_subdomain], calendar_id: params[:calendar_id], writeable: true)
          category = calendar.categories.find(params[:id])
          existing_style_id = category.applicable_style_id
          attributes = params.slice(:name, :active, :open, :category_id, :use_custom_styles, :partition)
          category.use_custom_styles = params[:use_custom_styles]
          if attributes.use_custom_styles
            style = category.style || Style.new
            style_params = params[:style].merge(calendar_id: params[:calendar_id], id: category.style_id)
            style.update_attributes!(style_params)
            category.style = style
          end
          category.update_attributes!(attributes)
          category.insert_at(params[:position]) unless params[:position].blank?
          if existing_style_id != category.applicable_style_id
            extended_category = Category.includes(
              categories: [:inherited_style, :style, items: [
                :inherited_style,
                :style,
                events: [:inherited_style, :style]
              ]],
              items: [
                :inherited_style,
                :style,
                events: [:inherited_style, :style]
              ]
            ).find(category.id)
            render ExtendedCategorySerializer.new(extended_category).as_json
          else
            render category
          end
        end
      end


      desc 'Updates the open status of an existing category.'
      oauth2
      params do
        requires :id, type: Integer, desc: 'Category id.'
        requires :organization_subdomain, type: String, desc: 'The organization the calendar belongs to.'
        requires :calendar_id, type: Integer, desc: 'The calendar the category belongs to.'
        requires :open, type: Boolean, desc: 'The open status of the category.'
      end
      route_param :id do
        patch '/open' do
          calendar = get_calendar(organization_subdomain: params[:organization_subdomain], calendar_id: params[:calendar_id], writeable: true)
          category = calendar.categories.find(params[:id])
          category.update_attributes!(open: params[:open])
          category
        end
      end


      desc 'Updates the active status of an existing category.'
      oauth2
      params do
        requires :id, type: Integer, desc: 'Category id.'
        requires :organization_subdomain, type: String, desc: 'The organization the calendar belongs to.'
        requires :calendar_id, type: Integer, desc: 'The calendar the category belongs to.'
        requires :active, type: Boolean, desc: 'The active status of the category.'
      end
      route_param :id do
        patch '/activate' do
          calendar = get_calendar(organization_subdomain: params[:organization_subdomain], calendar_id: params[:calendar_id], writeable: true)
          category = calendar.categories.find(params[:id])
          category.update_attributes!(active: params[:active])
          category
        end
      end


      desc 'Updates the parent category of an existing category.'
      oauth2
      params do
        requires :id, type: Integer, desc: 'Category id.'
        requires :organization_subdomain, type: String, desc: 'The organization the calendar belongs to.'
        requires :calendar_id, type: Integer, desc: 'The calendar the category belongs to.'
        optional :category_id, type: Integer, desc: 'The new parent category for the selected item.'
      end
      route_param :id do
        patch '/parent' do
          calendar = get_calendar(organization_subdomain: params[:organization_subdomain], calendar_id: params[:calendar_id], writeable: true)
          category = calendar.categories.find(params[:id])
          parent_category = calendar.categories.find(params[:category_id]) unless params[:category_id].nil?
          if params[:category_id].nil? || !parent_category.already_belongs_to_category(category)
            category.update_attributes!(category_id: params[:category_id])
          end
          render ExtendedCategorySerializer.new(category).as_json
        end
      end


      desc 'Deletes an existing category.'
      oauth2
      params do
        requires :calendar_id, type: Integer, desc: 'The id of the calendar the category belongs to.'
        requires :organization_subdomain, type: String, desc: 'The organization the calendar belongs to.'
        requires :id, type: Integer, desc: 'The id of the category to be deleted.'
      end
      route_param :id do
        delete do
          calendar = get_calendar(organization_subdomain: params[:organization_subdomain], calendar_id: params[:calendar_id], writeable: true)
          category = calendar.categories.find(params[:id])
          category.delete_including_children!
          category.destroy!
          category
        end
      end


      desc 'Deletes a group of existing categories.'
      oauth2
      params do
        requires :calendar_id, type: Integer, desc: 'The id of the calendar the event belongs to.'
        requires :organization_subdomain, type: String, desc: 'The organization the calendar belongs to.'
        requires :ids, type: Array[Integer], desc: 'The ids of the categories to be deleted.'
      end
      patch do
        calendar = get_calendar(organization_subdomain: params[:organization_subdomain], calendar_id: params[:calendar_id], writeable: true)
        categories = calendar.categories.includes(items: :events).not_deleted.find(params[:ids])
        categories.each do |category|
          category.delete_including_children!
        end
        categories
      end


    end
  end
end
