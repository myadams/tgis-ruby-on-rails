module FilmCal
  class V1::GranularPermissions < Grape::API
    include FilmCal::V1::Defaults
    resource :granular_permissions do


      desc 'Creates a new calendar permission for a user.'
      oauth2
      params do
        requires :invitation_id, type: Integer, desc: 'The id of the invitation to grant permissions for.'
        requires :calendar_id, type: Integer, desc: 'The id of the calendar the permission belongs to.'
        requires :read, type: Boolean, desc: 'If the user has read access'
        requires :write, type: Boolean, desc: 'If the user has write access'
      end
      post do
        invitation = Invitation.find(params[:invitation_id])
        error_unless_admin!(invitation.organization.subdomain)
        permission = GranularPermission.create!(
          invitation_id: params[:invitation_id],
          calendar_id: params[:calendar_id],
          read: params[:read],
          write: params[:write]
        )
        permission
      end


      desc 'Gets a list of permissions for a user.'
      oauth2
      params do
        requires :invitation_id, type: Integer, desc: 'The id of the invitation to grant permissions for.'
      end
      get do
        invitation = Invitation.find(params[:invitation_id])
        error_unless_admin!(invitation.organization.subdomain)
        error_unless_user_can_access!(organization: invitation.organization)
        invitation.granular_permissions
      end


      desc 'Updates a specific permission for an invitation.'
      oauth2
      params do
        requires :granular_permission_id, type: Integer, desc: 'The id of invitation to update'
        requires :read, type: Boolean, desc: 'If the user has read access'
        requires :write, type: Boolean, desc: 'If the user has write access'
      end
      route_param :granular_permission_id do
        put do
          permission = GranularPermission.find(params[:granular_permission_id])
          error_unless_admin!(permission.invitation.organization.subdomain)
          permission.update_attributes!(
            read: params[:read],
            write: params[:write]
          )
          permission
        end
      end


    end
  end
end
