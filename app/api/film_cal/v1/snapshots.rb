module FilmCal
  class V1::Snapshots < Grape::API
    include FilmCal::V1::Defaults
    resource :snapshots do


      helpers do
        def find_organization(subdomain)
          if current_user.super_admin?
            organization = Organization.where(subdomain: subdomain).not_deleted.first!
          else
            organization = current_user.organizations.where(subdomain: subdomain).not_deleted.first!
          end
          organization
        end

        def get_calendar(organization_subdomain:, calendar_id:, writeable: false)
          query = CalendarQuery.new(
            user: current_user,
            organization: find_organization(organization_subdomain)
          )
          if writeable
            return query.writeable_calendars.not_deleted.find(calendar_id)
          else
            return query.readable_calendars.not_deleted.find(calendar_id)
          end
        end
      end


      desc 'Returns a list of available snapshots'
      oauth2
      params do
        requires :organization_subdomain, type: String, desc: 'The organization the calendars belong to.'
        requires :calendar_id, type: String, desc: 'The calendar the items belong to.'
      end
      get do
        calendar = get_calendar(organization_subdomain: params[:organization_subdomain], calendar_id: params[:calendar_id])
        return calendar.snapshots.not_deleted.all
      end


      desc 'Return a specific snapshot.'
      oauth2
      params do
        requires :id, type: Integer, desc: 'Snapshot id.'
        requires :organization_subdomain, type: String, desc: 'The organization the calendar belongs to.'
        requires :calendar_id, type: String, desc: 'The calendar the items belong to.'
      end
      route_param :id do
        get serializer: ExtendedSnapshotSerializer do
          calendar = get_calendar(organization_subdomain: params[:organization_subdomain], calendar_id: params[:calendar_id])
          calendar.snapshots.not_deleted.find(params[:id])
        end
      end


      desc 'Posts a new snapshot.'
      oauth2
      params do
        requires :organization_subdomain, type: String, desc: 'The organization the calendar belongs to.'
        requires :calendar_id, type: String, desc: 'The calendar the item belongs to.'
        requires :name, type: String, desc: 'The name of the snapshot.'
        requires :notes, type: String, desc: 'A brief description about the snapshot.'
      end
      post do
        calendar = get_calendar(organization_subdomain: params[:organization_subdomain], calendar_id: params[:calendar_id], writeable: true)
        snapshot = Snapshot.new(calendar_id: calendar.id, user_id: current_user.id, name: params[:name], notes: params[:notes], pending: true)
        snapshot.save!
        SnapshotWorker.perform_async(snapshot.id)
        snapshot
      end


      desc 'Updates an existing snapshot.'
      oauth2
      params do
        requires :organization_subdomain, type: String, desc: 'The organization the calendar belongs to.'
        requires :calendar_id, type: String, desc: 'The calendar the item belongs to.'
        requires :id, type: Integer, desc: 'The id of the snapshot to update.'
        requires :name, type: String, desc: 'The name of the snapshot.'
        requires :notes, type: String, desc: 'A brief description about the snapshot.'
      end
      route_param :id do
        put do
          calendar = get_calendar(organization_subdomain: params[:organization_subdomain], calendar_id: params[:calendar_id], writeable: true)
          snapshot = calendar.snapshots.find(params[:id])
          snapshot.update_attributes!(name: params[:name], notes: params[:notes])
          snapshot
        end
      end


      desc 'Deletes an existing snapshot.'
      oauth2
      params do
        requires :calendar_id, type: Integer, desc: 'The id of the calendar the item belongs to.'
        requires :organization_subdomain, type: String, desc: 'The organization the calendar belongs to.'
        requires :id, type: Integer, desc: 'The id of the item to be deleted.'
      end
      route_param :id do
        delete do
          calendar = get_calendar(organization_subdomain: params[:organization_subdomain], calendar_id: params[:calendar_id], writeable: true)
          snapshot = calendar.snapshots.not_deleted.find(params[:id])
          snapshot.update_attributes({is_deleted: true})
          snapshot.destroy!
          snapshot
        end
      end


    end
  end
end
