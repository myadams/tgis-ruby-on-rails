module FilmCal
  module V1
    module Defaults
      extend ActiveSupport::Concern
      included do

        before do
          header['Access-Control-Allow-Origin'] = '*'
          header['Access-Control-Request-Method'] = '*'
        end

        format :json
        formatter :json, Grape::Formatter::ActiveModelSerializers
        version 'v1', using: :header, vendor: 'film_cal'
        default_error_formatter :json
        
        content_type :json, 'application/json'

        use ::WineBouncer::OAuth2

        rescue_from :all do |error|
          FilmCal::Base.cors_response_to_error(error)
        end

        helpers do

          def cors_headers
            {
              'Access-Control-Allow-Origin' => '*',
              'Access-Control-Request-Method' => '*'
            }
          end

          def current_token
            doorkeeper_access_token
          end

          def current_user
            resource_owner
          end

          def current_scopes
            current_token.scopes
          end

          def raise_401_unauthorized!(message: 'Access denied for current user.')
            raise WineBouncer::Errors::OAuthUnauthorizedError.new({description: message})
          end

          def raise_403_forbidden!(message: 'Access denied for current user.')
            raise WineBouncer::Errors::OAuthForbiddenError.new({description: message})
          end

          def raise_404_not_found!(message: 'The requested resource could not be found.')
            raise ActiveRecord::RecordNotFound.new({description: message})
          end

          def error_unless_admin!(subdomain)
            raise_403_forbidden! unless current_user.super_admin? || (!subdomain.nil? && current_user.admin?(subdomain))
          end

          def error_unless_super_admin!
            raise_403_forbidden! unless current_user.super_admin?
          end

          def error_unless_user_can_access!(organization:)
            access = current_user.super_admin? || current_user.organizations.include?(organization)
            raise_403_forbidden! unless access
          end
        end

      end
    end
  end
end
