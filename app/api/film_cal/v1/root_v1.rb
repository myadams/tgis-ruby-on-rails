module FilmCal
  module V1
    class RootV1 < Grape::API

      mount Me
      mount Organizations
      mount Users
      
    end
  end
end
