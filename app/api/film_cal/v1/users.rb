module FilmCal
  class V1::Users < Grape::API
    include FilmCal::V1::Defaults
    resource :users, desc: 'Operations for manahing users' do


      desc 'Generates an email allowing the user to securely reset their password.'
      params do
        requires :email, type: String, desc: 'The email address of the user to recieve reset token.'
      end
      post :forgot_password do
        user = User.where({email: params[:email]}).first!
        NotificationsMailer.forgot_password(user, user.generate_token).deliver_now
        { sent: true, email: params[:email] }
      end


      desc 'Resets a users password if the reset token is valid.'
      params do
        requires :token, type: String, desc: 'A JWT representing the secure authorization window for this action.'
        requires :password, type: String, desc: 'The new password for this user.'
      end
      patch :reset_password do
        decoded_token = JWT.decode params[:token], ENV['JWT_SECRET'], true, { :algorithm => 'HS512' }
        email = decoded_token[0]["email"]
        user = User.where({email: email}).first!
        user.update_attributes({password: params[:password]})
        NotificationsMailer.confirm_password_reset(user).deliver_now
        { updated: true, email: email }
      end


      desc 'Registers a new user.'
      params do
        requires :email, type: String, desc: 'The email address'
        requires :password, type: String, desc: 'The new password for this user.'
        requires :password_confirmation, type: String, desc: 'The confirmation of the password.'
        optional :token, type: String, desc: 'An optional invitation token to associate with the user.'
      end
      post :register do
        user = User.new({
          email: params[:email],
          password: params[:password],
          password_confirmation: params[:password_confirmation]
        })
        if !params[:token].blank? && (invitation = Invitation.retrieve_from_token(params[:token])) && !invitation.accepted
          user.is_super_admin = invitation.is_super_admin
          user.save!
          invitation.update_attributes!({
            accepted: true,
            user_id: user.id,
            email: user.email
          })
        else
          user.save!
        end
        user
      end


    end

  end
end
