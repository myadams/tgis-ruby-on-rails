module FilmCal
  class V1::Organizations < Grape::API
    include FilmCal::V1::Defaults
    resource :organizations do

      helpers do

        def find_organizations
          if current_user.super_admin?
            organizations = Organization.all
          else
            organizations = current_user.organizations
          end
          organizations
        end

        def find_organization(id)
          Organization.all.where({id: params[:id]}).first
        end

      end

      route_param :organization_subdomain do
        mount Calendars
        mount Invitations
        mount Reports
      end

      desc 'Validated supplied attributes for an organization'
      oauth2
      params do
        optional :id, type: Integer, desc: 'The id of the organization if it is an existing organization'
        optional :name, type: String, desc: 'The name of the organization.'
        optional :color, type: String, desc: 'A color to help identify the organization in a client UI.'
        optional :subdomain, type: String, desc: 'The sub domain or URL slug to identiy the organization in a route.'
        optional :uses_granular_permissions, type: Boolean, desc: 'Whether or not the organization utilizes access permissions.'
        optional :always_ok, type: Boolean, desc: 'Forces the API to always send a 200 response'
      end
      get :validations do
        if !(o = find_organization(params[:id])).nil?
          o.name = params[:name]
          o.subdomain = params[:subdomain]
          o.color = params[:color]
          o.uses_granular_permissions = params[:uses_granular_permissions]
        else
          o = Organization.new({
            name: params[:name],
            subdomain: params[:subdomain],
            color: params[:color],
            uses_granular_permissions: params[:uses_granular_permissions]
          })
        end
        o.save! unless o.valid? || params[:always_ok] == true
        errors = FilmCal::Base.map_active_record_errors(o.errors)
        { valid: o.valid?, errors: errors, error: "" }
      end


      desc 'Returns a list of available organizations'
      oauth2
      get do
        orgs = find_organizations.alphabetical.all
        orgs
      end


      desc 'Return a specific organization.'
      oauth2
      params do
        requires :subdomain, type: String, desc: 'Organization\'s subdomain.'
      end
      route_param :subdomain do
        get do
          if current_user.super_admin?
            orgs = Organization.where(subdomain: params[:subdomain]).all
          else
            orgs = current_user.organizations.where(subdomain: params[:subdomain]).all
          end
          raise ActiveRecord::RecordNotFound if orgs.length < 1
          orgs
        end
      end

      desc 'Posts a new organization.'
      oauth2
      params do
        requires :name, type: String, desc: 'The name of the organization.'
        requires :color, type: String, desc: 'A color to help identify the organization in a client UI.'
        requires :subdomain, type: String, desc: 'The sub domain or URL slug to identiy the organization in a route.'
        requires :uses_granular_permissions, type: Boolean, desc: 'Whether or not the organization utilizes access permissions.'
      end
      post do
        error_unless_super_admin!
        Organization.create!(
          name: params[:name],
          subdomain: params[:subdomain],
          color: params[:color],
          uses_granular_permissions: params[:uses_granular_permissions]
        )
      end


      desc 'Updates an existing organization.'
      oauth2
      params do
        requires :id, type: Integer, desc: 'Organization id.'
        requires :name, type: String, desc: 'The name of the organization.'
        requires :color, type: String, desc: 'A color to help identify the organization in a client UI.'
        requires :uses_granular_permissions, type: Boolean, desc: 'Whether or not the organization utilizes access permissions.'
      end
      route_param :id do
        put do
          error_unless_super_admin!
          organization = Organization.find(params[:id])
          organization.update!(
            name: params[:name],
            color: params[:color],
            uses_granular_permissions: params[:uses_granular_permissions]
          )
          organization
        end
      end


      desc 'Deletes an existing organization.'
      oauth2
      params do
        requires :id, type: Integer, desc: 'Organization id.'
      end
      route_param :id do
        delete do
          error_unless_super_admin!
          organization = Organization.find(params[:id])
          organization.update_attributes({is_deleted: true})
          organization.destroy!
          organization
        end
      end


    end
  end
end
