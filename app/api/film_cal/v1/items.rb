module FilmCal
  class V1::Items < Grape::API
    include FilmCal::V1::Defaults
    resource :items do


      helpers do
        def find_organization(subdomain)
          if current_user.super_admin?
            organization = Organization.where(subdomain: subdomain).not_deleted.first!
          else
            organization = current_user.organizations.where(subdomain: subdomain).not_deleted.first!
          end
          organization
        end

        def get_calendar(organization_subdomain:, calendar_id:, writeable: false)
          query = CalendarQuery.new(
            user: current_user,
            organization: find_organization(organization_subdomain)
          )
          if writeable
            return query.writeable_calendars.not_deleted.find(calendar_id)
          else
            return query.readable_calendars.not_deleted.find(calendar_id)
          end
        end
      end


      desc 'Returns a list of available items'
      oauth2
      params do
        requires :organization_subdomain, type: String, desc: 'The organization the calendars belong to.'
        requires :calendar_id, type: String, desc: 'The calendar the items belong to.'
        optional :category_id, type: Integer, desc: 'The id of the category the item belongs to.'
      end
      get do
        calendar = get_calendar(organization_subdomain: params[:organization_subdomain], calendar_id: params[:calendar_id])
        return calendar.items.not_deleted.all if params[:category_id].blank?
        category = calendar.categories.not_deleted.find(params[:category_id])
        category.items.not_deleted.all
      end


      desc 'Return a specific item.'
      oauth2
      params do
        requires :id, type: Integer, desc: 'Item id.'
        requires :organization_subdomain, type: String, desc: 'The organization the calendar belongs to.'
        requires :calendar_id, type: String, desc: 'The calendar the items belong to.'
      end
      route_param :id do
        get do
          calendar = get_calendar(organization_subdomain: params[:organization_subdomain], calendar_id: params[:calendar_id])
          calendar.items.not_deleted.find(params[:id])
        end
      end


      desc 'Posts a new item.'
      oauth2
      params do
        requires :organization_subdomain, type: String, desc: 'The organization the calendar belongs to.'
        requires :calendar_id, type: String, desc: 'The calendar the item belongs to.'
        requires :name, type: String, desc: 'The name of the item.'
        requires :use_custom_styles, type: Boolean, desc: 'If true this item uses custom styles.'
        optional :position, type: Integer, desc: 'The line ranking for the calendar item on the client. Lower values appear on top.'
        given :use_custom_styles do
          requires :style, type: Hash do
            requires :fill_color, type: String, desc: 'The color of the fill for the item.'
            requires :outline_color, type: String, desc: 'The color of the stroke around the item.'
            requires :text_alignment, type: String, desc: 'The alignment of the text.'
            requires :text_color, type: String, desc: 'The color of the text.'
            requires :text_decoration, type: String, desc: 'The decoration of the text.'
            requires :font, type: String, desc: 'The font used to render the item.'
            requires :font_size, type: String, desc: 'The size of the font used to render the item.'
            requires :font_style, type: String, desc: 'The style of the font used to render the item.'
            requires :font_weight, type: String, desc: 'The weight of the font used to render the item.'
          end
        end
      end
      post do
        calendar = get_calendar(organization_subdomain: params[:organization_subdomain], calendar_id: params[:calendar_id], writeable: true)
        attributes = params.slice(:use_custom_styles, :name)
        item = Item.new(attributes)
        if item.uses_custom_styles?
          style_params = params[:style].merge(calendar_id: params[:calendar_id])
          item.style = Style.create!(style_params)
        end
        calendar.items << item
        unless params[:category_id].blank?
          category = calendar.categories.not_deleted.find(params[:category_id])
          category.items << item
        end
        item.save!
        item.insert_at(params[:position]) unless params[:position].blank?
        item
      end


      desc 'Updates an existing item.'
      oauth2
      params do
        requires :id, type: Integer, desc: 'Item id.'
        requires :organization_subdomain, type: String, desc: 'The organization the calendar belongs to.'
        requires :calendar_id, type: Integer, desc: 'The calendar the item belongs to.'
        requires :active, type: Boolean, desc: 'Determines whether or not the calendar items should be considered visible.'
        requires :locked, type: Boolean, desc: 'Determines whether or not the calendar item can be shifted on the calendar.'
        requires :name, type: String, desc: 'The name of the item.'
        requires :use_custom_styles, type: Boolean, desc: 'If true this item uses custom styles.'
        optional :position, type: Integer, desc: 'The line ranking for the calendar item on the client. Lower values appear on top.'
        given :use_custom_styles do
          requires :style, type: Hash do
            requires :fill_color, type: String, desc: 'The color of the fill for the item.'
            requires :outline_color, type: String, desc: 'The color of the stroke around the item.'
            requires :text_alignment, type: String, desc: 'The alignment of the text.'
            requires :text_color, type: String, desc: 'The color of the text.'
            requires :text_decoration, type: String, desc: 'The decoration of the text.'
            requires :font, type: String, desc: 'The font used to render the item.'
            requires :font_size, type: String, desc: 'The size of the font used to render the item.'
            requires :font_style, type: String, desc: 'The style of the font used to render the item.'
            requires :font_weight, type: String, desc: 'The weight of the font used to render the item.'
          end
        end
      end
      route_param :id do
        put do
          calendar = get_calendar(organization_subdomain: params[:organization_subdomain], calendar_id: params[:calendar_id], writeable: true)
          item = calendar.items.not_deleted.find(params[:id])
          existing_style_id = item.applicable_style_id
          attributes = params.slice(:use_custom_styles, :active, :locked, :name, :category_id)
          if attributes.use_custom_styles
            style = item.style || Style.new
            style_params = params[:style].merge(calendar_id: params[:calendar_id], id: item.style_id)
            style.update_attributes!(style_params)
            item.style = style
          end
          item.update_attributes!(attributes)
          item.insert_at(params[:position]) unless params[:position].blank?
          if(item.applicable_style_id != existing_style_id)
            render ExtendedItemSerializer.new(Item.includes(:events).find(item.id)).as_json
          else
            render item
          end
        end
      end


      desc 'Changes the locked status of an item.'
      oauth2
      params do
        requires :id, type: Integer, desc: 'Item id.'
        requires :organization_subdomain, type: String, desc: 'The organization the calendar belongs to.'
        requires :calendar_id, type: Integer, desc: 'The calendar the item belongs to.'
        requires :locked, type: Boolean, desc: 'Determines whether or not the calendar item can be shifted on the calendar.'
      end
      route_param :id do
        patch '/lock' do
          calendar = get_calendar(organization_subdomain: params[:organization_subdomain], calendar_id: params[:calendar_id], writeable: true)
          item = calendar.items.not_deleted.find(params[:id])
          item.update_attributes!(locked: params[:locked])
          item
        end
      end


      desc 'Changes the active status of an item.'
      oauth2
      params do
        requires :id, type: Integer, desc: 'Item id.'
        requires :organization_subdomain, type: String, desc: 'The organization the calendar belongs to.'
        requires :calendar_id, type: Integer, desc: 'The calendar the item belongs to.'
        requires :active, type: Boolean, desc: 'Determines whether or not the calendar item should be visible on the calendar.'
      end
      route_param :id do
        patch '/activate' do
          calendar = get_calendar(organization_subdomain: params[:organization_subdomain], calendar_id: params[:calendar_id], writeable: true)
          item = calendar.items.not_deleted.find(params[:id])
          item.update_attributes!(active: params[:active])
          item
        end
      end


      desc 'Changes the parent category of an item.'
      oauth2
      params do
        requires :id, type: Integer, desc: 'Item id.'
        requires :organization_subdomain, type: String, desc: 'The organization the calendar belongs to.'
        requires :calendar_id, type: Integer, desc: 'The calendar the item belongs to.'
        requires :category_id, type: Integer, desc: 'The parent category of an item.'
      end
      route_param :id do
        patch '/parent' do
          calendar = get_calendar(organization_subdomain: params[:organization_subdomain], calendar_id: params[:calendar_id], writeable: true)
          item = calendar.items.not_deleted.find(params[:id])
          item.update_attributes!(category_id: params[:category_id])
          ExtendedItemSerializer.new(Item.includes(:events).find(item.id)).as_json
        end
      end


      desc 'Deletes an existing item.'
      oauth2
      params do
        requires :calendar_id, type: Integer, desc: 'The id of the calendar the item belongs to.'
        requires :organization_subdomain, type: String, desc: 'The organization the calendar belongs to.'
        requires :category_id, type: Integer, desc: 'The id of the category the item belongs to.'
        requires :id, type: Integer, desc: 'The id of the item to be deleted.'
      end
      route_param :id do
        delete do
          calendar = get_calendar(organization_subdomain: params[:organization_subdomain], calendar_id: params[:calendar_id], writeable: true)
          category = calendar.categories.not_deleted.find(params[:category_id])
          item = category.items.not_deleted.find(params[:id])
          item.delete_including_children!
          item.destroy!
          item
        end
      end


      desc 'Deletes a group of existing items.'
      oauth2
      params do
        requires :calendar_id, type: Integer, desc: 'The id of the calendar the event belongs to.'
        requires :organization_subdomain, type: String, desc: 'The organization the calendar belongs to.'
        requires :ids, type: Array[Integer], desc: 'The ids of the items to be deleted.'
      end
      patch do
        calendar = get_calendar(organization_subdomain: params[:organization_subdomain], calendar_id: params[:calendar_id], writeable: true)
        items = calendar.items.not_deleted.find(params[:ids])
        items.each {|item| item.delete_including_children! }
        items
      end


    end
  end
end
