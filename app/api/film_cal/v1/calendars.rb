require 'csv'

module FilmCal
  class V1::Calendars < Grape::API
    include FilmCal::V1::Defaults
    resource :calendars do

      content_type :csv, 'text/csv'

      helpers do
        def find_organization(subdomain)
          if current_user.super_admin?
            organization = Organization.includes(:calendars).where(subdomain: subdomain).first!
          else
            organization = current_user.organizations.includes(:calendars).where(subdomain: subdomain).first!
          end
          organization
        end

        def get_query(organization_subdomain:)
          CalendarQuery.new(
            user: current_user,
            organization: find_organization(organization_subdomain)
          )
        end
      end


      route_param :calendar_id do
        mount Events
        mount Items
        mount Categories
        mount Snapshots
        mount Holidays
      end


      desc 'Generates a CSV for a selected calendar'
      params do
        requires :token, type: String, desc: 'A valid JWT with the embedded details for the calendar to generate.'
        optional :identifier, type: String, desc: 'An identifier which will become the filename when downloaded by the browser.'
      end
      route_param :id do
        segment :csv do
          route_param :timestamp do
            get do
              content_type 'text/csv'
              calendar = Calendar.retrieve_from_token(params[:token])
              CalendarFormatter.new(calendar: calendar).format_as_weekly_grid
            end
          end
        end
      end


      desc 'Validates supplied attributes for a calendar'
      oauth2
      params do
        optional :id, type: Integer, desc: 'The id of the calendar if it is an existing calendar'
        optional :name, type: String, desc: 'The name of the calendar.'
        requires :organization_subdomain, type: String, desc: 'The organization the calendar should belong to.'
        optional :always_ok, type: Boolean, desc: 'Forces the API to always send a 200 response'
      end
      get :validations do
        org = find_organization(params[:organization_subdomain])
        if params[:id].nil?
          cal = Calendar.new({organization_id: org.id})
        else
          cal = org.calendars.find(params[:id])
        end
        cal.name = params[:name]
        cal.save! unless cal.valid? || params[:always_ok] == true
        errors = FilmCal::Base.map_active_record_errors(cal.errors)
        { valid: cal.valid?, errors: errors, error: "" }
      end


      desc 'Generates a download request for a selected calendar'
      oauth2
      params do
        requires :id, type: Integer, desc: 'The id of the calendar to export'
      end
      route_param :id do
        get :download do
          calendar = get_query(organization_subdomain: params[:organization_subdomain]).readable_calendars.find(params[:id])
          name_token = calendar.name.parameterize
          token = calendar.generate_token
          {
            download_token: token,
            download_url: "//#{ENV["DEFAULT_URL_OPTIONS_HOST"]}/api/organizations/#{params[:organization_subdomain]}/calendars/#{params[:id]}/csv/#{name_token}-#{Time.zone.now.to_i}?token=#{token}"
          }
        end
      end

      desc 'Returns a list of available calendars'
      oauth2
      params do
        requires :organization_subdomain, type: String, desc: 'The organization the calendars belong to.'
      end
      get do
        get_query(organization_subdomain: params[:organization_subdomain]).readable_calendars.all
      end


      desc 'Return a specific calendar.'
      oauth2
      params do
        requires :id, type: Integer, desc: 'Calendar id.'
        requires :organization_subdomain, type: String, desc: 'The organization the calendar belongs to.'
      end
      route_param :id do
        get serializer: ExtendedCalendarSerializer do
          calendar = get_query(organization_subdomain: params[:organization_subdomain])
            .readable_calendars.find(params[:id])
          Calendar.includes(:organization, :styles, events: [:style, :inherited_style], categories: [:style, :inherited_style], items: [:style, :inherited_style]).find(calendar.id)
        end
      end


      desc 'Posts a new calendar.'
      oauth2
      params do
        requires :name, type: String, desc: 'The name of the calendar.'
        requires :interrupted_by_holidays, type: Boolean, desc: 'Flag determining if holidays are treated as work days.'
        requires :interrupted_by_weekends, type: Boolean, desc: 'Flag determining if weekends are treated as work days.'
        requires :organization_subdomain, type: String, desc: 'The organization the calendar belongs to.'
        optional :template_calendar_id, type: Integer, desc: 'An ID of an existing calendar to utilize as a template.'
        optional :template_snapshot_id, type: Integer, desc: 'An ID of an existing snapshot to utilize as a template.'
        optional :include_events, type: Boolean, desc: 'Flag indicating whether or not to include events from a template.'
      end
      post do
        organization = find_organization(params[:organization_subdomain])
        error_unless_admin!(params[:organization_subdomain])
        calendar = Calendar.new(name: params[:name], interrupted_by_holidays: params[:interrupted_by_holidays], interrupted_by_weekends: params[:interrupted_by_weekends])
        organization.calendars << calendar
        template_id = params[:template_calendar_id]
        snapshot_id = params[:template_snapshot_id]
        calendar.save!
        if !template_id.blank?
          calendar.update_attributes!(pending: true)
          template_calendar = snapshot_id.blank? ? 
            Calendar.where(id: template_id, organization_id: organization.id).first :
            Snapshot.joins(:calendar).where(id: snapshot_id, calendar_id: template_id, calendars: {organization_id: organization.id}).first
          if template_calendar.blank?
            raise_404_not_found!(message: 'The supplied template does not exist or is inaccessible to the current user.')
          else
            DuplicationWorker.perform_async(organization.id, calendar.id, template_id, snapshot_id, params[:include_events])
          end
        end
        calendar
      end

      desc 'Updates an existing calendar.'
      oauth2
      params do
        requires :id, type: Integer, desc: 'Calendar id.'
        requires :interrupted_by_holidays, type: Boolean, desc: 'Flag determining if holidays are treated as work days.'
        requires :interrupted_by_weekends, type: Boolean, desc: 'Flag determining if weekends are treated as work days.'
        requires :organization_subdomain, type: String, desc: 'The organization the calendar belongs to.'
        requires :name, type: String, desc: 'The name of the calendar.'
      end
      route_param :id do
        put do
          error_unless_admin!(params[:organization_subdomain])
          query = get_query(organization_subdomain: params[:organization_subdomain])
          calendar = query.writeable_calendars.find(params[:id])
          calendar.update_attributes!(name: params[:name], interrupted_by_holidays: params[:interrupted_by_holidays], interrupted_by_weekends: params[:interrupted_by_weekends])
          calendar
        end
      end


      desc 'Pushes all events on an existing calendar past a certain date.'
      oauth2
      params do
        requires :organization_subdomain, type: String, desc: 'The organization the calendar belongs to.'
        requires :id, type: Integer, desc: 'Calendar id.'
        requires :date, type: DateTime, desc: 'The date that serves as the origin of the push.'
        requires :offset, type: Integer, desc: 'The number of days to push (can be negative to "pull").'
        requires :fill_unused_days, type: Boolean, desc: 'Set to true if the offset should fill the void between multiple events.'
        optional :categories, type: Array[Integer], desc: 'A set of category IDs to filter the push results'
        optional :items, type: Array[Integer], desc: 'A set of item type IDs to filter the push results'
      end
      route_param :id do
        patch '/push' do
          error_unless_admin!(params[:organization_subdomain])
          query = get_query(organization_subdomain: params[:organization_subdomain])
          calendar = query.writeable_calendars.find(params[:id])
          events = calendar.events.unlocked.chronological.where(['ends_at >= ? ', params[:date]])
          events_for_categories = params[:categories].blank? ? [] : events.for_categories(Category.inherited_id_map(params[:categories]))
          events_for_items = params[:items].blank? ? [] : events.for_items(params[:items])
          filtered_events = events_for_categories + events_for_items
          interrupted_by_weekends = params[:interrupted_by_weekends].nil? ? calendar.interrupted_by_weekends : params[:interrupted_by_weekends]
          push = PushTransaction.new(
            events: (filtered_events.length > 0 ? filtered_events : events),
            offset: params[:offset],
            fill_unused_days: params[:fill_unused_days])
          push.run!
          push.result
        end
      end


      desc 'Deletes an existing calendar.'
      oauth2
      params do
        requires :calendar_id, type: Integer, desc: 'The id of the calendar to be deleted.'
        requires :organization_subdomain, type: String, desc: 'The organization the calendar belongs to.'
      end
      route_param :calendar_id do
        delete do
          error_unless_admin!(params[:organization_subdomain])
          query = get_query(organization_subdomain: params[:organization_subdomain])
          calendar = query.writeable_calendars.find(params[:calendar_id])
          calendar.update_attributes({is_deleted: true})
          calendar.destroy!
          calendar
        end
      end


    end
  end
end
