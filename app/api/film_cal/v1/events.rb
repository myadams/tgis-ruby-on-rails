module FilmCal
  class V1::Events < Grape::API
    include FilmCal::V1::Defaults
    resource :events do


      helpers do
        def find_organization(subdomain)
          if current_user.super_admin?
            organization = Organization.not_deleted.where(subdomain: subdomain).first!
          else
            organization = current_user.organizations.not_deleted.where(subdomain: subdomain).first!
          end
          organization
        end

        def get_calendar(organization_subdomain:, calendar_id:, writeable: false)
          query = CalendarQuery.new(
            user: current_user,
            organization: find_organization(organization_subdomain)
          )
          if writeable
            return query.writeable_calendars.not_deleted.find(calendar_id)
          else
            return query.readable_calendars.not_deleted.find(calendar_id)
          end
        end
      end


      desc 'Returns a list of available events'
      oauth2
      params do
        requires :organization_subdomain, type: String, desc: 'The organization the calendars belong to.'
        requires :calendar_id, type: String, desc: 'The calendar the events belong to.'
      end
      get do
        get_calendar(organization_subdomain: params[:organization_subdomain], calendar_id: params[:calendar_id]).events.all
      end


      desc 'Return a specific event.'
      oauth2
      params do
        requires :id, type: Integer, desc: 'Event id.'
        requires :organization_subdomain, type: String, desc: 'The organization the calendar belongs to.'
        requires :calendar_id, type: String, desc: 'The calendar the events belong to.'
      end
      route_param :id do
        get do
          get_calendar(organization_subdomain: params[:organization_subdomain], calendar_id: params[:calendar_id]).events.not_deleted.find(params[:id])
        end
      end


      desc 'Posts a new event.'
      oauth2
      params do
        requires :organization_subdomain, type: String, desc: 'The organization the calendar belongs to.'
        requires :calendar_id, type: String, desc: 'The calendar the event belongs to.'
        requires :item_id, type: String, desc: 'The item the event belongs to.'
        requires :starts_at, type: DateTime, desc: 'The time the event starts.'
        requires :length, type: Integer, desc: 'The length the event should occur in days.'
        requires :use_custom_styles, type: Boolean, desc: 'If true this item uses custom styles.'
        optional :position, type: Integer, desc: 'The line ranking for the calendar item on the client. Lower values appear on top.'
        optional :partition, type: Integer, desc: 'Determines the track to display this event in a given week on the calendar.'
        given :use_custom_styles do
          requires :style, type: Hash do
            requires :fill_color, type: String, desc: 'The color of the fill for the event.'
            requires :outline_color, type: String, desc: 'The color of the stroke around the event.'
            requires :text_alignment, type: String, desc: 'The alignment of the text.'
            requires :text_color, type: String, desc: 'The color of the text.'
            requires :text_decoration, type: String, desc: 'The decoration of the text.'
            requires :font, type: String, desc: 'The font used to render the item.'
            requires :font_size, type: String, desc: 'The size of the font used to render the event.'
            requires :font_style, type: String, desc: 'The style of the font used to render the event.'
            requires :font_weight, type: String, desc: 'The weight of the font used to render the event.'
          end
        end
      end
      post do
        calendar = get_calendar(organization_subdomain: params[:organization_subdomain], calendar_id: params[:calendar_id], writeable: true)
        attributes = params.slice(:item_id, :starts_at, :length, :partition, :use_custom_styles)
        inherited_attributes = calendar.attributes.slice("interrupted_by_holidays", "interrupted_by_weekends")
        partition = params[:partition]
        if params[:partition].blank?
          category = Item.find(params[:item_id]).category
          partition = category.nil? ? 0 : category.partition
        end
        event = Event.new(attributes.merge(inherited_attributes).merge({partition: partition}))
        event.style = Style.create!(params[:style]) if event.uses_custom_styles?
        calendar.events << event
        event.save!
        event.insert_at(params[:position]) unless params[:position].blank?
        event
      end


      desc 'Updates an existing event.'
      oauth2
      params do
        requires :id, type: Integer, desc: 'Event id.'
        requires :organization_subdomain, type: String, desc: 'The organization the calendar belongs to.'
        requires :calendar_id, type: Integer, desc: 'The calendar the event belongs to.'
        requires :starts_at, type: DateTime, desc: 'The time the event starts.'
        requires :length, type: Integer, desc: 'The length the event should occur in days.'
        requires :partition, type: Integer, desc: 'Determines the track to display this event in a given week on the calendar.'
        requires :use_custom_styles, type: Boolean, desc: 'If true this item uses custom styles.'
        optional :position, type: Integer, desc: 'The line ranking for the calendar item on the client. Lower values appear on top.'
        optional :name, type: String, desc: 'The name of the event (optional as it inheritly is the item name).'
        given :use_custom_styles do
          requires :style, type: Hash do
            requires :fill_color, type: String, desc: 'The color of the fill for the event.'
            requires :outline_color, type: String, desc: 'The color of the stroke around the event.'
            requires :text_alignment, type: String, desc: 'The alignment of the text.'
            requires :text_color, type: String, desc: 'The color of the text.'
            requires :text_decoration, type: String, desc: 'The decoration of the text.'
            requires :font, type: String, desc: 'The font used to render the item.'
            requires :font_size, type: String, desc: 'The size of the font used to render the event.'
            requires :font_style, type: String, desc: 'The style of the font used to render the event.'
            requires :font_weight, type: String, desc: 'The weight of the font used to render the event.'
          end
        end
      end
      route_param :id do
        put do
          calendar = get_calendar(organization_subdomain: params[:organization_subdomain], calendar_id: params[:calendar_id], writeable: true)
          event = calendar.events.not_deleted.find(params[:id])
          attributes = params.slice(:item_id, :starts_at, :ends_at, :partition, :use_custom_styles, :name, :length)
          event.use_custom_styles = params[:use_custom_styles]
          if event.needs_new_style?
            event.style = Style.create!(params[:style])
          elsif event.uses_custom_styles?
            event.style.update_attributes!(params[:style])
          end
          event.update_attributes!(attributes)
          event.insert_at(params[:position]) unless params[:position].blank?
          event
        end
      end


      desc 'Updates the position of an existing event.'
      oauth2
      params do
        requires :starts_at, type: DateTime, desc: 'The time the event starts.'
        requires :length, type: Integer, desc: 'The length of the event.'
        requires :id, type: Integer, desc: 'The id of the event to be shifted.'
        optional :partition, type: Integer, desc: 'Determines the track to display this event in a given week on the calendar.'
        requires :position, type: Integer, desc: 'The line ranking for the calendar item on the client. Lower values appear on top.'
      end
      route_param :id do
        patch 'shift' do
          calendar = get_calendar(organization_subdomain: params[:organization_subdomain], calendar_id: params[:calendar_id], writeable: true)
          event = calendar.events.not_deleted.find(params[:id])
          event.update_attributes!(
            starts_at: params[:starts_at],
            length: params[:length],
            partition: params[:partition] || event.partition)
          event.insert_at(params[:position]) unless params[:position].blank?
          event
        end
      end


      desc 'Adjusts the dates of an existing event(s) by an incremental offset in days.'
      oauth2
      params do
        requires :ids, type: Array[Integer], desc: 'The ids of the events to be adjusted.'
        requires :offset, type: Integer, desc: 'The amount the event(s) should be offset in days.'
        optional :partition, type: Integer, desc: 'Determines the track to display this event in a given week on the calendar.'
        requires :duplicate, type: Boolean, desc: 'If true the existing events will be left unchanged and copies will be created with the applied offset.'
      end
      patch 'offset' do
        calendar = get_calendar(organization_subdomain: params[:organization_subdomain], calendar_id: params[:calendar_id], writeable: true)
        events = calendar.events.not_deleted.find(params[:ids])
        events.map! { |e| e.deep_copy } if params[:duplicate]
        events.each { |e| e.update_attributes!(starts_at: e.starts_at + params[:offset].days, ends_at: e.ends_at + params[:offset].days, partition: params[:partition] || e.partition) }
        events
      end


      desc 'Deletes an existing event.'
      oauth2
      params do
        requires :calendar_id, type: Integer, desc: 'The id of the calendar the event belongs to.'
        requires :organization_subdomain, type: String, desc: 'The organization the calendar belongs to.'
        requires :id, type: Integer, desc: 'The id of the event to be deleted.'
      end
      route_param :id do
        delete do
          calendar = get_calendar(organization_subdomain: params[:organization_subdomain], calendar_id: params[:calendar_id], writeable: true)
          event = calendar.events.not_deleted.find(params[:id])
          event.update_attributes({is_deleted: true})
          event.destroy!
          event
        end
      end


      desc 'Deletes a group of existing events.'
      oauth2
      params do
        requires :calendar_id, type: Integer, desc: 'The id of the calendar the event belongs to.'
        requires :organization_subdomain, type: String, desc: 'The organization the calendar belongs to.'
        requires :ids, type: Array[Integer], desc: 'The ids of the events to be deleted.'
      end
      patch do
        calendar = get_calendar(organization_subdomain: params[:organization_subdomain], calendar_id: params[:calendar_id], writeable: true)
        events = calendar.events.not_deleted.find(params[:ids])
        events.each {|event| event.update_attributes({is_deleted: true}) }
        events
      end


    end
  end
end
