module FilmCal
  class V1::Holidays < Grape::API
    include FilmCal::V1::Defaults
    resource :holidays do


      helpers do
        def find_organization(subdomain)
          if current_user.super_admin?
            organization = Organization.where(subdomain: subdomain).first!
          else
            organization = current_user.organizations.where(subdomain: subdomain).first!
          end
          organization
        end

        def get_calendar(organization_subdomain:, calendar_id:, writeable: false)
          query = CalendarQuery.new(
            user: current_user,
            organization: find_organization(organization_subdomain)
          )
          if writeable
            return query.writeable_calendars.find(calendar_id)
          else
            return query.readable_calendars.find(calendar_id)
          end
        end
      end


      desc 'Returns a list of available holidays.'
      oauth2
      params do
        requires :organization_subdomain, type: String, desc: 'The organization the calendar belong to.'
        requires :calendar_id, type: String, desc: 'The calendar the holidays belong to.'
      end
      get do
        get_calendar(
          organization_subdomain: params[:organization_subdomain],
          calendar_id: params[:calendar_id]
        ).holidays.not_deleted.all
      end


      desc 'Return a specific holiday.'
      oauth2
      params do
        requires :id, type: Integer, desc: 'The id of the holiday.'
        requires :organization_subdomain, type: String, desc: 'The organization the calendar belongs to.'
        requires :calendar_id, type: String, desc: 'The calendar the holidays belong to.'
      end
      route_param :id do
        get do
          get_calendar(
            organization_subdomain: params[:organization_subdomain],
            calendar_id: params[:calendar_id]
          ).holidays.not_deleted.find(params[:id])
        end
      end


      desc 'Creates a new holiday.'
      oauth2
      params do
        requires :organization_subdomain, type: String, desc: 'The organization the calendar belongs to.'
        requires :calendar_id, type: Integer, desc: 'The id of the calendar the holiday belongs to.'
        requires :starts_at, type: DateTime, desc: 'The date the holiday starts.'
        requires :length, type: Integer, desc: 'The length in days the holiday may run.'
        requires :name, type: String, desc: 'The name of the holiday.'
      end
      post serializer: ExtendedHolidaySerializer do
        calendar = get_calendar(organization_subdomain: params[:organization_subdomain], calendar_id: params[:calendar_id])
        holiday = Holiday.create!(
          calendar_id: params[:calendar_id],
          starts_at: params[:starts_at],
          length: params[:length],
          name: params[:name]
        )
        calendar.holidays << holiday
        holiday.save!
        holiday.update_affected_events!
        holiday
      end


      desc 'Updates an existing holiday.'
      oauth2
      params do
        requires :organization_subdomain, type: String, desc: 'The organization the calendar belongs to.'
        requires :calendar_id, type: String, desc: 'The calendar the item belongs to.'
        requires :id, type: Integer, desc: 'The id of the holiday to update.'
        requires :starts_at, type: DateTime, desc: 'The date the holiday starts.'
        requires :length, type: Integer, desc: 'The length in days the holiday may run.'
        requires :name, type: String, desc: 'The name of the holiday.'
      end
      route_param :id do
        put serializer: ExtendedHolidaySerializer do
          calendar = get_calendar(organization_subdomain: params[:organization_subdomain], calendar_id: params[:calendar_id], writeable: true)
          holiday = calendar.holidays.find(params[:id])
          holiday.update_attributes!(name: params[:name], starts_at: params[:starts_at], length: params[:length])
          holiday.update_affected_events!
          holiday
        end
      end


      desc 'Deletes an existing holiday.'
      oauth2
      params do
        requires :calendar_id, type: Integer, desc: 'The id of the calendar the item belongs to.'
        requires :organization_subdomain, type: String, desc: 'The organization the calendar belongs to.'
        requires :id, type: Integer, desc: 'The id of the holiday to be deleted.'
      end
      route_param :id do
        delete serializer: ExtendedHolidaySerializer do
          calendar = get_calendar(organization_subdomain: params[:organization_subdomain], calendar_id: params[:calendar_id], writeable: true)
          holiday = calendar.holidays.not_deleted.find(params[:id])
          holiday.update_attributes({is_deleted: true})
          holiday.destroy!
          holiday.update_affected_events!
          holiday
        end
      end


    end
  end
end
