module FilmCal
  class V1::Reports < Grape::API
    include FilmCal::V1::Defaults
    resource :reports do

      helpers do
        def find_organization(subdomain)
          if current_user.super_admin?
            organization = Organization.includes(:reports).where(subdomain: subdomain).first!
          else
            organization = current_user.organizations.includes(:reports).where(subdomain: subdomain).first!
          end
          organization
        end
      end


      desc 'Returns a list of available reports'
      oauth2
      params do
        requires :organization_subdomain, type: String, desc: 'The organization the reports belong to.'
      end
      get do
        find_organization(params[:organization_subdomain]).reports.all
      end


      desc 'Return a specific report.'
      oauth2
      params do
        requires :id, type: Integer, desc: 'Report id.'
        requires :organization_subdomain, type: String, desc: 'The organization the report belongs to.'
      end
      route_param :id do
        get serializer: ExtendedReportSerializer do
          find_organization(params[:organization_subdomain]).reports.find(params[:id])
        end
      end


      desc 'Return a group of calendars for a specific report.'
      oauth2
      params do
        requires :id, type: Integer, desc: 'Report id.'
        requires :organization_subdomain, type: String, desc: 'The organization the report belongs to.'
      end
      route_param :id do
        get 'calendars', serializer: ExtendedCalendarSerializer do
          report = find_organization(params[:organization_subdomain]).reports.find(params[:id])
          report.calendars
        end
      end


      desc 'Posts a new report.'
      oauth2
      params do
        requires :name, type: String, desc: 'The name of the report.'
        requires :organization_subdomain, type: String, desc: 'The organization the report belongs to.'
      end
      post do
        organization = find_organization(params[:organization_subdomain])
        error_unless_admin!(params[:organization_subdomain])
        report = Report.new(name: params[:name])
        organization.reports << report
        report.save!
        report
      end


      desc 'Updates an existing report.'
      oauth2
      params do
        requires :id, type: Integer, desc: 'Report id.'
        requires :organization_subdomain, type: String, desc: 'The organization the report belongs to.'
        requires :name, type: String, desc: 'The name of the report.'
      end
      route_param :id do
        put do
          error_unless_admin!(params[:organization_subdomain])
          report = find_organization(params[:organization_subdomain]).reports.find(params[:id])
          report.update_attributes!(name: params[:name])
          report
        end
      end


      desc 'Deletes an existing report.'
      oauth2
      params do
        requires :report_id, type: Integer, desc: 'The id of the report to be deleted.'
        requires :organization_subdomain, type: String, desc: 'The organization the report belongs to.'
      end
      route_param :report_id do
        delete do
          error_unless_admin!(params[:organization_subdomain])
          report = find_organization(params[:organization_subdomain]).reports.find(params[:report_id])
          report.update_attributes({is_deleted: true})
          report.destroy!
          report
        end
      end


      desc 'Adds a calendar to the report.'
      oauth2
      params do
        requires :report_id, type: Integer, desc: 'The id of the report to be deleted.'
        requires :calendar_id, type: Integer, desc: 'The id of the calendar to add to the report.'
        requires :organization_subdomain, type: String, desc: 'The organization the report belongs to.'
      end
      route_param :report_id do
        segment :add_calendar do
          route_param :calendar_id do
            post do
              error_unless_admin!(params[:organization_subdomain])
              report = find_organization(params[:organization_subdomain]).reports.find(params[:report_id])
              report_calendar = report.report_calendars.find_or_create_by!(calendar_id: params[:calendar_id])
              report_calendar
            end
          end
        end
      end


      desc 'Removes a calendar to the report.'
      oauth2
      params do
        requires :report_id, type: Integer, desc: 'The id of the report to be deleted.'
        requires :calendar_id, type: Integer, desc: 'The id of the calendar to add to the report.'
        requires :organization_subdomain, type: String, desc: 'The organization the report belongs to.'
      end
      route_param :report_id do
        segment :remove_calendar do
          route_param :calendar_id do
            delete do
              error_unless_admin!(params[:organization_subdomain])
              report = find_organization(params[:organization_subdomain]).reports.find(params[:report_id])
              report_calendar = report.report_calendars.find_by_calendar_id!(params[:calendar_id])
              report_calendar.update_attributes({is_deleted: true})
              report_calendar.destroy!
              report_calendar
            end
          end
        end
      end


    end
  end
end
