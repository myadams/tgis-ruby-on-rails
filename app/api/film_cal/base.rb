require 'raven/integrations/rack'

module FilmCal
  class Base < Grape::API
    use Raven::Rack
    use GrapeLogging::Middleware::RequestLogger, instrumentation_key: 'grape_key',
                                                 include: [GrapeLogging::Loggers::FilterParameters.new]
    prefix 'api'
    default_format :json
    format :json
    use ::WineBouncer::OAuth2

    mount FilmCal::V1::RootV1

    def self.cors_response_to_error(e)
      logger.error e.to_yaml unless Rails.env.test? # Breaks tests...
      eclass = e.class.to_s
      message = "OAuth error: #{e}" if eclass =~ /WineBouncer::Errors/

      opts = {
        error: message || e.message,
        errors: self.errors_for(e, eclass) || 'no errors listed...'
      }

      opts[:trace] = e.backtrace[0, 10] unless Rails.env.production?
      Rack::Response.new(opts.to_json, status_code_for(e, eclass), {
                           'Content-Type' => 'application/json',
                           'Access-Control-Allow-Origin' => '*',
                           'Access-Control-Request-Method' => '*'
                         }).finish
    end

    def self.map_active_record_errors(errors)
      errors.messages.map {|k,v|
        {k => v.join(' and ')}
      }.inject({}){|memo, v|
        memo.merge(v)
      }
    end

    def self.errors_for(error, eclass)
      if eclass =~ /RecordInvalid/
        return  self.map_active_record_errors(error.record.errors)
      elsif eclass =~ /Grape::Exceptions::ValidationErrors/
        return error.errors.map{ |e|
          e.last.first
        }.each_with_object({}){ |e, memo|
          memo[e.params.first] = e.message
        }
      end
    end

    def self.status_code_for(error, eclass)
      if eclass =~ /OAuthUnauthorizedError/
        401
      elsif (eclass =~ /OAuthForbiddenError/) || (eclass =~ /ExpiredSignature/)
        403
      elsif (eclass =~ /RecordNotFound/) || (error.message =~ /unable to find/i)
        404
      elsif eclass =~ /RecordInvalid/
        422
      else
        (error.respond_to? :status) && error.status || 500
      end
    end

  end
end
