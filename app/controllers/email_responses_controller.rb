require 'aws-sdk'
require 'json'

class EmailResponsesController < ApplicationController

  before_action :log_incoming_message

  def bounce
    Rails.logger.info "Received possible bounce: "
    Rails.logger.info request.raw_post
    if !verifier.authentic?(request.raw_post)
      Rails.logger.info "Not a valid post"
    elsif subscribe_url
      confirm_subscribe_url
    elsif type != 'Bounce'
      Rails.logger.info "Not a bounce - exiting"
    else
      bounce = message['bounce']
      bouncerecps = bounce['bouncedRecipients']
      bouncerecps.each do |recp|
        email = recp['emailAddress']
        extra_info  = "status: #{recp['status']}, action: #{recp['action']}, diagnosticCode: #{recp['diagnosticCode']}"
        Rails.logger.info "Creating a bounce record for #{email}"
        EmailResponse.create ({ email: email, response_type: 'bounce', extra_info: extra_info})
      end
    end

    render_ok
  end

  def complaint
    Rails.logger.info "Received possible complaint: "
    Rails.logger.info request.raw_post
    if !verifier.authentic?(request.raw_post)
      Rails.logger.info "Not a valid post"
    elsif subscribe_url
      confirm_subscribe_url
    elsif type != 'Complaint'
      Rails.logger.info "Not a complaint - exiting"
    else
      complaint = message['complaint']
      recipients = complaint['complainedRecipients']
      recipients.each do |recp|
        email = recp['emailAddress']
        extra_info = "complaintFeedbackType: #{complaint['complaintFeedbackType']}"
        EmailResponse.create ( { email: email, response_type: 'complaint', extra_info: extra_info } )
      end
    end

    render_ok
  end

  protected

  def confirm_subscribe_url
    Rails.logger.info "AWS is requesting confirmation of the bounce handler URL"
    uri = URI.parse(subscribe_url)
    http = Net::HTTP.new(uri.host, uri.port)
    http.use_ssl = true
    http.verify_mode = OpenSSL::SSL::VERIFY_NONE
    http.get(uri.request_uri)
  end

  def render_ok
    response.headers['Cache-Control'] = 'no-cache'
    render json: ''
  end

  def verifier
    @verifier ||= Aws::SNS::MessageVerifier.new
  end

  def log_incoming_message
    Rails.logger.info request.raw_post
  end

  def message
    @message ||= JSON.parse JSON.parse(request.raw_post)['Message']
  end

  def subscribe_url
    @subscribe_url ||= JSON.parse(request.raw_post)['SubscribeURL']
  end

  def type
    message['notificationType']
  end
end
