class CalendarQuery

  def initialize(user:, organization:)
    @user = user
    @organization = organization
  end

  def ignores_permissions?
    @user.admin?(@organization.subdomain) # || !@organization.uses_granular_permissions
  end

  def all_calendars
    @calendars ||= @organization.calendars if @user.super_admin?
  end

  def readable_calendars
    @readable ||= fetch_calendars(access: :read)
  end

  def writeable_calendars
    @writeable ||= fetch_calendars(access: :write)
  end

  protected

    def fetch_calendars(access: :read)
      return all_calendars unless all_calendars.nil?
      if ignores_permissions?
        return @user.calendars.within_organization(@organization)
      elsif access == :read
        return @user.readable_calendars.within_organization(@organization)
      elsif access == :write
        return @user.writeable_calendars.within_organization(@organization)
      end
    end

end
