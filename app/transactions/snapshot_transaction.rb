class SnapshotTransaction

  def initialize(calendar: nil, name: nil, notes: nil, user_id: nil, snapshot_id: nil)
    if snapshot_id.blank?
      @snapshot = Snapshot.new(name: name, notes: notes, user_id: user_id)
    else
      @snapshot = Snapshot.find(snapshot_id)
    end
    @calendar = calendar || @snapshot.calendar
  end

  def run!
    copy!
  end

  def result
    @snapshot
  end

  protected

  def copy!
    Snapshot.transaction do
      @snapshot.calendar_id = @calendar.id
      @snapshot.style = copy_style(@calendar.style)
      @snapshot.categories << @calendar.categories.top_level.map{|c| copy_category(c)}
      @snapshot.holidays << @calendar.holidays.map{|h| copy_holiday(h)}
      @snapshot.items << @calendar.items.top_level.map{|i| copy_item(i)}
      @snapshot.save
    end
  end

  def copy_category(old_category)
    keys = [:name, :use_custom_styles, :position, :is_deleted, :interrupted_by_weekends, :interrupted_by_holidays]
    new_category = Category.new(old_category.attributes.symbolize_keys.slice(*keys))
    new_category.style = copy_style(old_category.style) unless old_category.style.blank?
    @snapshot.categories << new_category
    new_category.items << old_category.items.map{|i| copy_item(i)}
    new_category.categories << old_category.categories.map{|c| copy_category(c)}
    new_category
  end

  def copy_item(old_item)
    keys = [:name, :active, :is_deleted, :active, :locked, :use_custom_styles, :interrupted_by_weekends, :interrupted_by_holidays]
    new_item = Item.new(old_item.attributes.symbolize_keys.slice(*keys))
    new_item.style = copy_style(old_item.style) unless old_item.style.blank?
    @snapshot.items << new_item
    new_item.events << old_item.events.map{|e| copy_event(e)}
    new_item
  end

  def copy_holiday(old_holiday)
    keys = [:name, :starts_at, :is_deleted, :ends_at, :length]
    new_holiday = Holiday.new(old_holiday.attributes.symbolize_keys.slice(*keys))
    @snapshot.holidays << new_holiday
    new_holiday
  end

  def copy_event(old_event)
    keys = [
      :is_deleted, :starts_at, :ends_at, :position, :name, :use_custom_styles,
      :partition, :length, :interruptions, :interrupted_by_weekends, :interrupted_by_holidays
    ]
    new_event = Event.new(old_event.attributes.symbolize_keys.slice(*keys))
    new_event.style = copy_style(old_event.style) unless old_event.style.blank?
    @snapshot.events << new_event
    new_event
  end

  def copy_style(old_style)
    keys = [
      :fill_color, :outline_color, :font, :font_style, :font_size, :font_weight,
      :text_alignment, :text_decoration, :text_color
    ]
    new_style = Style.new(old_style.attributes.symbolize_keys.slice(*keys))
    @snapshot.styles << new_style
    new_style
  end

end
