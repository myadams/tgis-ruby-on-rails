class DuplicateTransaction

  def initialize(sourceCalendar:, targetCalendar:, includeEvents: false)
    @sourceCalendar = sourceCalendar
    @targetCalendar = targetCalendar
    @includeEvents = includeEvents
  end

  def run!
    copy!
  end

  def result
    @targetCalendar
  end

  protected

  def copy!
    Calendar.transaction do
      @targetCalendar.style = copy_style(@sourceCalendar.style)
      @targetCalendar.categories << @sourceCalendar.categories.top_level.map{|c| copy_category(c)}
      @targetCalendar.items << @sourceCalendar.items.top_level.map{|i| copy_item(i)}
      @targetCalendar.save
    end
  end

  def copy_category(old_category)
    keys = [:name, :use_custom_styles, :position, :is_deleted]
    new_category = Category.new(old_category.attributes.symbolize_keys.slice(*keys))
    new_category.style = copy_style(old_category.style) unless old_category.style.blank?
    @targetCalendar.categories << new_category
    new_category.items << old_category.items.map{|i| copy_item(i)}
    new_category.categories << old_category.categories.map{|c| copy_category(c)}
    new_category
  end

  def copy_item(old_item)
    keys = [:name, :active, :is_deleted, :active, :locked, :use_custom_styles]
    new_item = Item.new(old_item.attributes.symbolize_keys.slice(*keys))
    new_item.style = copy_style(old_item.style) unless old_item.style.blank?
    @targetCalendar.items << new_item
    new_item.events << old_item.events.map{|e| copy_event(e)} if @includeEvents
    new_item
  end

  def copy_event(old_event)
    keys = [
      :is_deleted, :starts_at, :ends_at, :position, :name, :use_custom_styles,
      :partition, :length
    ]
    new_event = Event.new(old_event.attributes.symbolize_keys.slice(*keys))
    new_event.style = copy_style(old_event.style) unless old_event.style.blank?
    @targetCalendar.events << new_event
    new_event
  end

  def copy_style(old_style)
    keys = [
      :fill_color, :outline_color, :font, :font_style, :font_size, :font_weight,
      :text_alignment, :text_decoration, :text_color
    ]
    new_style = Style.new(old_style.attributes.symbolize_keys.slice(*keys))
    @targetCalendar.styles << new_style
    new_style
  end

end
