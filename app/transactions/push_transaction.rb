class PushTransaction

  def initialize(events:, offset:, target_event: nil, fill_unused_days: false)
    @target_event = target_event
    @events = events
    @offset = offset
    @affected_events = []
    @fill_unused_days = fill_unused_days
  end

  def run!
    return complex_offset! if @fill_unused_days && (@offset > 0)
    simple_offset!
  end

  def result
    @affected_events
  end

  protected

  def calculate_offset_days(date, offset)
    target = date.wday + offset
    return offset
  end

  def offset_date(date, offset)
    date + calculate_offset_days(date, offset).days
  end

  def offset_event(event, offset)
    return event if offset == 0
    new_starts_at = offset_date(event.starts_at, offset)
    if !@target_event.nil? && event.id == @target_event.id
      event.starts_at = (offset < 0) ? new_starts_at : event.starts_at
      event.length = event.length + offset.abs
    else
      event.starts_at = new_starts_at
    end
    # Incremental calculations occur naturally otherwise.
    event.calculate_interruptions!(bias: :decrement) if offset < 0
    event.save!
    event
  end

  def simple_offset!
    @affected_events = @events.map { |event| offset_event(event, @offset) }
  end

  def complex_offset!
    remaining_offset = @offset
    unused_days = map_unused_days
    @affected_events = @events.map.with_index { |event,i|
      available_space = unused_days[i]
      remaining_offset = remaining_offset - available_space
      next if remaining_offset < 1
      offset_event(event, remaining_offset)
    }.reject(&:blank?)
  end

  def map_unused_days
    @events.map.with_index do |event,i|
      (i < 1) ? 0 : space_between_events(event, @events[i-1])
    end
  end

  def space_between_events(event, prior_event)
    start_date = event.starts_at.to_date
    end_date = prior_event.ends_at.to_date
    diff = (start_date - end_date).to_i - 1
    intersecting_weekends = !@interrupted_by_weekends ? ((end_date.wday + diff)/6).floor : 0
    diff -= (intersecting_weekends*2)
    [0, diff].max
  end

end
