class NotificationsMailer < ApplicationMailer

  def invitation(invitation, token)
    @invitation = invitation
    @token = token
    @organization = invitation.organization
    @url = invitation.client_url(@token)
    @days_until_expires = ENV["INVITATION_TOKEN_LIFESPAN_DAYS"]
    mail to: invitation.email,
         from: ENV['SES_DEFAULT_EMAIL_ADDRESS'],
         subject: "Invitation to ProCal"
  end

  def forgot_password(user, token)
    @user = user
    @url = "#{client_url}reset-password?token=#{token}"
    mail to: user.email,
         from: ENV['SES_DEFAULT_EMAIL_ADDRESS'],
         subject: "Reset Your ProCal Password"
  end

  def confirm_password_reset(user)
    @user = user
    mail to: user.email,
         from: ENV['SES_DEFAULT_EMAIL_ADDRESS'],
         subject: "Your ProCal Password was Successfully Reset!"
  end

  protected

  def client_url()
    domain = ENV['CLIENT_APP_DOMAIN']
    if ENV['CLIENT_APP_ORG_USE_SUBDOMAIN'] != 'true'
      host = "https://#{domain}/"
    else
      host = organization.client_url
    end
    host
  end

end
