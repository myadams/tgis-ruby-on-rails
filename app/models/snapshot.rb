class Snapshot < ApplicationRecord
  include Deletable
  include Styleable

  has_many :events
  has_many :categories
  has_many :items
  has_many :styles
  has_many :holidays

  belongs_to :calendar
  belongs_to :user

  validates :name, presence: true, length: {minimum: 1}

  def created_by
    user.invitations.where(organization_id: calendar.organization_id).name
  end

  def styleable_children
    return [self.items.where(category_id: nil), self.categories.where(category_id: nil)]
  end

  def inherit_styles_from
    nil
  end

  def uses_custom_styles?
    return true
  end

end
