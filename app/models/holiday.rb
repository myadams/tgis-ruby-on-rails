class Holiday < ApplicationRecord

  include Deletable

  belongs_to :calendar
  belongs_to :snapshot

  before_save :update_ends_at!

  validates :name, presence: true, length: {minimum: 1}

  scope :for_calendar, ->(calendar_id) { where(calendar_id: calendar_id) }
  scope :for_duration, ->(duration) {
    ends_at = Holiday.arel_table[:ends_at]
    starts_at = Holiday.arel_table[:starts_at]
    where(starts_at: duration).or(where(ends_at: duration)).or(where(starts_at.lt(duration.first)).where(ends_at.gt(duration.last)))
  }
  scope :for_event, ->(event) { for_calendar(event.calendar_id).for_duration(event.duration) }

  def duration
    starts_at...ends_at
  end

  def update_ends_at!
    self.ends_at = self.starts_at + self.length.days - 1
  end

  def update_affected_events!
    # Simply saving the event triggers date calculations if applicable.
    events = Event.for_holiday(self) unless calendar.nil?
    @affected_events = events.nil? ? [] : events.map{ |event| event.save!; event }
  end

  def affected_events
    @affected_events
  end

end
