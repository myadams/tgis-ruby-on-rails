class Calendar < ApplicationRecord
  include Deletable
  include Styleable

  belongs_to :organization

  has_many :events
  has_many :categories
  has_many :holidays
  has_many :items
  has_many :styles
  has_many :granular_permissions
  has_many :snapshots

  after_create :generate_default_style!
  after_save :update_all_configurables!

  validates :name, uniqueness: {
    scope: [:organization_id, :is_deleted],
    message: 'is already in use by another calendar',
    if: :is_not_deleted?
  }

  scope :within_organization, -> (org) { where(organization_id: org.id) }

  def self.retrieve_from_token(token)
    decoded_token = JWT.decode token, ENV['JWT_SECRET'], true, { :algorithm => 'HS512' }
    Calendar.find(decoded_token[0]["calendar_id"])
  end

  def generate_token(exp: nil)
    exp = 1.minutes.from_now.to_i if exp.nil?
    payload = {
      calendar_id: id,
      exp: exp
    }
    token = JWT.encode payload, ENV['JWT_SECRET'], 'HS512'
  end

  def generate_default_style!
    self.style = Style.new(fill_color: "#FFFFFF", outline_color: "#000000", text_color: "#000000")
    self.styles << self.style
    self.save
  end

  def update_all_configurables!
    configure_inheritable_entities(categories.all)
    configure_inheritable_entities(items.all)
    configure_inheritable_entities(events.all)
  end

  def configure_inheritable_entities(entities)
    entities.each{|e| e.update_attributes(
      interrupted_by_holidays: interrupted_by_holidays,
      interrupted_by_weekends: interrupted_by_weekends
    )}
  end

  def styleable_children
    return [self.items.where(category_id: nil), self.categories.where(category_id: nil)]
  end

  def inherit_styles_from
    nil
  end

  def uses_custom_styles?
    return true
  end

  def present_categories
    categories.not_deleted
  end

  def present_items
    items.not_deleted
  end

  def present_events
    events.not_deleted
  end

end
