class Style < ApplicationRecord

  belongs_to :calendar
  belongs_to :snapshot

  HEX_FORMAT = /\A#?(?:[A-F0-9]{3}){1,2}\z/i

  validates :outline_color, format: {
    with: HEX_FORMAT,
    message: "must be a valid color."
  }
  validates :fill_color, format: {
    with: HEX_FORMAT,
    message: "must be a valid color."
  }
  validates :font, inclusion: {
    in: %w(Default Comic Courier Helvetica Lucida Times),
    message: "must be a supported font ('Default', 'Comic', 'Courier', 'Helvetica', 'Lucida', 'Times')."
  }
  validates :font_style, inclusion: {
    in: %w(normal italic),
    message: "must be a supported font style ('normal', 'italic')."
  }
  validates :font_weight, inclusion: {
    in: %w(normal bold),
    message: "must be a supported font style ('normal', 'bold')."
  }
  validates :font_size, numericality: { only_integer: true }
  validates :text_decoration, inclusion: {
    in: %w(none underline),
    message: "must be a supported text decoration ('none', 'underline')."
  }
  validates :text_alignment, inclusion: {
    in: %w(left center right),
    message: "must be a supported text alignment ('left', 'center', 'right')."
  }

end
