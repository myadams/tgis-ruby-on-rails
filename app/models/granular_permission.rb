class GranularPermission < ApplicationRecord
  belongs_to :invitation, dependent: :destroy
  belongs_to :calendar, dependent: :destroy

  validates :invitation_id, uniqueness: { scope: :calendar_id, message: 'already exists for this user and calendar combination' }
end
