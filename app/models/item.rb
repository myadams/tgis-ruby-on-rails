class Item < ApplicationRecord
  include Styleable
  include Deletable

  belongs_to :category
  belongs_to :calendar
  belongs_to :snapshot
  has_many :events

  validates :name, presence: true, length: {minimum: 1}

  scope :locked, -> { where(locked: true) }
  scope :unlocked, -> { where(locked: true) }
  scope :top_level, -> { where(category_id: nil) }

  def styleable_children
    return [self.events]
  end

  def inherit_styles_from
    self.category || self.calendar
  end

  def delete_including_children!
    self.update_attributes(is_deleted: true)
    self.events.update_all(is_deleted: true)
  end

end
