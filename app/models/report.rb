class Report < ApplicationRecord
  include Deletable
  belongs_to :organization
  has_many :report_calendars
  has_many :calendars, through: :report_calendars, class_name: 'Calendar', source: :calendar
end
