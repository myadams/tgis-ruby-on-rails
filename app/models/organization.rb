class Organization < ApplicationRecord
  has_many :calendars
  has_many :invitations
  has_many :users, through: :invitations
  has_many :granular_permissions, through: :invitations
  has_many :reports
  
  validates :name, uniqueness: true, presence: true
  validates :subdomain, uniqueness: true, presence: true
  validates :color, presence: true

  scope :alphabetical, -> { order(name: :asc) }
  scope :not_deleted, -> { where(is_deleted: false) }
  scope :deleted, -> { where(is_deleted: true) }

  def is_not_deleted?
    !self.is_deleted
  end

  def client_url
    domain = ENV['CLIENT_APP_DOMAIN']
    return "https://#{domain}/org/#{subdomain}" if ENV['CLIENT_APP_ORG_USE_SUBDOMAIN'] != 'true'
    "https://#{subdomain}.#{domain}"
  end

end
