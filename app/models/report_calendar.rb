class ReportCalendar < ApplicationRecord
  include Deletable
  belongs_to :report
  belongs_to :calendar
end
