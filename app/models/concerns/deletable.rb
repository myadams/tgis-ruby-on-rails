module Deletable extend ActiveSupport::Concern
  included do
    scope :not_deleted, -> { where(is_deleted: false) }
    scope :deleted, -> { where(is_deleted: true) }
  end

  def is_not_deleted?
    !self.is_deleted
  end
end
