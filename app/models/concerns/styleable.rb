module Styleable extend ActiveSupport::Concern
  included do
    belongs_to :style
    belongs_to :inherited_style, class_name: 'Style'
    before_save :clear_custom_style_if_needed!, :inherit_styles!
    after_save :update_styleable_children!
  end

  def clear_custom_style_if_needed!
    if !self.uses_custom_styles?
      self.style_id = nil
      self.style.destroy unless self.style.nil?
    end
  end

  def inherit_styles!
    return nil if self.inherit_styles_from.nil? || uses_custom_styles?
    self.inherited_style_id = self.inherit_styles_from.applicable_style_id
  end

  def update_styleable_children!
    return if styleable_children.nil?
    self.styleable_children.each do |rel|
      rel.each{|entity| entity.update_attributes({inherited_style_id: self.applicable_style_id})}
    end
  end

  def applicable_style_id
    (self.applicable_style && self.applicable_style.id) || nil
  end

  def applicable_style
    (self.uses_custom_styles? && self.style) || self.inherited_style
  end

  def uses_custom_styles?
    raise "A styleable object must define 'use_custom_styles' to return true or false indicative of whether to pass along it's own custom or inherited style." unless self.respond_to?(:use_custom_styles)
    return self.use_custom_styles
  end

  def needs_new_style?
    (self.use_custom_styles_changed? && self.uses_custom_styles?) || (self.uses_custom_styles? && self.style_id.nil?)
  end

  #
  # Required mehtods by concern.
  #

  def styleable_children
    raise "A styleable object must define 'styleable_children' to return an array of ActiveRecord relationships."
  end

  def inherit_styles_from
    raise "A styleable object must define 'inherit_styles_from' to determine where to obtain it's inherited styles from."
  end

end
