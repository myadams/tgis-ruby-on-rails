class Invitation < ApplicationRecord
  belongs_to :organization
  belongs_to :user, inverse_of: :invitations

  has_many :granular_permissions, inverse_of: :invitation
  has_many :calendars

  # Granular readable permissions
  has_many :readable_granular_permissions, -> { where read: true }, class_name: 'GranularPermission'
  has_many :readable_calendars, through: :readable_granular_permissions, class_name: 'Calendar', source: :calendar

  # Granular writeable permissions
  has_many :writeable_granular_permissions, -> { where write: true }, class_name: 'GranularPermission'
  has_many :writeable_calendars, through: :writeable_granular_permissions, class_name: 'Calendar', source: :calendar

  validates :email,
    email_format: { :message => 'is not a valid address' },
    uniqueness: { scope: :organization_id, message: 'has already been invited' },
    presence: true

  validates :name,
    presence: true

  validates :user_id,
    uniqueness: { scope: :organization_id, allow_blank: true, message: 'has already been invited' }

  validates :organization_id,
    presence: true

  after_validation :bind_to_existing_user!

  scope :not_deleted, -> { where(is_deleted: false) }
  scope :accepted, -> { where(accepted: true) }
  scope :pending, -> { where(accepted: false) }

  def is_super_admin
    return read_attribute(:is_super_admin) if user.nil?
    user.is_super_admin
  end

  def bind_to_existing_user!
    users = User.where(email: email)
    self.user_id = users.first.id if users.length > 0
  end

  def access_level
    return 'super' if is_super_admin
    return 'admin' if is_admin == true
    'user'
  end

  def generate_token(exp: nil)
    exp = 1.days.from_now if exp.nil?
    payload = {
      invitation_id: id,
      organization: {
        id: organization.id,
        name: organization.name,
        subdomain: organization.subdomain
      },
      exp: exp.to_i
    }
    token = JWT.encode payload, ENV['JWT_SECRET'], 'HS512'
  end

  def for_existing_user?
    !user_id.nil?
  end

  def self.retrieve_from_token(token)
    decoded_token = JWT.decode token, ENV['JWT_SECRET'], true, { :algorithm => 'HS512' }
    Invitation.find(decoded_token[0]["invitation_id"])
  end

  def client_url(token)
    domain = ENV['CLIENT_APP_DOMAIN']
    if ENV['CLIENT_APP_ORG_USE_SUBDOMAIN'] == 'false'
      host = "https://#{domain}/"
    else
      host = organization.client_url
    end
    "#{host}invitation?invitation=#{token}"
  end

end
