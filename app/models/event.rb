class Event < ApplicationRecord
  include Styleable
  include Deletable

  INTERRUPT_DATE_FORMAT = "%Y-%m-%d"

  belongs_to :calendar
  belongs_to :snapshot
  belongs_to :item

  before_validation :calculate_naive_end_date!
  after_validation :calculate_interruptions!

  validate :ends_at_cannot_occur_before_starts_at
  validates :ends_at, presence: true
  validates :starts_at, presence: true
  validates :length, numericality: { only_integer: true, greater_than: 0, message: "Must be at least 1 day long." }

  acts_as_list scope: :calendar

  scope :unlocked, -> { joins(:item).where(items: { locked: false }) }
  scope :locked, -> { joins(:item).where(items: { locked: true }) }
  scope :chronological, -> { order(starts_at: :asc) }
  scope :for_categories, ->(category_ids) { joins(:item).where(items: { category_id: category_ids }) }
  scope :for_items, ->(item_ids) { where(item_id: item_ids) }
  scope :for_calendar, ->(calendar_id) { where(calendar_id: calendar_id) }
  scope :for_duration, ->(duration) {
    ends_at = Event.arel_table[:ends_at]
    starts_at = Event.arel_table[:starts_at]
    where(starts_at: duration).or(where(ends_at: duration)).or(where(starts_at.lt(duration.first)).where(ends_at.gt(duration.last)))
  }

  def self.for_holiday(holiday)
    holiday.calendar.events.for_duration(holiday.duration)
  end

  def duration
    starts_at..ends_at
  end

  def is_not_interrupted?
    !interrupted_by_holidays? && !interrupted_by_weekends
  end

  def calculate_interruptions!(bias: :increment, persist_existing_interruptions: false)
    self.interruptions = [] unless persist_existing_interruptions
    self.interruptions = overlapping_weekends
      .append(overlapping_holidays)
      .flatten.map{|d| d.iso8601}.uniq
    initial_end_date = self.ends_at
    if start_date_interrupted?
      calculate_true_start_date!(direction: bias)
      self.interruptions = []
      calculate_interruptions!(bias: bias, persist_existing_interruptions: false)
    end
    calculate_true_end_date!
    calculate_interruptions!(bias: bias, persist_existing_interruptions: true) if self.ends_at != initial_end_date
  end

  def calculate_naive_end_date!
    self.ends_at = starts_at+(length-1).days
  end

  def start_date_interrupted?
    interruptions.include?(starts_at.iso8601)
  end

  def calculate_true_start_date!(direction: :increment)
    loop do
      break unless start_date_interrupted?
      offset = (direction == :increment ? 1 : -1)
      self.starts_at += offset.days
    end
  end

  def calculate_true_end_date!
    interrupted_days = (interruptions.blank? ? 0 : interruptions.length).days
    self.ends_at = self.starts_at + (length - 1).days + interrupted_days unless starts_at.nil?
  end

  def overlapping_weekends
    return [] if !interrupted_by_weekends?
    calculate_naive_end_date! if self.ends_at.nil?
    interrupted_days = (interruptions.blank? ? 0 : interruptions.length)
    padding_for_sunday = (ends_at.wday == 6 ? 1 : 0)
    working_length = length + interrupted_days + padding_for_sunday
    weekends = Array.new(working_length) {|i| starts_at + i.days}.select{|date| [0,6].include?(date.wday)}
    weekends
  end

  def overlapping_holidays
    return [] if !interrupted_by_holidays?
    calculate_naive_end_date! if self.ends_at.nil?
    Holiday.for_calendar(calendar_id).for_duration(duration).map{|h| Array.new(h.length) {|i| h.starts_at + i.days}}.flatten.uniq
  end

  def styleable_children
    return nil
  end

  def inherit_styles_from
    self.item
  end

  def ends_at_cannot_occur_before_starts_at
    if ends_at.present? && starts_at.present? && ends_at < starts_at
      errors.add(:ends_at, "can't occur before start date")
    end
  end

  def end_date
    ends_at.strftime("%m-%d-%Y")
  end

  def start_date
    starts_at.strftime("%m-%d-%Y")
  end

  def deep_copy
    new_instance = Event.new(self.attributes.except("id"))
    if !self.style.nil?
      new_style = Style.new(self.style.attributes.except("id"))
      new_style.save
      new_instance.style = new_style
    end
    new_instance.save
    new_instance
  end

  def days_between_prior_event
    return 0 if calendar.nil?
    prior_event = calendar.events.where(['ends_at <= ?', starts_at]).order(ends_at: :desc).first
    prior_event.nil? ? 0 : (starts_at.to_date - prior_event.ends_at.to_date).to_i
  end

end
