class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  has_many :invitations, -> { where(accepted: true) }
  has_many :admin_invitations, -> { where(accepted: true, is_admin: true) }, class_name: 'Invitation'
  has_many :organizations, through: :invitations
  has_many :admin_organizations, through: :admin_invitations, class_name: 'Organization', source: :organization
  has_many :calendars, through: :organizations
  has_many :readable_calendars, through: :invitations, class_name: 'Calendar'
  has_many :writeable_calendars, through: :invitations, class_name: 'Calendar'
  has_many :granular_permissions, through: :invitations

  def admin?(subdomain)
    return super_admin? || admin_organization_list.include?(subdomain)
  end

  def super_admin?
    return is_super_admin
  end

  def generate_token(exp: nil)
    exp = 10.minutes.from_now.to_i if exp.nil?
    payload = {
      id: id,
      email: email,
      exp: exp
    }
    token = JWT.encode payload, ENV['JWT_SECRET'], 'HS512'
  end

  def admin_organization_list
    admin_organizations.map{ |o| o.subdomain }
  end

end
