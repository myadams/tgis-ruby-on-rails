class Category < ApplicationRecord
  include Styleable
  include Deletable

  belongs_to :snapshot
  belongs_to :calendar
  belongs_to :category

  has_many :items
  has_many :categories

  validates :name, presence: true, length: {minimum: 1}, uniqueness: { scope: [:calendar_id, :is_deleted, :snapshot_id], if: :is_not_deleted? }
  validate :category_is_not_nested

  scope :top_level, -> { where(category_id: nil) }

  def self.inherited_id_map(category_ids)
    categories = self.where(id: category_ids)
    categories.inject(category_ids.clone) do |ids_array, category|
      nested_ids = category.categories.map { |c| c.id }
      self.inherited_id_map(nested_ids).each { |id| ids_array.push(id) }
      ids_array.uniq
    end
  end

  def delete_including_children!
    self.update_attributes(is_deleted: true)
    self.categories.each{ |c| c.delete_including_children! }
    self.items.each {|i| i.delete_including_children! }
  end

  def styleable_children
    return [self.items, self.categories]
  end

  def inherit_styles_from
    self.category || self.calendar
  end

  def category_is_not_nested
    if !calendar.nil? && !category_id.nil? && Category.find(category_id).already_belongs_to_category(self)
      errors.add :category_id, "is a child of this category"
    end
  end

  def already_belongs_to_category(category)
    return false if category_id.nil?
    return true if category_id == category.id
    calendar.categories.find(category_id).already_belongs_to_category(category)
  end

end
