require 'rails_helper'

RSpec.describe CalendarQuery, type: :model do

  describe "readable_calendars and writeable_calendars" do

    let(:user) { create :user }
    let(:organization) { create :organization }
    let(:irrelevant_organization) { create :organization }
    let(:invitation) { create :invitation, user_id: user.id, organization_id: organization.id, accepted: true }
    let(:irrelevant_invitation) { create :invitation, user_id: user.id, organization_id: irrelevant_organization.id, accepted: true }
    let(:first_calendar) { create :calendar, organization_id: organization.id }
    let(:second_calendar) { create :calendar, organization_id: organization.id }
    let(:irrelevant_calendar) { create :calendar, organization_id: irrelevant_organization.id }
    before(:each) do
      organization.calendars << [first_calendar, second_calendar]
      user.invitations << [invitation, irrelevant_invitation]
      organization.invitations << invitation
      irrelevant_organization.invitations << irrelevant_invitation
      irrelevant_calendar.touch
      @query = CalendarQuery.new(user: user, organization: organization)
    end

    it 'should return no calendars if the organization utilizes granular permissions and none have been granted.' do
      expect(@query.readable_calendars.count).to eq(0)
      expect(@query.writeable_calendars.count).to eq(0)
    end

    it 'should return all calendars if the user is an admin' do
      invitation.update_attributes(is_admin: true)
      expect(@query.readable_calendars.count).to eq(2)
      expect(@query.writeable_calendars.count).to eq(2)
    end

    it 'should return all calendars if the user is a superadmin' do
      user.update_attributes(is_super_admin: true)
      expect(@query.readable_calendars.count).to eq(2)
      expect(@query.writeable_calendars.count).to eq(2)
    end

    context 'with permissions in place' do

      let(:disabled_permission) { create :granular_permission, invitation_id: invitation.id }
      let(:read_permission) { create :granular_permission, invitation_id: invitation.id, read: true, calendar_id: first_calendar.id }
      let(:write_permission) { create :granular_permission, invitation_id: invitation.id, write: true, calendar_id: second_calendar.id }
      let(:ignored_permission) { create :granular_permission, invitation_id: invitation }

      before(:each) do
        organization.update_attributes(uses_granular_permissions: true)
        invitation.granular_permissions << [disabled_permission, read_permission, write_permission]
      end

      it 'should return all calendars if the user is an admin' do
        invitation.update_attributes(is_admin: true)
        expect(@query.readable_calendars.count).to eq(2)
        expect(@query.writeable_calendars.count).to eq(2)
      end

      it 'should return all calendars if the user is a superadmin' do
        user.update_attributes(is_super_admin: true)
        expect(@query.readable_calendars.count).to eq(2)
        expect(@query.writeable_calendars.count).to eq(2)
      end

      it 'should only return calendars that are readable for a normal user' do
        expect(@query.readable_calendars.count).to eq(1)
        expect(@query.readable_calendars.all.include?(first_calendar)).to eq(true)
        expect(@query.readable_calendars.all.include?(second_calendar)).to eq(false)
      end

      it 'should only return calendars that are writeable for a normal user' do
        expect(@query.writeable_calendars.count).to eq(1)
        expect(@query.writeable_calendars.all.include?(first_calendar)).to eq(false)
        expect(@query.writeable_calendars.all.include?(second_calendar)).to eq(true)
      end

      it 'should not return any calendars if the invitation has not been accepted for a normal user' do
        invitation.update_attributes(accepted: false)
        expect(@query.readable_calendars.count).to eq(0)
        expect(@query.writeable_calendars.count).to eq(0)
      end

      it 'should not return any calendars if the user is an admin without an accepted invitation' do
        invitation.update_attributes(is_admin: true)
        invitation.update_attributes(accepted: false)
        expect(@query.readable_calendars.count).to eq(0)
        expect(@query.writeable_calendars.count).to eq(0)
      end

      it 'should return all calendars if the user is a super admin without an accepted invitation' do
        user.update_attributes(is_super_admin: true)
        invitation.update_attributes(accepted: false)
        expect(@query.readable_calendars.count).to eq(2)
        expect(@query.writeable_calendars.count).to eq(2)
      end

    end

  end

end
