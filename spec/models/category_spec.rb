require 'rails_helper'

RSpec.describe Category, type: :model do

  describe "styleable" do
    let (:category_style) { create :style }
    let (:first_calendar) { create :calendar }
    let (:first_category) { create :category, style_id: category_style.id, use_custom_styles: true }
    let (:second_category) { create :category, category_id: first_category }
    let (:first_item) { create :item }

    it "should inherit styles from the calendar" do
      first_category.update_attributes(use_custom_styles: false)
      first_calendar.categories << first_category
      expect(first_category.applicable_style_id).to eq(first_calendar.style.id)
    end

    it "should not inherit styles from the calendar if it uses a custom style" do
      first_calendar.categories << first_category
      expect(first_category.applicable_style_id).not_to eq(first_calendar.style.id)
      expect(first_category.applicable_style_id).to eq(category_style.id)
    end

    it "should inherit from a category in favor of a calendar" do
      first_category.categories << second_category
      first_calendar.categories << first_category
      expect(Category.find(second_category.id).applicable_style_id).to eq(category_style.id)
    end

    it "should pass its inherited style down to child items" do
      first_category.update_attributes(use_custom_styles: false)
      first_category.items << first_item
      first_category.categories << second_category
      first_calendar.categories << first_category
      expect(Item.find(first_item.id).applicable_style_id).to eq(first_calendar.style.id)
      expect(Category.find(second_category.id).applicable_style_id).to eq(first_calendar.style.id)
    end

    it "should pass its inherited style down to child items if assigned after the fact" do
      first_category.items << first_item
      first_calendar.categories << first_category
      expect(first_category.applicable_style_id).not_to eq(first_calendar.style.id)
      first_category.update_attributes(use_custom_styles: false)
      expect(Item.find(first_item.id).applicable_style_id).to eq(first_calendar.style.id)
    end

    it "should pass its inherited style down to child items if custom styles are disabled after the fact" do
      first_calendar.categories << first_category
      first_category.items << first_item
      expect(first_category.applicable_style_id).to eq(category_style.id)
      expect(Item.find(first_item.id).applicable_style_id).to eq(category_style.id)
      first_category.update_attributes(use_custom_styles: false)
      expect(first_category.applicable_style_id).to eq(first_calendar.style.id)
      expect(Item.find(first_item.id).applicable_style_id).to eq(first_calendar.style.id)
    end

    it "should pass its own style down to child items if it uses custom styles" do
      first_category.items << first_item
      expect(first_category.applicable_style_id).not_to eq(first_calendar.style.id)
      first_calendar.categories << first_category
      category_style = create(:style)
      first_category.update_attributes(use_custom_styles: true, style_id: category_style.id)
      expect(Item.find(first_item.id).applicable_style_id).to eq(category_style.id)
    end

    it "should not pass its inherited style down to child items that use custom styles" do
      first_item.use_custom_styles = true
      first_category.items << first_item
      expect(first_category.applicable_style_id).not_to eq(first_calendar.style.id)
      first_calendar.categories << first_category
      expect(Item.find(first_item.id).applicable_style_id).not_to eq(first_calendar.style.id)
    end

    it "should revert to its inherited style if custom styles have been disabled." do
      first_category.items << first_item
      expect(first_category.applicable_style_id).not_to eq(first_calendar.style.id)
      first_calendar.categories << first_category
      category_style = create(:style)
      first_category.update_attributes(use_custom_styles: true, style_id: category_style.id)
      expect(Item.find(first_item.id).applicable_style_id).to eq(category_style.id)
      first_category.update_attributes(use_custom_styles: false)
      expect(Item.find(first_item.id).applicable_style_id).to eq(first_calendar.style.id)
    end

  end
end
