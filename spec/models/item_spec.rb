require 'rails_helper'

RSpec.describe Item, type: :model do

  describe "styleable" do
    let (:category_style) { create :style }
    let (:first_calendar) { create :calendar }
    let (:first_category) { create :category, style_id: category_style.id, use_custom_styles: true }
    let (:first_item) { create :item }
    let (:first_event) { create :event }

    it "should inherit styles from the calendar" do
      first_calendar.items << first_item
      expect(first_item.applicable_style_id).to eq(first_calendar.style.id)
    end

    it "should not inherit styles from the calendar if it uses a custom style" do
      first_item.update_attributes(use_custom_styles: true)
      first_calendar.items << first_item
      expect(first_item.applicable_style_id).not_to eq(first_calendar.style.id)
    end

    it "should inherit from a category in favor of a calendar" do
      first_calendar.items << first_item
      first_category.items << first_item
      expect(Item.find(first_item.id).applicable_style_id).to eq(category_style.id)
    end

    it "should pass its inherited style down to child events" do
      first_calendar.items << first_item
      first_item.events << first_event
      expect(first_event.applicable_style_id).to eq(first_calendar.style.id)
    end

    it "should pass its inherited style down to child events if assigned after the fact" do
      first_item.events << first_event
      expect(first_item.applicable_style_id).not_to eq(first_calendar.style.id)
      first_calendar.items << first_item
      expect(Event.find(first_event.id).applicable_style_id).to eq(first_calendar.style.id)
    end

    it "should pass its own style down to child events if it uses custom styles" do
      first_item.events << first_event
      expect(first_item.applicable_style_id).not_to eq(first_calendar.style.id)
      first_calendar.items << first_item
      item_style = create(:style)
      first_item.update_attributes(use_custom_styles: true, style_id: item_style.id)
      expect(Event.find(first_event.id).applicable_style_id).to eq(item_style.id)
    end

    it "should not pass its inherited style down to child events that use custom styles" do
      first_event.use_custom_styles = true
      first_event.style = create(:style)
      first_item.events << first_event
      expect(first_item.applicable_style_id).not_to eq(first_calendar.style.id)
      first_calendar.items << first_item
      expect(Event.find(first_event.id).applicable_style_id).not_to eq(first_calendar.style.id)
    end

    it "should revert to its inherited style if custom styles have been disabled." do
      first_item.events << first_event
      expect(first_item.applicable_style_id).not_to eq(first_calendar.style.id)
      first_calendar.items << first_item
      item_style = create(:style)
      first_item.update_attributes(use_custom_styles: true, style_id: item_style.id)
      expect(Event.find(first_event.id).applicable_style_id).to eq(item_style.id)
      first_item.update_attributes(use_custom_styles: false)
      expect(Event.find(first_event.id).applicable_style_id).to eq(first_calendar.style.id)
    end

  end
end
