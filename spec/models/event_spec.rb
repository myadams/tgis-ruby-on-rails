require 'rails_helper'

RSpec.describe Event, type: :model do

  describe "styleable" do
    let (:first_calendar) { create :calendar }
    let (:first_category) { create :category }
    let (:first_item) { create :item }
    let (:first_event) { create :event }

    it "should inherit styles from the calendar if no intermediaries use custom styles" do
      first_item.events << first_event
      first_category.items << first_item
      first_calendar.categories << first_category
      first_calendar.items << first_item
      expect(Event.find(first_event.id).applicable_style_id).to eq(first_calendar.style.id)
    end

    it "should not inherit styles from the event if it uses a custom style" do
      first_item.update_attributes(use_custom_styles: true)
      first_item.events << first_event
      first_category.items << first_item
      first_calendar.items << first_item
      expect(first_item.applicable_style_id).not_to eq(first_calendar.style.id)
    end

    it "should inherit from a category in favor of a calendar" do
      category_style = create(:style)
      first_category.update_attributes(use_custom_styles: true, style_id: category_style.id)
      first_item.events << first_event
      first_category.items << first_item
      first_calendar.items << first_item
      expect(Item.find(first_item.id).applicable_style_id).to eq(category_style.id)
    end

    it "should inherit from an item in favor of a calendar or category" do
      item_style = create(:style)
      first_item.update_attributes(use_custom_styles: true, style_id: item_style.id)
      category_style = create(:style)
      first_category.update_attributes(use_custom_styles: true, style_id: category_style.id)
      first_item.events << first_event
      first_category.items << first_item
      first_calendar.items << first_item
      expect(Item.find(first_item.id).applicable_style_id).to eq(item_style.id)
    end

  end

  describe "during holiday" do
    let (:first_calendar) { create :calendar }
    let (:first_category) { create :category }
    let (:first_item) { create :item}
    let (:new_years_eve) { create :holiday, starts_at:  Time.zone.parse('2017-12-31'), ends_at: Time.zone.parse('2017-12-31'), length: 1}
    let (:christmas_week) { create :holiday, starts_at:  Time.zone.parse('2017-12-22'), ends_at: Time.zone.parse('2017-12-29'), length: 7}
    let (:first_event) { create :event, starts_at: Time.zone.parse('2017-12-26'), length: 2, ends_at: Time.zone.parse('2017-12-27'), interrupted_by_weekends: true, interrupted_by_holidays: true }
    let (:second_event) { create :event, starts_at: Time.zone.parse('2017-12-21'), length: 2, ends_at: Time.zone.parse('2017-12-22'), interrupted_by_weekends: true, interrupted_by_holidays: true }
    let (:third_event) { create :event, starts_at: Time.zone.parse('2017-12-29'), length: 7, ends_at: Time.zone.parse('2018-01-05'), interrupted_by_weekends: true, interrupted_by_holidays: true}

    before(:each) do
      first_calendar.holidays << [new_years_eve, christmas_week]
      first_calendar.categories << first_category
      first_calendar.events << [first_event, second_event, third_event]
      first_category.items << first_item
      first_item.events << [first_event, second_event, third_event]
    end

    it "should not be able find an event that is completely within the dates of the holiday" do
      events = Event.for_holiday(christmas_week)
      expect(events.map(&:id).include?(first_event.id)).to eq(false)
    end

    it "should not be able find an event that is completely within the dates of the holiday unless the event ignores holidays" do
      uninterrupted_event = create :event, starts_at: Time.zone.parse('2017-12-26'), length: 2, ends_at: Time.zone.parse('2017-12-27'), interrupted_by_holidays: true
      events = Event.for_holiday(christmas_week)
      expect(events.map(&:id).include?(uninterrupted_event.id)).to eq(false)
    end

    it "should find an event that overlaps the dates of the holiday" do
      events = Event.for_holiday(christmas_week)
      expect(events.map(&:id).include?(second_event.id)).to eq(true)
    end

    it "should find an event that begins before and ends after the holiday" do
      events = Event.for_holiday(new_years_eve)
      expect(events.map(&:id).include?(third_event.id)).to eq(true)
    end
  end

  describe "calculate_interruptions" do

    let(:calendar) { create :calendar }
    let(:valentines_day) { create :holiday, starts_at: Time.zone.parse('2017-02-14'), ends_at: Time.zone.parse('2017-02-15'), length: 1 }
    let(:valentines_duplicate) { create :holiday, starts_at: Time.zone.parse('2017-02-14'), ends_at: Time.zone.parse('2017-02-15'), length: 1 }
    let(:holiday_test_example) { create :holiday, starts_at: Time.zone.parse('2017-02-23'), ends_at: Time.zone.parse('2017-02-28'), length: 6 }

    before(:each) do
      calendar.holidays << [valentines_day, valentines_duplicate, holiday_test_example]
    end

    describe "overlapping_weekends" do

      it "should not find any interruptions on a monday through friday event" do
        event = Event.new(
          starts_at: Time.zone.parse('2017-02-06'),
          ends_at: Time.zone.parse('2017-02-10'),
          length: 5,
          interrupted_by_weekends: true,
          interrupted_by_holidays: true
        )
        expect(event.overlapping_weekends.length).to eq(0)
      end

      it "should find 1 interruption on a Sunday through Friday event" do
        event = Event.new(
          starts_at: Time.zone.parse('2017-02-05'),
          ends_at: Time.zone.parse('2017-02-10'),
          length: 6,
          interrupted_by_weekends: true,
          interrupted_by_holidays: true
        )
        weekends = event.overlapping_weekends.map{|d| d.strftime("%Y-%m-%d")}
        expect(weekends.length).to eq(1)
        expect(weekends.include?('2017-02-05')).to eq(true)
      end

      it "should find 2 interruptions on a Saturday through Friday event" do
        event = Event.new(
          starts_at: Time.zone.parse('2017-02-04'),
          ends_at: Time.zone.parse('2017-02-10'),
          length: 7,
          interrupted_by_weekends: true,
          interrupted_by_holidays: true
        )
        weekends = event.overlapping_weekends.map{|d| d.strftime("%Y-%m-%d")}
        expect(weekends.length).to eq(2)
        expect(weekends.include?('2017-02-04')).to eq(true)
        expect(weekends.include?('2017-02-05')).to eq(true)

      end

      it "should find 2 interruptions on a Monday through Saturday event" do
        event = Event.new(
          starts_at: Time.zone.parse('2017-02-06'),
          ends_at: Time.zone.parse('2017-02-11'),
          length: 6,
          interrupted_by_weekends: true,
          interrupted_by_holidays: true
        )
        weekends = event.overlapping_weekends.map{|d| d.strftime("%Y-%m-%d")}
        expect(weekends.length).to eq(2)
        expect(weekends.include?('2017-02-11')).to eq(true)
        expect(weekends.include?('2017-02-12')).to eq(true)
      end

      it "should find 2 interruptions on a Monday through Sunday event" do
        event = Event.new(
          starts_at: Time.zone.parse('2017-02-06'),
          ends_at: Time.zone.parse('2017-02-12'),
          length: 7,
          interrupted_by_weekends: true,
          interrupted_by_holidays: true
        )
        weekends = event.overlapping_weekends.map{|d| d.strftime("%Y-%m-%d")}
        expect(weekends.length).to eq(2)
        expect(weekends.include?('2017-02-11')).to eq(true)
        expect(weekends.include?('2017-02-12')).to eq(true)
      end

      it "should find 3 interruptions on a Sunday through Saturday event" do
        event = Event.new(
          starts_at: Time.zone.parse('2017-02-05'),
          ends_at: Time.zone.parse('2017-02-11'),
          length: 7,
          interrupted_by_weekends: true,
          interrupted_by_holidays: true
        )
        weekends = event.overlapping_weekends.map{|d| d.strftime("%Y-%m-%d")}
        expect(weekends.length).to eq(3)
        expect(weekends.include?('2017-02-05')).to eq(true)
        expect(weekends.include?('2017-02-11')).to eq(true)
        expect(weekends.include?('2017-02-12')).to eq(true)
      end

      it "should find 3 interruptions on a Sunday through Sunday event" do
        event = Event.new(
          starts_at: Time.zone.parse('2017-02-05'),
          ends_at: Time.zone.parse('2017-02-12'),
          length: 8,
          interrupted_by_weekends: true,
          interrupted_by_holidays: true
        )
        weekends = event.overlapping_weekends.map{|d| d.strftime("%Y-%m-%d")}
        expect(weekends.length).to eq(3)
        expect(weekends.include?('2017-02-05')).to eq(true)
        expect(weekends.include?('2017-02-11')).to eq(true)
        expect(weekends.include?('2017-02-12')).to eq(true)
      end

      it "should find 4 interruptions on a Saturday through Saturday event" do
        event = Event.new(
          starts_at: Time.zone.parse('2017-02-04'),
          ends_at: Time.zone.parse('2017-02-11'),
          length: 8,
          interrupted_by_weekends: true,
          interrupted_by_holidays: true
        )
        weekends = event.overlapping_weekends.map{|d| d.strftime("%Y-%m-%d")}
        expect(weekends.length).to eq(4)
        expect(weekends.include?('2017-02-04')).to eq(true)
        expect(weekends.include?('2017-02-05')).to eq(true)
        expect(weekends.include?('2017-02-11')).to eq(true)
        expect(weekends.include?('2017-02-12')).to eq(true)
      end

      it "should find 10 interruptions on this multiple week example" do
        event = Event.new(
          starts_at: Time.zone.parse('2017-02-01'),
          ends_at: Time.zone.parse('2017-03-09'),
          length: 36,
          interrupted_by_weekends: true,
          interrupted_by_holidays: true
        )
        weekends = event.overlapping_weekends.map{|d| d.strftime("%Y-%m-%d")}
        expect(weekends.length).to eq(10)
        expect(weekends.include?('2017-02-04')).to eq(true)
        expect(weekends.include?('2017-02-05')).to eq(true)
        expect(weekends.include?('2017-02-11')).to eq(true)
        expect(weekends.include?('2017-02-12')).to eq(true)
        expect(weekends.include?('2017-02-18')).to eq(true)
        expect(weekends.include?('2017-02-19')).to eq(true)
        expect(weekends.include?('2017-02-25')).to eq(true)
        expect(weekends.include?('2017-02-26')).to eq(true)
        expect(weekends.include?('2017-03-04')).to eq(true)
        expect(weekends.include?('2017-03-05')).to eq(true)
      end

    end

    describe "overlapping_holidays" do

      it "should find 1 interruption on an event that encompasses valentines day" do
        event = Event.new(
          calendar_id: calendar.id,
          starts_at: Time.zone.parse('2017-02-13'),
          ends_at: Time.zone.parse('2017-02-15'),
          length: 2,
          interrupted_by_weekends: true,
          interrupted_by_holidays: true
        )
        holidays = event.overlapping_holidays.map{|d| d.strftime("%Y-%m-%d")}
        expect(holidays.length).to eq(1)
        expect(holidays.include?('2017-02-14')).to eq(true)
      end

      it "should find 1 interruption on an event that is on valentines day" do
        event = Event.new(
          calendar_id: calendar.id,
          starts_at: Time.zone.parse('2017-02-14'),
          ends_at: Time.zone.parse('2017-02-14'),
          length: 1,
          interrupted_by_weekends: true,
          interrupted_by_holidays: true
        )
        holidays = event.overlapping_holidays.map{|d| d.strftime("%Y-%m-%d")}
        expect(holidays.include?('2017-02-14')).to eq(true)
        expect(holidays.length).to eq(1)
      end

      it "should find 1 interruption on an event that ends on valentines day" do
        event = Event.new(
          calendar_id: calendar.id,
          starts_at: Time.zone.parse('2017-02-13'),
          ends_at: Time.zone.parse('2017-02-14'),
          length: 1,
          interrupted_by_weekends: true,
          interrupted_by_holidays: true
        )
        holidays = event.overlapping_holidays.map{|d| d.strftime("%Y-%m-%d")}
        expect(holidays.include?('2017-02-14')).to eq(true)
        expect(holidays.length).to eq(1)
      end

      it "should find 6 interruptions on an event that ends on our test holiday" do
        event = Event.new(
          calendar_id: calendar.id,
          starts_at: Time.zone.parse('2017-02-21'),
          ends_at: Time.zone.parse('2017-02-23'),
          length: 3,
          interrupted_by_weekends: true,
          interrupted_by_holidays: true
        )
        holidays = event.overlapping_holidays.map{|d| d.strftime("%Y-%m-%d")}
        expect(holidays.length).to eq(6)
        expect(holidays.include?('2017-02-23')).to eq(true)
        expect(holidays.include?('2017-02-24')).to eq(true)
        expect(holidays.include?('2017-02-25')).to eq(true)
        expect(holidays.include?('2017-02-26')).to eq(true)
        expect(holidays.include?('2017-02-27')).to eq(true)
        expect(holidays.include?('2017-02-28')).to eq(true)
      end

      it "should find 6 interruptions on an event that ends on our test holiday" do
        event = Event.new(
          calendar_id: calendar.id,
          starts_at: Time.zone.parse('2017-02-13'),
          ends_at: Time.zone.parse('2017-02-23'),
          length: 11,
          interrupted_by_weekends: true,
          interrupted_by_holidays: true
        )
        holidays = event.overlapping_holidays.map{|d| d.strftime("%Y-%m-%d")}
        expect(holidays.length).to eq(7)
        expect(holidays.include?('2017-02-14')).to eq(true)
        expect(holidays.include?('2017-02-23')).to eq(true)
        expect(holidays.include?('2017-02-24')).to eq(true)
        expect(holidays.include?('2017-02-25')).to eq(true)
        expect(holidays.include?('2017-02-26')).to eq(true)
        expect(holidays.include?('2017-02-27')).to eq(true)
        expect(holidays.include?('2017-02-28')).to eq(true)
      end

    end

    describe "interruptions" do

      it "should find 9 interruptions on this multiple week example" do
        event = Event.new(
          calendar_id: calendar.id,
          starts_at: Time.zone.parse('2017-02-13'),
          ends_at: Time.zone.parse('2017-02-23'),
          length: 9,
          interrupted_by_weekends: true,
          interrupted_by_holidays: true
        )
        event.calculate_interruptions!
        interruptions = event.interruptions
        expect(interruptions.length).to eq(9)
        expect(interruptions.include?('2017-02-14T00:00:00Z')).to eq(true)
        expect(interruptions.include?('2017-02-18T00:00:00Z')).to eq(true)
        expect(interruptions.include?('2017-02-19T00:00:00Z')).to eq(true)
        expect(interruptions.include?('2017-02-23T00:00:00Z')).to eq(true)
        expect(interruptions.include?('2017-02-24T00:00:00Z')).to eq(true)
        expect(interruptions.include?('2017-02-25T00:00:00Z')).to eq(true)
        expect(interruptions.include?('2017-02-26T00:00:00Z')).to eq(true)
        expect(interruptions.include?('2017-02-27T00:00:00Z')).to eq(true)
        expect(interruptions.include?('2017-02-28T00:00:00Z')).to eq(true)
      end

      it "should find 7 interruptions on this multiple week example if the event is only interrupted by holidays." do
        event = Event.new(
          calendar_id: calendar.id,
          starts_at: Time.zone.parse('2017-02-13'),
          ends_at: Time.zone.parse('2017-02-23'),
          length: 9,
          interrupted_by_weekends: false,
          interrupted_by_holidays: true
        )
        event.calculate_interruptions!
        interruptions = event.interruptions
        expect(interruptions.length).to eq(7)
        expect(interruptions.include?('2017-02-14T00:00:00Z')).to eq(true)
        expect(interruptions.include?('2017-02-23T00:00:00Z')).to eq(true)
        expect(interruptions.include?('2017-02-24T00:00:00Z')).to eq(true)
        expect(interruptions.include?('2017-02-25T00:00:00Z')).to eq(true)
        expect(interruptions.include?('2017-02-26T00:00:00Z')).to eq(true)
        expect(interruptions.include?('2017-02-27T00:00:00Z')).to eq(true)
        expect(interruptions.include?('2017-02-28T00:00:00Z')).to eq(true)
      end

      it "should find 2 interruptions on this multiple week example if the event is only interrupted by weekends." do
        event = Event.new(
          calendar_id: calendar.id,
          starts_at: Time.zone.parse('2017-02-13'),
          ends_at: Time.zone.parse('2017-02-23'),
          length: 9,
          interrupted_by_weekends: true,
          interrupted_by_holidays: false
        )
        event.calculate_interruptions!
        interruptions = event.interruptions
        expect(interruptions.length).to eq(2)
        expect(interruptions.include?('2017-02-18T00:00:00Z')).to eq(true)
        expect(interruptions.include?('2017-02-19T00:00:00Z')).to eq(true)
      end

      it "should find 0 interruptions on this multiple week example if the event is not interrupted by holidays or weekends." do
        event = Event.new(
          calendar_id: calendar.id,
          starts_at: Time.zone.parse('2017-02-13'),
          ends_at: Time.zone.parse('2017-02-23'),
          length: 9,
          interrupted_by_weekends: false,
          interrupted_by_holidays: false
        )
        event.calculate_interruptions!
        interruptions = event.interruptions
        expect(interruptions.length).to eq(0)
      end

    end

    describe "calculate_ends_at" do

      it "should recalculate the end date beyond a holiday" do
        event = Event.new(
          calendar_id: calendar.id,
          starts_at: Time.zone.parse('2017-02-13'),
          length: 3,
          interrupted_by_weekends: true,
          interrupted_by_holidays: true
        )
        event.calculate_interruptions!
        interruptions = event.interruptions
        expect(interruptions.length).to eq(1)
        expect(interruptions.include?('2017-02-14T00:00:00Z')).to eq(true)
        expect(event.starts_at.strftime('%Y-%m-%d')).to eq('2017-02-13')
        expect(event.ends_at.strftime('%Y-%m-%d')).to eq('2017-02-16')
      end

      it "should not recalculate the end date beyond a holiday if interruptions are disabled" do
        event = Event.new(
          calendar_id: calendar.id,
          starts_at: Time.zone.parse('2017-02-13'),
          length: 3,
          interrupted_by_weekends: true,
          interrupted_by_holidays: false
        )
        event.calculate_interruptions!
        interruptions = event.interruptions
        expect(interruptions.length).to eq(0)
        expect(event.starts_at.strftime('%Y-%m-%d')).to eq('2017-02-13')
        expect(event.ends_at.strftime('%Y-%m-%d')).to eq('2017-02-15')
      end

      it "should recalculate the end date beyond a weekend" do
        event = Event.new(
          calendar_id: calendar.id,
          starts_at: Time.zone.parse('2017-02-17'),
          length: 3,
          interrupted_by_weekends: true,
          interrupted_by_holidays: true
        )
        event.calculate_interruptions!
        interruptions = event.interruptions
        expect(interruptions.length).to eq(2)
        expect(interruptions.include?('2017-02-18T00:00:00Z')).to eq(true)
        expect(interruptions.include?('2017-02-19T00:00:00Z')).to eq(true)
        expect(event.starts_at.strftime('%Y-%m-%d')).to eq('2017-02-17')
        expect(event.ends_at.strftime('%Y-%m-%d')).to eq('2017-02-21')
      end

      it "should not accidentally recalculate an event with outdated interruptions" do
        event = Event.new(
          calendar_id: calendar.id,
          starts_at: Time.zone.parse('2017-02-07T08:00:00'),
          length: 4,
          interrupted_by_weekends: true,
          interrupted_by_holidays: true,
          interruptions: ["2017-02-11", "2017-02-12"]
        )
        event.calculate_interruptions!
        interruptions = event.interruptions
        expect(interruptions.length).to eq(0)
        expect(event.starts_at.strftime('%Y-%m-%d')).to eq('2017-02-07')
        expect(event.ends_at.strftime('%Y-%m-%d')).to eq('2017-02-10')
      end

      it "should recalculate the end date into a weekend if interruptions are disabled" do
        event = Event.new(
          calendar_id: calendar.id,
          starts_at: Time.zone.parse('2017-02-17'),
          length: 3,
          interrupted_by_weekends: false,
          interrupted_by_holidays: true
        )
        event.calculate_interruptions!
        interruptions = event.interruptions
        expect(interruptions.length).to eq(0)
        expect(event.starts_at.strftime('%Y-%m-%d')).to eq('2017-02-17')
        expect(event.ends_at.strftime('%Y-%m-%d')).to eq('2017-02-19')
      end

      it "should recalculate the end date beyond a holiday and subsequently beyond a weekend" do
        event = Event.new(
          calendar_id: calendar.id,
          starts_at: Time.zone.parse('2017-02-13'),
          length: 5,
          interrupted_by_weekends: true,
          interrupted_by_holidays: true
        )
        event.calculate_interruptions!
        interruptions = event.interruptions
        expect(interruptions.length).to eq(3)
        expect(interruptions.include?('2017-02-14T00:00:00Z')).to eq(true)
        expect(interruptions.include?('2017-02-18T00:00:00Z')).to eq(true)
        expect(interruptions.include?('2017-02-19T00:00:00Z')).to eq(true)
        expect(event.starts_at.strftime('%Y-%m-%d')).to eq('2017-02-13')
        expect(event.ends_at.strftime('%Y-%m-%d')).to eq('2017-02-20')
      end

      it "should recalculate the start date beyond a holiday" do
        event = Event.new(
          calendar_id: calendar.id,
          starts_at: Time.zone.parse('2017-02-14'),
          length: 3,
          interrupted_by_weekends: true,
          interrupted_by_holidays: true
        )
        event.calculate_interruptions!
        interruptions = event.interruptions
        expect(interruptions.length).to eq(0)
        expect(interruptions.include?('2017-02-14T00:00:00Z')).to eq(false)
        expect(event.starts_at.strftime('%Y-%m-%d')).to eq('2017-02-15')
        expect(event.ends_at.strftime('%Y-%m-%d')).to eq('2017-02-17')
      end

      it "should recalculate the start date before a holiday if a decremental bias is used" do
        event = Event.new(
          calendar_id: calendar.id,
          starts_at: Time.zone.parse('2017-02-14'),
          length: 3,
          interrupted_by_weekends: true,
          interrupted_by_holidays: true
        )
        event.calculate_interruptions!(bias: :decrement)
        interruptions = event.interruptions
        expect(interruptions.length).to eq(1)
        expect(interruptions.include?('2017-02-14T00:00:00Z')).to eq(true)
        expect(event.starts_at.strftime('%Y-%m-%d')).to eq('2017-02-13')
        expect(event.ends_at.strftime('%Y-%m-%d')).to eq('2017-02-16')
      end

      it "should not recalculate the start date beyond a holiday if interruptions are disabled" do
        event = Event.new(
          calendar_id: calendar.id,
          starts_at: Time.zone.parse('2017-02-14'),
          length: 3,
          interrupted_by_weekends: true,
          interrupted_by_holidays: false
        )
        event.calculate_interruptions!
        interruptions = event.interruptions
        expect(interruptions.length).to eq(0)
        expect(event.starts_at.strftime('%Y-%m-%d')).to eq('2017-02-14')
        expect(event.ends_at.strftime('%Y-%m-%d')).to eq('2017-02-16')
      end

      it "should recalculate the start date beyond a weekend" do
        event = Event.new(
          calendar_id: calendar.id,
          starts_at: Time.zone.parse('2017-02-11'),
          length: 3,
          interrupted_by_weekends: true,
          interrupted_by_holidays: true
        )
        event.calculate_interruptions!
        interruptions = event.interruptions
        expect(interruptions.length).to eq(1)
        expect(interruptions.include?('2017-02-14T00:00:00Z')).to eq(true)
        expect(event.starts_at.strftime('%Y-%m-%d')).to eq('2017-02-13')
        expect(event.ends_at.strftime('%Y-%m-%d')).to eq('2017-02-16')
      end

      it "should recalculate the start date before a weekend if a decremental bias is used" do
        event = Event.new(
          calendar_id: calendar.id,
          starts_at: Time.zone.parse('2017-02-11'),
          length: 3,
          interrupted_by_weekends: true,
          interrupted_by_holidays: true
        )
        event.calculate_interruptions!(bias: :decrement)
        interruptions = event.interruptions
        expect(interruptions.length).to eq(3)
        expect(interruptions.include?('2017-02-11T00:00:00Z')).to eq(true)
        expect(interruptions.include?('2017-02-12T00:00:00Z')).to eq(true)
        expect(interruptions.include?('2017-02-14T00:00:00Z')).to eq(true)
        expect(event.starts_at.strftime('%Y-%m-%d')).to eq('2017-02-10')
        expect(event.ends_at.strftime('%Y-%m-%d')).to eq('2017-02-15')
      end

      it "should not recalculate the start date beyond a weekend if interruptions are disabled" do
        event = Event.new(
          calendar_id: calendar.id,
          starts_at: Time.zone.parse('2017-02-11'),
          length: 3,
          interrupted_by_weekends: false,
          interrupted_by_holidays: true
        )
        event.calculate_interruptions!
        interruptions = event.interruptions
        expect(interruptions.length).to eq(0)
        expect(event.starts_at.strftime('%Y-%m-%d')).to eq('2017-02-11')
        expect(event.ends_at.strftime('%Y-%m-%d')).to eq('2017-02-13')
      end

    end

  end

end
