require 'rails_helper'

RSpec.describe Holiday, type: :model do
  it "should update the end date after creating a new holiday" do
    holiday = Holiday.create(starts_at: 0.days.from_now, length: 2, name: 'test')
    expect(holiday.ends_at.day).to eq(2.days.from_now.day)
  end

  context "updating" do
    before(:each) do
      @holiday = Holiday.create(starts_at: 0.days.from_now, length: 2, name: 'test')
    end

    it "should update the end date after updating the length of an existing holiday" do
      @holiday.update_attributes(length: 12)
      expect(@holiday.ends_at.day).to eq(12.days.from_now.day)
    end

    it "should update the end date after updating the start date of an existing holiday" do
      @holiday.update_attributes(starts_at: 2.days.ago)
      expect(@holiday.ends_at.day).to eq(0.days.from_now.day)
    end
  end

  context "affected events" do

    let(:calendar) { create :calendar }
    let(:calendar_2) { create :calendar }
    let(:event_1) { create :event, starts_at: Time.zone.parse('2017-02-18'), length: 2, interrupted_by_holidays: true }
    let(:event_2) { create :event, starts_at: Time.zone.parse('2017-02-13'), length: 2, interrupted_by_holidays: true }
    let(:event_3) { create :event, starts_at: Time.zone.parse('2017-02-14'), length: 2, interrupted_by_holidays: true }
    let(:holiday) { create :holiday, starts_at: Time.zone.parse('2017-02-14'), length: 1}

    before(:each) do
      calendar.events << [event_1, event_2]
      calendar_2.events << event_3
      calendar.holidays << holiday
    end

    it "should not return events outside of the date range" do
      holiday.update_affected_events!
      expect(holiday.affected_events.include?(event_1)).to eq(false)
    end

    it "should only affect events which occur during the same date range as the holiday" do
      holiday.update_affected_events!
      expect(holiday.affected_events.include?(event_2)).to eq(true)
    end

    it "should update the start or end date of an affected event accordingly" do
      expect(event_2.ends_at.strftime("%Y-%m-%d")).to eq("2017-02-14")
      holiday.update_affected_events!
      event = Event.find(event_2.id)
      expect(event.starts_at.strftime("%Y-%m-%d")).to eq("2017-02-13")
      expect(event.ends_at.strftime("%Y-%m-%d")).to eq("2017-02-15")
    end

    it "should not return events that belong to a different calendar as the event" do
      holiday.update_affected_events!
      expect(holiday.affected_events.include?(event_3)).to eq(false)
    end

  end

end
