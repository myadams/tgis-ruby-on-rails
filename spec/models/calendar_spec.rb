require 'rails_helper'

RSpec.describe Calendar, type: :model do

  it "should automatically generate a default style when created" do
    calendar = create :calendar
    expect(calendar.style).not_to eq(nil)
  end
  
end
