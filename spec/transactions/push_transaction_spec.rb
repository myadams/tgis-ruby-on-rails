require 'rails_helper'

RSpec.describe PushTransaction, type: :model do

  describe "offset_event" do
    let(:event) { create :event, starts_at: 0.days.from_now, length: 2 }

    it "should delay the event if a positive offset is supplied" do
      push = PushTransaction.new(events: [event], offset: 10)
      push.run!
      updated_event = push.result.first
      expect(updated_event.starts_at.to_date).to eq(10.days.from_now.to_date)
      expect(updated_event.ends_at.to_date).to eq(11.days.from_now.to_date)
    end

    it "should advance the event if a negative offset is supplied" do
      push = PushTransaction.new(events: [event], offset: -10)
      push.run!
      updated_event = push.result.first
      expect(updated_event.starts_at.to_date).to eq(10.days.ago.to_date)
      expect(updated_event.ends_at.to_date).to eq(9.days.ago.to_date)
    end

    it "should push an event to the following week if ignoring weekends" do
      event = Event.create!(starts_at: Time.zone.parse('2017-01-11'), length: 2, interrupted_by_weekends: true)
      push = PushTransaction.new(events: [event], offset: 1)
      push.run!
      updated_event = push.result.first
      expect(updated_event.starts_at.to_date).to eq(Time.zone.parse('2017-01-12').to_date)
      expect(updated_event.ends_at.to_date).to eq(Time.zone.parse('2017-01-13').to_date)
    end

    it "should push an event to the following week if ignoring weekends" do
      event = Event.create!(starts_at: Time.zone.parse('2017-01-13'), length: 2, interrupted_by_weekends: true)
      push = PushTransaction.new(events: [event], offset: 1)
      push.run!
      updated_event = push.result.first
      expect(updated_event.starts_at.to_date).to eq(Time.zone.parse('2017-01-16').to_date)
      expect(updated_event.ends_at.to_date).to eq(Time.zone.parse('2017-01-17').to_date)
    end

    it "should push an event to the following week if ignoring weekends when pushed multiple days" do
      event = Event.create!(starts_at: Time.zone.parse('2017-01-13'), length: 4, interrupted_by_weekends: true)
      push = PushTransaction.new(events: [event], offset: 5)
      push.run!
      updated_event = push.result.first
      expect(updated_event.starts_at.to_date).to eq(Time.zone.parse('2017-01-18').to_date)
      expect(updated_event.ends_at.to_date).to eq(Time.zone.parse('2017-01-23').to_date)
    end

    it "should advance an event to the prior week if ignoring weekends when pulled" do
      event = Event.create!(starts_at: Time.zone.parse('2017-01-22'), length: 4, interrupted_by_weekends: true)
      push = PushTransaction.new(events: [event], offset: -1)
      push.run!
      updated_event = push.result.first
      expect(updated_event.starts_at.to_date).to eq(Time.zone.parse('2017-01-20').to_date)
      expect(updated_event.ends_at.to_date).to eq(Time.zone.parse('2017-01-25').to_date)
    end
  end

  describe "simple_offset" do

    let(:calendar) { create :calendar }
    let(:event_1) { create :event, calendar_id: calendar.id, starts_at: Time.zone.parse('2017-01-30'), length: 2}
    let(:event_2) { create :event, calendar_id: calendar.id, starts_at: Time.zone.parse('2017-02-02'), length: 1}
    let(:event_3) { create :event, calendar_id: calendar.id, starts_at: Time.zone.parse('2017-02-06'), length: 1}

    before(:each) do
      calendar.events << [event_1, event_2, event_3]
    end

    it "should apply the offset to all events equally" do
      push = PushTransaction.new(events: calendar.events, offset: 1)
      push.run!
      expect(push.result.map {|e| e.starts_at.to_date.strftime('%Y-%m-%d')}.sort).to eq(['2017-01-31','2017-02-03','2017-02-07'])
    end

    it "should extend a target event while pushing the rest" do
      push = PushTransaction.new(events: calendar.events, offset: 2, target_event: event_1)
      push.run!
      expect(push.result.map {|e| e.starts_at.to_date.strftime('%Y-%m-%d')}.sort).to eq(['2017-01-30','2017-02-04','2017-02-08'])
      expect(push.result.map {|e| e.ends_at.to_date.strftime('%Y-%m-%d')}.sort).to eq(['2017-02-02','2017-02-04','2017-02-08'])
    end

    it "should prepend a target event while pulling the rest" do
      push = PushTransaction.new(events: calendar.events, offset: -1, target_event: event_1)
      push.run!
      expect(push.result.map {|e| e.starts_at.to_date.strftime('%Y-%m-%d')}.sort).to eq(['2017-01-29','2017-02-01','2017-02-05'])
      expect(push.result.map {|e| e.ends_at.to_date.strftime('%Y-%m-%d')}.sort).to eq(['2017-01-31','2017-02-01','2017-02-05'])
    end

    it "should extend a target event while pushing the rest and ignoring weekends" do
      event_1.interrupted_by_weekends = true
      event_2.interrupted_by_weekends = true
      event_3.interrupted_by_weekends = true
      push = PushTransaction.new(events: calendar.events, offset: 2, target_event: event_1)
      push.run!
      expect(push.result.map {|e| e.starts_at.to_date.strftime('%Y-%m-%d')}.sort).to eq(['2017-01-30','2017-02-06','2017-02-08'])
      expect(push.result.map {|e| e.ends_at.to_date.strftime('%Y-%m-%d')}.sort).to eq(['2017-02-02','2017-02-06','2017-02-08'])
    end
  end

  # Complex Offset is no longer supported or utilized so these tests have been disabled for speed.
  
  # describe "complex_offset" do
  #
  #   let(:calendar) { create :calendar }
  #   let(:event_1) { create :event, calendar_id: calendar.id, starts_at: Time.zone.parse('2017-01-30'), length: 2}
  #   let(:event_2) { create :event, calendar_id: calendar.id, starts_at: Time.zone.parse('2017-02-02'), length: 1}
  #   let(:event_3) { create :event, calendar_id: calendar.id, starts_at: Time.zone.parse('2017-02-06'), length: 1}
  #
  #   before(:each) do
  #     calendar.events << [event_1, event_2, event_3]
  #   end
  #
  #   it "should apply an offset to all of the events and return all events" do
  #     push = PushTransaction.new(events: calendar.events, offset: 5, fill_unused_days: true)
  #     push.run!
  #     expect(push.result.length).to eq(3)
  #   end
  #
  #   it "should apply an offset to all of the events if the offset is larger than the amount of empty space between the events" do
  #     push = PushTransaction.new(events: calendar.events, offset: 5, fill_unused_days: true)
  #     push.run!
  #     expect(push.result.map {|e| e.starts_at.to_date.strftime('%Y-%m-%d')}.sort).to eq(['2017-02-04','2017-02-06','2017-02-09'])
  #   end
  #
  #   it "should only apply an offset to the first event if there is enough space between the second and third events" do
  #     push = PushTransaction.new(events: calendar.events, offset: 1, fill_unused_days: true)
  #     push.run!
  #     expect(push.result.map {|e| e.starts_at.to_date.strftime('%Y-%m-%d')}.sort).to eq(['2017-01-31'])
  #   end
  #
  #   it "should prepend a target event while pulling the rest" do
  #     push = PushTransaction.new(events: calendar.events, offset: -1, target_event: event_1, fill_unused_days: true)
  #     push.run!
  #     expect(push.result.map {|e| e.starts_at.to_date.strftime('%Y-%m-%d')}.sort).to eq(['2017-01-29','2017-02-01','2017-02-05'])
  #     expect(push.result.map {|e| e.ends_at.to_date.strftime('%Y-%m-%d')}.sort).to eq(['2017-01-31','2017-02-01','2017-02-05'])
  #   end
  #
  #   it "should extend a target event while pushing the rest and ignoring weekend and filling unused days" do
  #     push = PushTransaction.new(events: calendar.events, offset: 2, target_event: event_1, fill_unused_days: true)
  #     push.run!
  #     expect(push.result.map {|e| e.starts_at.to_date.strftime('%Y-%m-%d')}.sort).to eq(['2017-01-30','2017-02-03'])
  #     expect(push.result.map {|e| e.ends_at.to_date.strftime('%Y-%m-%d')}.sort).to eq(['2017-02-02','2017-02-03'])
  #   end
  # end

end
