require 'rails_helper'

RSpec.describe SnapshotTransaction, type: :model do

  let(:user) { create :user }
  let(:calendar) { create :calendar }
  let(:christmas) { create :holiday, name: "Christmas" }
  let(:new_years) { create :holiday, name: "New Years" }
  let(:category_style) { create :style, calendar_id: calendar.id }
  let(:parent_category) { create :category, name: "Parent", calendar_id: calendar.id, style_id: category_style.id, use_custom_styles: true }
  let(:child_category) { create :category, name: "Child", calendar_id: calendar.id, category_id: parent_category.id }
  let(:item_style) { create :style, calendar_id: calendar.id }
  let(:item) { create :item, calendar_id: calendar.id, category_id: child_category.id, use_custom_styles: true, style_id: item_style.id }
  let(:event) { create :event, calendar_id: calendar.id, item_id: item.id }
  let(:top_level_item) { create :item, calendar_id: calendar.id }
  let(:top_level_event) { create :event, calendar_id: calendar.id, item_id: top_level_item.id }


  before(:each) do
    user.save
    parent_category.categories << child_category
    calendar.categories << [parent_category, child_category]
    calendar.items << [item, top_level_item]
    calendar.events << [event, top_level_event]
    calendar.holidays << [christmas, new_years]
    calendar.save
  end

  describe "test environment" do
    it "should have three styles" do
      expect(calendar.styles.count).to eq(3)
    end

    it "should have two categories" do
      expect(calendar.categories.count).to eq(2)
    end

    it "should have two holidays" do
      expect(calendar.holidays.count).to eq(2)
    end

    it "should have two items" do
      expect(calendar.items.count).to eq(2)
    end

    it "should have two events" do
      expect(calendar.events.count).to eq(2)
    end
  end

  describe "snapshot" do
    before(:each) do
      snapshot = SnapshotTransaction.new(calendar: calendar, name: "Test", notes: "This is a test.", user_id: user.id)
      snapshot.run!
      @result = snapshot.result
    end

    it "should have belong to the calendar it's based from" do
      expect(@result.calendar_id).to eq(calendar.id)
    end

    it "should have three styles" do
      expect(@result.styles.count).to eq(3)
      expect(@result.styles.where(calendar_id: nil).count).to eq(3)
      expect(@result.styles.where(calendar_id: calendar.id).count).to eq(0)
    end

    it "should have two holidays" do
      expect(@result.holidays.count).to eq(2)
      expect(@result.holidays.where(calendar_id: nil).count).to eq(2)
      expect(@result.holidays.where(calendar_id: calendar.id).count).to eq(0)
    end

    it "should have two categories" do
      expect(@result.categories.count).to eq(2)
      expect(@result.categories.where(calendar_id: nil).count).to eq(2)
      expect(@result.categories.where(calendar_id: calendar.id).count).to eq(0)
    end

    it "should have two items" do
      expect(@result.items.count).to eq(2)
      expect(@result.items.where(calendar_id: nil).count).to eq(2)
      expect(@result.items.where(calendar_id: calendar.id).count).to eq(0)
    end

    it "should have two events" do
      expect(@result.events.count).to eq(2)
      expect(@result.events.where(calendar_id: nil).count).to eq(2)
      expect(@result.events.where(calendar_id: calendar.id).count).to eq(0)
    end
  end

end
