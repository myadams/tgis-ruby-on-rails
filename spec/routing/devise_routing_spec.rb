require 'rails_helper'

RSpec.describe 'DeviseControllers' do
  describe "routing" do
    it "routes to #sign_in" do
      expect(post: "/users/sign_in").not_to be_routable
    end

    it "routes to #sign_out" do
      expect(delete: "/users/sign_out").not_to be_routable
    end
  end
end
