require 'rails_helper'

describe "API V1 Organizations", type: :request, reqres_section: 'Organizations' do

  let(:accept_header) { 'application/vnd.film_cal-v1+json' }
  let(:user) { create :user,
    email: 'user@example.com',
    password: '12345678',
    is_super_admin: true }

  context 'GET /api/organizations/' do
    it 'returns an empty array of organizations' do
      get_as_user '/api/organizations/'
      expect(response.status).to eq(200)
      expect(JSON.parse(response.body)).to eq []
      expect(response.headers['Content-Type']).to match('json')
      expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
      expect(response.headers['Access-Control-Request-Method']).to eq('*')
    end

    context 'when results are present', :skip_reqres do
      let(:first_organization) { create :organization }
      let(:second_organization) { create :organization }
      let(:invitation) { create :invitation,
        user_id: user.id,
        organization_id: second_organization.id,
        accepted: true }

      before(:example) do
        [first_organization, second_organization, invitation].each { |x| x.touch }
      end

      it 'returns an array of all organizations when user is a super admin' do
        get_as_user '/api/organizations/'
        expect(response.status).to eq(200)
        expect(response.headers['Content-Type']).to match('json')
        expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
        expect(response.headers['Access-Control-Request-Method']).to eq('*')
        expect(JSON.parse(response.body).count).to eq 2
      end

      it 'only returns organizations a normal user has been invited to' do
        user.update_attribute(:is_super_admin, false)
        get_as_user '/api/organizations/'
        expect(response.status).to eq(200)
        expect(response.headers['Content-Type']).to match('json')
        expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
        expect(response.headers['Access-Control-Request-Method']).to eq('*')
        expect(JSON.parse(response.body).count).to eq 1
        expect(JSON.parse(response.body).first["id"]).to eq second_organization.id
      end

      it 'does not return organizations a normal user has been invited to but has not accepted' do
        user.update_attribute(:is_super_admin, false)
        invitation.update_attributes(accepted: false)
        get_as_user '/api/organizations/'
        expect(response.status).to eq(200)
        expect(response.headers['Content-Type']).to match('json')
        expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
        expect(response.headers['Access-Control-Request-Method']).to eq('*')
        expect(JSON.parse(response.body).count).to eq 0
      end
    end

    it 'returns a 401 not authorized error to an anonymous user', :skip_reqres do
      get '/api/organizations/'
      expect(response.response_code).to eq(401)
      expect(response.body).to match('OAuth')
      expect(response.headers['Content-Type']).to match('json')
      expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
      expect(response.headers['Access-Control-Request-Method']).to eq('*')
    end

    it 'returns a 401 if the current token has expired', :skip_reqres do
      get_as_user '/api/organizations/', expired: true
      expect(response.status).to eq(401)
      expect(response.headers['Content-Type']).to match('json')
      expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
      expect(response.headers['Access-Control-Request-Method']).to eq('*')
    end
  end

  context 'GET /api/organizations/:subdomain' do
    it 'returns an empty array of organizations' do
      get_as_user '/api/organizations/some-domain'
      expect(response.status).to eq(404)
      expect(response.headers['Content-Type']).to match('json')
      expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
      expect(response.headers['Access-Control-Request-Method']).to eq('*')
    end

    context 'when results are present', :skip_reqres do
      let(:first_organization) { create :organization }
      let(:second_organization) { create :organization }
      let(:invitation) { create :invitation,
        user_id: user.id,
        organization_id: second_organization.id,
        accepted: true }

      before(:example) do
        [first_organization, second_organization, invitation].each { |x| x.touch }
      end

      it 'returns the organization regardless of invite status if super user.' do
        get_as_user "/api/organizations/#{first_organization.subdomain}"
        expect(response.status).to eq(200)
        expect(response.headers['Content-Type']).to match('json')
        expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
        expect(response.headers['Access-Control-Request-Method']).to eq('*')
        expect(JSON.parse(response.body).first["id"]).to eq first_organization.id
      end

      it 'only returns an organization a normal user has been invited' do
        user.update_attribute(:is_super_admin, false)
        get_as_user "/api/organizations/#{second_organization.subdomain}"
        expect(response.status).to eq(200)
        expect(response.headers['Content-Type']).to match('json')
        expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
        expect(response.headers['Access-Control-Request-Method']).to eq('*')
        expect(JSON.parse(response.body).first["id"]).to eq second_organization.id
      end

      it 'does not return an organization a normal user has been invited to but has not accepted' do
        user.update_attribute(:is_super_admin, false)
        invitation.update_attributes(accepted: false)
        get_as_user "/api/organizations/#{first_organization.subdomain}"
        expect(response.status).to eq(404)
        expect(response.headers['Content-Type']).to match('json')
        expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
        expect(response.headers['Access-Control-Request-Method']).to eq('*')
      end
    end

    it 'returns a 401 not authorized error to an anonymous user', :skip_reqres do
      get '/api/organizations/some-sub-domain'
      expect(response.response_code).to eq(401)
      expect(response.body).to match('OAuth')
      expect(response.headers['Content-Type']).to match('json')
      expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
      expect(response.headers['Access-Control-Request-Method']).to eq('*')
    end

    it 'returns a 401 if the current token has expired', :skip_reqres do
      get_as_user '/api/organizations/some-sub-domain', expired: true
      expect(response.status).to eq(401)
      expect(response.headers['Content-Type']).to match('json')
      expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
      expect(response.headers['Access-Control-Request-Method']).to eq('*')
    end
  end

  context 'GET /api/organizations/validations?name=%s' do

    context 'when results are present', :skip_reqres do
      let(:first_organization) { create :organization }

      before(:example) do
        [first_organization].each { |x| x.touch }
      end

      it 'returns a 200 with a valid response if the name is valid amongst existing organizations' do
        get_as_user '/api/organizations/validations?name=Sumo%20Creations&color=ff9900&subdomain=somedomain'
        expect(response.status).to eq(200)
        expect(response.headers['Content-Type']).to match('json')
        expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
        expect(response.headers['Access-Control-Request-Method']).to eq('*')
        expect(JSON.parse(response.body)["valid"]).to eq true
      end

      it 'returns a 200 with an invalid response if the invalid credentials are passed but always_ok is set to true' do
        user.update_attribute(:is_super_admin, false)
        get_as_user '/api/organizations/validations?name=SomeOtherUnusedName&always_ok=true'
        expect(response.status).to eq(200)
        expect(response.headers['Content-Type']).to match('json')
        expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
        expect(response.headers['Access-Control-Request-Method']).to eq('*')
        expect(JSON.parse(response.body)["valid"]).to eq false
        expect(JSON.parse(response.body)["errors"]["name"]).to eq nil
      end

      it 'returns a 200 with an valid response if the name is matches its existing value' do
        user.update_attribute(:is_super_admin, false)
        get_as_user '/api/organizations/validations?name=SomeOtherUnusedName'
        expect(response.status).to eq(422)
        expect(response.headers['Content-Type']).to match('json')
        expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
        expect(response.headers['Access-Control-Request-Method']).to eq('*')
        expect(JSON.parse(response.body)["valid"]).to eq nil
        expect(JSON.parse(response.body)["errors"]["name"]).to eq nil
      end

      it 'returns a 422 with an invalid response if the name is invalid amongst existing organizations' do
        user.update_attribute(:is_super_admin, false)
        get_as_user '/api/organizations/validations?name='+first_organization.name
        expect(response.status).to eq(422)
        expect(response.headers['Content-Type']).to match('json')
        expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
        expect(response.headers['Access-Control-Request-Method']).to eq('*')
        expect(JSON.parse(response.body)["valid"]).to eq nil
        expect(JSON.parse(response.body)["errors"]["name"]).to eq "has already been taken"
      end

      it 'returns a 200 with an valid response if the name is being tested against itself' do
        user.update_attribute(:is_super_admin, false)
        get_as_user "/api/organizations/validations?name=#{first_organization.name}&color=ff9900&subdomain=somedomain&id=#{first_organization.id}"
        expect(response.status).to eq(200)
        expect(response.headers['Content-Type']).to match('json')
        expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
        expect(response.headers['Access-Control-Request-Method']).to eq('*')
        expect(JSON.parse(response.body)["valid"]).to eq true
        expect(JSON.parse(response.body)["errors"]["name"]).to eq nil
      end
    end

    it 'returns a 401 not authorized error to an anonymous user', :skip_reqres do
      get '/api/organizations/name?name=Sumo%20Creations'
      expect(response.response_code).to eq(401)
      expect(response.body).to match('OAuth')
      expect(response.headers['Content-Type']).to match('json')
      expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
      expect(response.headers['Access-Control-Request-Method']).to eq('*')
    end

    it 'returns a 401 if the current token has expired', :skip_reqres do
      get_as_user '/api/organizations/name?name=Sumo%20Creations', expired: true
      expect(response.status).to eq(401)
      expect(response.headers['Content-Type']).to match('json')
      expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
      expect(response.headers['Access-Control-Request-Method']).to eq('*')
    end
  end

  context 'POST /api/organizations/' do

    it 'returns a 201 if the organization is valid', reqres_title: 'Create a new organization' do
      post_as_user '/api/organizations/', params: { name: "Test", uses_granular_permissions: false, subdomain: 'test', color: '#ff3322' }
      expect(response.status).to eq(201)
      expect(response.headers['Content-Type']).to match('json')
      expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
      expect(response.headers['Access-Control-Request-Method']).to eq('*')
      expect(Organization.count).to eq(1)
    end

    it 'returns a 422 if the organization is invalid', :skip_reqres do
      existing_organization = create :organization
      existing_organization.touch
      post_as_user '/api/organizations/', params: { name: existing_organization.name, uses_granular_permissions: false, subdomain: 'test', color: '#ff3322' }
      expect(response.status).to eq(422)
      expect(response.headers['Content-Type']).to match('json')
      expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
      expect(response.headers['Access-Control-Request-Method']).to eq('*')
      expect(Organization.count).to eq(1) # Should not be 2
    end

    it 'returns a 403 if the user is not a super admin', :skip_reqres do
      user.update_attribute(:is_super_admin, false)
      post_as_user '/api/organizations/', params: { name: "Test", uses_granular_permissions: false, subdomain: 'test', color: '#ff3322' }
      expect(response.status).to eq(403)
      expect(response.headers['Content-Type']).to match('json')
      expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
      expect(response.headers['Access-Control-Request-Method']).to eq('*')
      expect(Organization.count).to eq(0)
    end

    it 'returns a 401 if the token is expired', :skip_reqres do
      post_as_user '/api/organizations/', params: { name: 'Test', uses_granular_permissions: false, subdomain: 'test', color: '#ff3322' }, expired: true
      expect(response.status).to eq(401)
      expect(response.headers['Content-Type']).to match('json')
      expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
      expect(response.headers['Access-Control-Request-Method']).to eq('*')
      expect(Organization.count).to eq(0)
    end

  end

  context 'PUT /api/organizations/' do

    let(:existing_organization) { create :organization }
    let(:path) { "/api/organizations/#{existing_organization.id}" }

    before(:each) do
      existing_organization.touch
    end

    it 'returns a 200 if the organization is valid', reqres_title: 'Update an existing organization' do
      put_as_user path, params: { name: 'Test', uses_granular_permissions: true, color: '#ff3322' }
      expect(response.status).to eq(200)
      expect(response.headers['Content-Type']).to match('json')
      expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
      expect(response.headers['Access-Control-Request-Method']).to eq('*')
      expect(Organization.find(existing_organization.id).name).to eq('Test')
      expect(Organization.find(existing_organization.id).uses_granular_permissions).to eq(true)
    end

    it 'returns a 422 if the organization is invalid', :skip_reqres do
      conflicting_organization = create :organization
      conflicting_organization.touch
      put_as_user path, params: { name: conflicting_organization.name, uses_granular_permissions: true, color: '#ff3322' }
      expect(response.status).to eq(422)
      expect(response.headers['Content-Type']).to match('json')
      expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
      expect(response.headers['Access-Control-Request-Method']).to eq('*')
      expect(Organization.find(existing_organization.id).name).not_to eq('Test')
    end

    it 'returns a 404 if the organization does not exist', :skip_reqres do
      put_as_user "/api/organizations/#{existing_organization.id+999}" , params: { name: 'Test', uses_granular_permissions: true, color: '#ff3322' }
      expect(response.status).to eq(404)
      expect(response.headers['Content-Type']).to match('json')
      expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
      expect(response.headers['Access-Control-Request-Method']).to eq('*')
    end

    it 'returns a 403 if the user is not a super admin', :skip_reqres do
      user.update_attribute(:is_super_admin, false)
      put_as_user path, params: { name: "Test", uses_granular_permissions: true, color: '#ff3322'}
      expect(response.status).to eq(403)
      expect(response.headers['Content-Type']).to match('json')
      expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
      expect(response.headers['Access-Control-Request-Method']).to eq('*')
      expect(Organization.find(existing_organization.id).name).not_to eq('Test')
    end

    it 'returns a 401 if the token is expired', :skip_reqres do
      user.update_attribute(:is_super_admin, false)
      put_as_user path, params: { name: 'Test', color: '#ff3322', uses_granular_permissions: true }, expired: true
      expect(response.status).to eq(401)
      expect(response.headers['Content-Type']).to match('json')
      expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
      expect(response.headers['Access-Control-Request-Method']).to eq('*')
      expect(Organization.find(existing_organization.id).name).not_to eq('Test')
    end

  end

  context 'DELETE /api/organizations/' do

    let(:existing_organization) { create :organization }
    let(:path) { "/api/organizations/#{existing_organization.id}" }

    before(:each) do
      existing_organization.touch
    end

    it 'returns a 200 if the organization is valid', reqres_title: 'Update an existing organization' do
      expect(Organization.where(id: existing_organization.id).count).to eq(1)
      delete_as_user path
      expect(response.status).to eq(200)
      expect(response.headers['Content-Type']).to match('json')
      expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
      expect(response.headers['Access-Control-Request-Method']).to eq('*')
      expect(Organization.where(id: existing_organization.id).count).to eq(0)
      expect(JSON.parse(response.body)["is_deleted"]).to eq(true)
    end

    it 'returns a 401 if the token is expired', :skip_reqres do
      user.update_attribute(:is_super_admin, false)
      delete_as_user path, params: { name: 'Test' }, expired: true
      expect(response.status).to eq(401)
      expect(response.headers['Content-Type']).to match('json')
      expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
      expect(response.headers['Access-Control-Request-Method']).to eq('*')
      expect(Organization.where(id: existing_organization.id).count).to eq(1)
    end

    it 'returns a 403 if the user is not a super admin', :skip_reqres do
      user.update_attribute(:is_super_admin, false)
      delete_as_user path, params: { name: "Test", uses_granular_permissions: true}
      expect(response.status).to eq(403)
      expect(response.headers['Content-Type']).to match('json')
      expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
      expect(response.headers['Access-Control-Request-Method']).to eq('*')
      expect(Organization.where(id: existing_organization.id).count).to eq(1)
    end

    it 'returns a 404 if the organization does not exist', :skip_reqres do
      delete_as_user "/api/organizations/#{existing_organization.id+999}" , params: { name: 'Test', uses_granular_permissions: true }
      expect(response.status).to eq(404)
      expect(response.headers['Content-Type']).to match('json')
      expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
      expect(response.headers['Access-Control-Request-Method']).to eq('*')
      expect(Organization.where(id: existing_organization.id).count).to eq(1)
    end

  end

end
