require 'rails_helper'

describe "API V1 Granular Permissions", type: :request, reqres_section: 'Granular Permissions' do

  let(:accept_header) { 'application/vnd.film_cal-v1+json' }
  let(:email) { 'user@example.com' }
  let(:user) { create :user, email: email, password: '12345678' }

  context 'POST /api/organizations/:id/invitations/:invitation_id/granular_permissions' do

    let(:organization) { create :organization }
    let(:calendar) { create :calendar, organization_id: organization.id }
    let(:invitation) { create :invitation, email: user.email, organization_id: organization.id, user_id: user.id, accepted: true, is_admin: true }
    let(:path) { "/api/organizations/#{organization.subdomain}/invitations/#{invitation.id}/granular_permissions" }

    before(:each) do
      organization.calendars << calendar
    end

    it 'returns a 201 if the permission is valid', reqres_title: 'Create a new granular permission' do
      post_as_user path, params: { read: true, write: true, calendar_id: calendar_id }
      expect(response.status).to eq(201)
      expect(response.headers['Content-Type']).to match('json')
      expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
      expect(response.headers['Access-Control-Request-Method']).to eq('*')
      expect(GranularPermission.count).to eq(1)
    end

    it 'returns a 401 if the user is not authorized', :skip_reqres do
      post path, params: { read: true, write: true, calendar_id: calendar.id }
      expect(response.status).to eq(401)
      expect(response.headers['Content-Type']).to match('json')
      expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
      expect(response.headers['Access-Control-Request-Method']).to eq('*')
    end

    it 'returns a 403 if the user is not an admin', :skip_reqres do
      invitation.update_attributes(is_admin: false)
      post_as_user path, params: { read: true, write: true, calendar_id: calendar.id }
      expect(response.status).to eq(403)
      expect(response.headers['Content-Type']).to match('json')
      expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
      expect(response.headers['Access-Control-Request-Method']).to eq('*')
    end

    it 'returns a 422 if an invitation is invalid', :skip_reqres do
      invitation.granular_permissions << create(:granular_permission, invitation_id: invitation.id, calendar_id: calendar.id)
      post_as_user path, params: { read: true, write: true, calendar_id: calendar.id }
      expect(response.status).to eq(422)
      expect(response.headers['Content-Type']).to match('json')
      expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
      expect(response.headers['Access-Control-Request-Method']).to eq('*')
      expect(GranularPermission.count).to eq(1)
    end

  end

  context 'PUT /api/organizations/:id/invitations/:invitation_id/granular_permissions/:permission_id' do

    let(:organization) { create :organization }
    let(:calendar) { create :calendar, organization_id: organization.id }
    let(:invitation) { create :invitation, email: user.email, organization_id: organization.id, user_id: user.id, accepted: true, is_admin: true }
    let(:granular_permission) { create :granular_permission, invitation_id: invitation.id, calendar_id: calendar.id }
    let(:path) { "/api/organizations/#{organization.subdomain}/invitations/#{invitation.id}/granular_permissions/#{granular_permission.id}" }

    before(:each) do
      organization.calendars << calendar
    end

    it 'returns a 200 if the permission is valid', reqres_title: 'Create a new granular permission' do
      put_as_user path, params: { read: true, write: true }
      expect(response.status).to eq(200)
      expect(response.headers['Content-Type']).to match('json')
      expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
      expect(response.headers['Access-Control-Request-Method']).to eq('*')
      expect(GranularPermission.find(granular_permission.id).write).to eq(true)
    end

    it 'returns a 200 if the user is a super admin regardless of whether or not they are invited.' do
      invitation.update_attributes(is_admin: false, accepted: false)
      user.update_attributes(is_super_admin: true)
      put_as_user path, params: { read: true, write: true }
      expect(response.status).to eq(200)
      expect(response.headers['Content-Type']).to match('json')
      expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
      expect(response.headers['Access-Control-Request-Method']).to eq('*')
      expect(GranularPermission.find(granular_permission.id).write).to eq(true)
    end

    it 'returns a 400 if an invitation is invalid', :skip_reqres do
      put_as_user path, params: { read: true }
      expect(response.status).to eq(400)
      expect(response.headers['Content-Type']).to match('json')
      expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
      expect(response.headers['Access-Control-Request-Method']).to eq('*')
      expect(GranularPermission.find(granular_permission.id).write).to eq(false)
    end

    it 'returns a 401 if the user is not authorized', :skip_reqres do
      put path, params: { read: true, write: true }
      expect(response.status).to eq(401)
      expect(response.headers['Content-Type']).to match('json')
      expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
      expect(response.headers['Access-Control-Request-Method']).to eq('*')
    end

    it 'returns a 403 if the user is not an admin', :skip_reqres do
      invitation.update_attributes(is_admin: false)
      put_as_user path, params: { read: true, write: true }
      expect(response.status).to eq(403)
      expect(response.headers['Content-Type']).to match('json')
      expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
      expect(response.headers['Access-Control-Request-Method']).to eq('*')
    end

    it 'returns a 403 if the user is not invited to the parent organization', :skip_reqres do
      invitation.update_attributes(accepted: false)
      put_as_user path, params: { read: true, write: true }
      expect(response.status).to eq(403)
      expect(response.headers['Content-Type']).to match('json')
      expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
      expect(response.headers['Access-Control-Request-Method']).to eq('*')
    end

  end

  context 'GET /api/organizations/:id/invitations/:invitation_id/granular_permissions' do
    let(:organization) { create :organization }
    let(:calendar) { create :calendar, organization_id: organization.id }
    let(:alternate_calendar) { create :calendar, organization_id: organization.id }
    let(:invitation) { create :invitation, email: user.email, organization_id: organization.id, user_id: user.id, accepted: true, is_admin: true }
    let(:first_permission) { create :granular_permission, invitation_id: invitation.id, calendar_id: calendar.id }
    let(:second_permission) { create :granular_permission, invitation_id: invitation.id, calendar_id: alternate_calendar.id }
    let(:path) { "/api/organizations/#{organization.subdomain}/invitations/#{invitation.id}/granular_permissions" }
    let(:missing_path) { "/api/organizations/#{organization.subdomain}/invitations/#{invitation.id+999}/granular_permissions" }

    before(:each) do
      invitation.granular_permissions << [first_permission, second_permission]
    end

    it 'returns a 200 if the request is valid', reqres_title: 'Request a list of permissions for an invitation' do
      get_as_user path, params: { read: true, write: true, calendar_id: calendar_id }
      expect(response.status).to eq(200)
      expect(response.headers['Content-Type']).to match('json')
      expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
      expect(response.headers['Access-Control-Request-Method']).to eq('*')
      expect(JSON.parse(response.body).count).to eq 2
    end

    it 'returns a 200 if the user is a super admin regardless of whether or not they are invited.' do
      invitation.update_attributes(accepted: false, is_admin: false,)
      user.update_attributes(is_super_admin: true)
      get_as_user path, params: { read: true, write: true }
      expect(response.status).to eq(200)
      expect(response.headers['Content-Type']).to match('json')
      expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
      expect(response.headers['Access-Control-Request-Method']).to eq('*')
      expect(JSON.parse(response.body).count).to eq 2
    end

    it 'returns a 401 if the user is not authorized', :skip_reqres do
      get path, params: { read: true, write: true }
      expect(response.status).to eq(401)
      expect(response.headers['Content-Type']).to match('json')
      expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
      expect(response.headers['Access-Control-Request-Method']).to eq('*')
    end

    it 'returns a 403 if the user is not an admin', :skip_reqres do
      invitation.update_attributes(is_admin: false)
      get_as_user path, params: { read: true, write: true }
      expect(response.status).to eq(403)
      expect(response.headers['Content-Type']).to match('json')
      expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
      expect(response.headers['Access-Control-Request-Method']).to eq('*')
    end

    it 'returns a 403 if the user is not invited to the parent organization', :skip_reqres do
      invitation.update_attributes(accepted: false)
      get_as_user path, params: { read: true, write: true }
      expect(response.status).to eq(403)
      expect(response.headers['Content-Type']).to match('json')
      expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
      expect(response.headers['Access-Control-Request-Method']).to eq('*')
    end

    it 'returns a 404 if an invitation does not exist', :skip_reqres do
      get_as_user missing_path, params: { read: true }
      expect(response.status).to eq(404)
      expect(response.headers['Content-Type']).to match('json')
      expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
      expect(response.headers['Access-Control-Request-Method']).to eq('*')
    end

  end

end
