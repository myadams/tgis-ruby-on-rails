require 'rails_helper'

describe "API V1 Calendars", type: :request, reqres_section: 'Calendars' do

  let(:accept_header) { 'application/vnd.film_cal-v1+json' }
  let(:user) { create :user, email: 'user@example.com', password: '12345678' }
  let(:existing_organization) { create :organization }
  let(:irrelevant_organization) { create :organization }
  let(:first_invitation) { create :invitation,
    user_id: user.id,
    organization_id: existing_organization.id,
    accepted: true }
  let(:second_invitation) { create :invitation,
    user_id: user.id,
    organization_id: irrelevant_organization.id,
    accepted: true }

  before(:example) do
    [existing_organization, irrelevant_organization, first_invitation, second_invitation].each { |x| x.touch }
  end

  context 'GET /api/organizations/:id/calendars' do

    let(:path) { "/api/organizations/#{existing_organization.subdomain}/calendars" }

    it 'returns an empty array of calendars', reqres_title: 'Retreive all calendars for a given organization' do
      get_as_user path
      expect(response.status).to eq(200)
      expect(JSON.parse(response.body)).to eq []
      expect(response.headers['Content-Type']).to match('json')
      expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
      expect(response.headers['Access-Control-Request-Method']).to eq('*')
    end

    context 'when results are present', :skip_reqres do
      let(:first_calendar) { create :calendar, organization_id: existing_organization.id }
      let(:second_calendar) { create :calendar, organization_id: existing_organization.id }

      before(:example) do
        [first_calendar, second_calendar].each { |x| x.touch }
      end

      context 'when a user has no granular permissions' do

        before(:each) do
          existing_organization.update_attributes(uses_granular_permissions: true)
        end

        it 'should return 0 calendars for a user without any permissions' do
          get_as_user path
          expect(response.status).to eq(200)
          expect(response.headers['Content-Type']).to match('json')
          expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
          expect(response.headers['Access-Control-Request-Method']).to eq('*')
          expect(JSON.parse(response.body).count).to eq 0
        end

        it 'should return 2 calendars for an admin without any permissions' do
          first_invitation.update_attributes(is_admin: true)
          get_as_user path
          expect(response.status).to eq(200)
          expect(response.headers['Content-Type']).to match('json')
          expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
          expect(response.headers['Access-Control-Request-Method']).to eq('*')
          expect(JSON.parse(response.body).count).to eq 2
        end

        it 'should return 2 calendars for a super admin without any permissions' do
          user.update_attributes(is_super_admin: true)
          get_as_user path
          expect(response.status).to eq(200)
          expect(response.headers['Content-Type']).to match('json')
          expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
          expect(response.headers['Access-Control-Request-Method']).to eq('*')
          expect(JSON.parse(response.body).count).to eq 2
        end

      end

      context 'when a user has permissions' do

        let(:read_permission) { create :granular_permission, read: true, write: false, invitation_id: first_invitation.id, calendar_id: first_calendar.id }
        let(:write_permission) { create :granular_permission, read: false, write: true, invitation_id: first_invitation.id, calendar_id: second_calendar.id }

        before(:each) do
          first_invitation.granular_permissions << [read_permission, write_permission]
          existing_organization.update_attributes(uses_granular_permissions: true)
        end

        it 'should return 1 calendars for a user with only 1 read permission' do
          get_as_user path
          expect(response.status).to eq(200)
          expect(response.headers['Content-Type']).to match('json')
          expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
          expect(response.headers['Access-Control-Request-Method']).to eq('*')
          expect(JSON.parse(response.body).count).to eq 1
        end

        it 'should return 2 calendars for a user with 2 read permissions' do
          write_permission.update_attributes(read: true)
          get_as_user path
          expect(response.status).to eq(200)
          expect(response.headers['Content-Type']).to match('json')
          expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
          expect(response.headers['Access-Control-Request-Method']).to eq('*')
          expect(JSON.parse(response.body).count).to eq 2
        end

        it 'should return 2 calendars for an admin without any permissions' do
          user.update_attributes(is_super_admin: true)
          get_as_user path
          expect(response.status).to eq(200)
          expect(response.headers['Content-Type']).to match('json')
          expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
          expect(response.headers['Access-Control-Request-Method']).to eq('*')
          expect(JSON.parse(response.body).count).to eq 2
        end

        it 'should return 2 calendars for a super admin without any permissions' do
          user.update_attributes(is_super_admin: true)
          get_as_user path
          expect(response.status).to eq(200)
          expect(response.headers['Content-Type']).to match('json')
          expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
          expect(response.headers['Access-Control-Request-Method']).to eq('*')
          expect(JSON.parse(response.body).count).to eq 2
        end

      end

      context 'when a user is not invited to an organization' do

        before(:each) do
          first_invitation.update_attributes(accepted: false)
        end

        it 'should return 0 calendars for a user' do
          get_as_user path
          expect(response.status).to eq(404)
          expect(response.headers['Content-Type']).to match('json')
          expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
          expect(response.headers['Access-Control-Request-Method']).to eq('*')
        end

        it 'should return 0 calendars for an admin' do
          first_invitation.update_attributes(is_admin: true)
          get_as_user path
          expect(response.status).to eq(404)
          expect(response.headers['Content-Type']).to match('json')
          expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
          expect(response.headers['Access-Control-Request-Method']).to eq('*')
        end

        it 'should return 2 calendars for a super admin' do
          user.update_attributes(is_super_admin: true)
          get_as_user path
          expect(response.status).to eq(200)
          expect(response.headers['Content-Type']).to match('json')
          expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
          expect(response.headers['Access-Control-Request-Method']).to eq('*')
          expect(JSON.parse(response.body).count).to eq 2
        end

      end

    end

    it 'returns a 401 not authorized error to an anonymous user', :skip_reqres do
      get path
      expect(response.response_code).to eq(401)
      expect(response.body).to match('OAuth')
      expect(response.headers['Content-Type']).to match('json')
      expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
      expect(response.headers['Access-Control-Request-Method']).to eq('*')
    end

    it 'returns a 401 if the current token has expired', :skip_reqres do
      get_as_user path, expired: true
      expect(response.status).to eq(401)
      expect(response.headers['Content-Type']).to match('json')
      expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
      expect(response.headers['Access-Control-Request-Method']).to eq('*')
    end

  end

  context 'GET /api/organizations/:id/calendars/:id' do

    let(:first_calendar) { create :calendar, organization_id: existing_organization.id }
    let(:second_calendar) { create :calendar, organization_id: existing_organization.id }
    let(:third_calendar) { create :calendar, organization_id: irrelevant_organization.id }
    let(:path) { "/api/organizations/#{existing_organization.subdomain}/calendars/#{first_calendar.id}" }

    before(:each) do
      existing_organization.calendars << [first_calendar, second_calendar]
      irrelevant_organization.calendars << second_calendar
    end

    # it 'returns a 200 with a single calendar for the requested ID', reqres_title: 'Retreive specific calendar for a given organization' do
    #   get_as_user path
    #   expect(response.status).to eq(200)
    #   expect(JSON.parse(response.body)["id"]).to eq first_calendar.id
    #   expect(response.headers['Content-Type']).to match('json')
    #   expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
    #   expect(response.headers['Access-Control-Request-Method']).to eq('*')
    # end

    it 'returns a 401 not authorized error to an anonymous user', :skip_reqres do
      get path
      expect(response.response_code).to eq(401)
      expect(response.body).to match('OAuth')
      expect(response.headers['Content-Type']).to match('json')
      expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
      expect(response.headers['Access-Control-Request-Method']).to eq('*')
    end

    it 'returns a 401 if the current token has expired', :skip_reqres do
      get_as_user path, expired: true
      expect(response.status).to eq(401)
      expect(response.headers['Content-Type']).to match('json')
      expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
      expect(response.headers['Access-Control-Request-Method']).to eq('*')
    end

    context 'when the user is not invited' do

      before(:each) do
        first_invitation.update_attributes(accepted: false)
      end

      it 'returns a 200 with the existing calendar for the requested ID if the user is a super admin', :skip_reqres do
        user.update_attributes(is_super_admin: true)
        get_as_user path
        expect(response.status).to eq(200)
        expect(JSON.parse(response.body)["id"]).to eq first_calendar.id
        expect(response.headers['Content-Type']).to match('json')
        expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
        expect(response.headers['Access-Control-Request-Method']).to eq('*')
      end

      it 'returns a 404 if the user is not an admin', :skip_reqres do
        get_as_user path
        expect(response.status).to eq(404)
        expect(response.headers['Content-Type']).to match('json')
        expect(response.headers['Content-Type']).to match('json')
        expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
        expect(response.headers['Access-Control-Request-Method']).to eq('*')
      end

    end

    context 'with granular permissions' do

      it 'returns a 200 with the existing calendar for the requested ID if the user is an admin', :skip_reqres do
        first_invitation.update_attributes(is_admin: true)
        get_as_user path
        expect(response.status).to eq(200)
        expect(JSON.parse(response.body)["id"]).to eq first_calendar.id
        expect(response.headers['Content-Type']).to match('json')
        expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
        expect(response.headers['Access-Control-Request-Method']).to eq('*')
      end

      it 'returns a 200 with the existing calendar for the requested ID if the user is a super admin', :skip_reqres do
        user.update_attributes(is_super_admin: true)
        get_as_user path
        expect(response.status).to eq(200)
        expect(JSON.parse(response.body)["id"]).to eq first_calendar.id
        expect(response.headers['Content-Type']).to match('json')
        expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
        expect(response.headers['Access-Control-Request-Method']).to eq('*')
      end

      it 'returns a 404 if the user does not have permission', :skip_reqres do
        get_as_user path
        expect(response.status).to eq(404)
        expect(response.headers['Content-Type']).to match('json')
        expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
        expect(response.headers['Access-Control-Request-Method']).to eq('*')
      end

      context 'with permissions present' do
        let(:read_permission) { create :granular_permission, read: true, write: false, invitation_id: first_invitation.id, calendar_id: first_calendar.id }

        before(:each) do
          first_invitation.granular_permissions << [read_permission]
        end

        it 'returns the existing calendar for the requested ID if the user has read permissions', :skip_reqres do
          get_as_user path
          expect(response.status).to eq(200)
          expect(JSON.parse(response.body)["id"]).to eq first_calendar.id
          expect(response.headers['Content-Type']).to match('json')
          expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
          expect(response.headers['Access-Control-Request-Method']).to eq('*')
        end

        it 'returns a 404 if the user does not have read permission', :skip_reqres do
          read_permission.update_attributes(read: false)
          get_as_user path
          expect(response.status).to eq(404)
          expect(response.headers['Content-Type']).to match('json')
          expect(response.headers['Content-Type']).to match('json')
          expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
          expect(response.headers['Access-Control-Request-Method']).to eq('*')
        end

      end

    end

  end

  context 'GET /api/organizations/:id/calendars/validations?name=%s' do

    context 'when results are present', :skip_reqres do
      let(:first_organization) { create :organization }
      let(:first_calendar) { create :calendar }

      before(:example) do
        [first_organization].each { |x| x.touch }
        first_organization.calendars << first_calendar
      end

      it 'returns a 200 with a valid response if the name is valid amongst existing organizations' do
        user.update_attribute(:is_super_admin, true)
        get_as_user "/api/organizations/#{first_organization.subdomain}/calendars/validations?name=Sumo%20Creations"
        expect(response.status).to eq(200)
        expect(response.headers['Content-Type']).to match('json')
        expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
        expect(response.headers['Access-Control-Request-Method']).to eq('*')
        expect(JSON.parse(response.body)["valid"]).to eq true
      end

      it 'returns a 200 with an invalid response if the invalid credentials are passed but always_ok is set to true' do
        user.update_attribute(:is_super_admin, true)
        get_as_user "/api/organizations/#{first_organization.subdomain}/calendars/validations?name=#{first_calendar.name}&always_ok=true"
        expect(response.status).to eq(200)
        expect(response.headers['Content-Type']).to match('json')
        expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
        expect(response.headers['Access-Control-Request-Method']).to eq('*')
        expect(JSON.parse(response.body)["valid"]).to eq false
      end

      it 'returns a 422 with an invalid response if the name is invalid amongst existing calendars for the organization' do
        user.update_attribute(:is_super_admin, true)
        get_as_user "/api/organizations/#{first_organization.subdomain}/calendars/validations?name=#{first_calendar.name}"
        expect(response.status).to eq(422)
        expect(response.headers['Content-Type']).to match('json')
        expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
        expect(response.headers['Access-Control-Request-Method']).to eq('*')
        expect(JSON.parse(response.body)["valid"]).to eq nil
        expect(JSON.parse(response.body)["errors"]["name"].nil?).to eq false
      end

      it 'returns a 401 not authorized error to an anonymous user', :skip_reqres do
        get "/api/organizations/#{first_organization.subdomain}/calendars/validations?name=Sumo%20Creations"
        expect(response.response_code).to eq(401)
        expect(response.body).to match('OAuth')
        expect(response.headers['Content-Type']).to match('json')
        expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
        expect(response.headers['Access-Control-Request-Method']).to eq('*')
      end

      it 'returns a 401 if the current token has expired', :skip_reqres do
        get_as_user  "/api/organizations/#{first_organization.subdomain}/calendars/validations?name=Sumo%20Creations", expired: true
        expect(response.status).to eq(401)
        expect(response.headers['Content-Type']).to match('json')
        expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
        expect(response.headers['Access-Control-Request-Method']).to eq('*')
      end

    end

  end

  context 'POST /api/calendars/' do

    let(:path) { "/api/organizations/#{existing_organization.subdomain}/calendars" }

    before(:each) do
      first_invitation.update_attribute(:is_admin, true)
    end

    it 'returns a 201 if the calendar is valid', reqres_title: 'Create a new calendar' do
      post_as_user path, params: { name: "Test", interrupted_by_holidays: false, interrupted_by_weekends: false }
      expect(response.status).to eq(201)
      expect(response.headers['Content-Type']).to match('json')
      expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
      expect(response.headers['Access-Control-Request-Method']).to eq('*')
      expect(Calendar.count).to eq(1)
    end

    context 'with templates' do
      let(:template_calendar) { create :calendar, organization_id: existing_organization.id }
      let(:category) { create :category }
      let(:inaccessible_calendar) { create :calendar}
      let(:style) { create :style, fill_color: '#ccddee' }

      before(:each) do
        existing_organization.calendars << template_calendar
        template_calendar.style = style
        template_calendar.categories << category
        template_calendar.save
        inaccessible_calendar.touch
      end

      it 'returns a 201 if the calendar is valid and uses a template calendar', :skip_reqres do
        post_as_user path, params: { name: "Test", interrupted_by_holidays: false, interrupted_by_weekends: false, template_calendar_id: template_calendar.id }
        expect(response.status).to eq(201)
        expect(response.headers['Content-Type']).to match('json')
        expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
        expect(response.headers['Access-Control-Request-Method']).to eq('*')
        expect(Calendar.count).to eq(3)
        new_calendar = Calendar.find(JSON.parse(response.body)["id"])
        expect(new_calendar.pending).to eq(true)
      end

      it 'returns a 404 if the calendar is valid but uses a template calendar that does not belong to the organization', :skip_reqres do
        post_as_user path, params: { name: "Test", interrupted_by_holidays: false, interrupted_by_weekends: false, template_calendar_id: inaccessible_calendar.id }
        expect(response.status).to eq(404)
        expect(response.headers['Content-Type']).to match('json')
        expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
        expect(response.headers['Access-Control-Request-Method']).to eq('*')
      end

      context 'using a snapshot' do
        before(:each) do
          snapshot = SnapshotTransaction.new(calendar: template_calendar, name: "Test", notes: "This is a test.", user_id: user.id)
          snapshot.run!
          @result = snapshot.result
          template_calendar.categories.each{|c| c.destroy}
        end

        it 'returns a 201 if the calendar is valid and uses a template snapshot', :skip_reqres do
          post_as_user path, params: { name: "Test", interrupted_by_holidays: false, interrupted_by_weekends: false, template_calendar_id: template_calendar.id, template_snapshot_id: @result.id }
          expect(response.status).to eq(201)
          expect(response.headers['Content-Type']).to match('json')
          expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
          expect(response.headers['Access-Control-Request-Method']).to eq('*')
          expect(Calendar.count).to eq(3)
          new_calendar = Calendar.find(JSON.parse(response.body)["id"])
          expect(new_calendar.pending).to eq(true)
        end

        it 'returns a 404' do
          snapshot = SnapshotTransaction.new(calendar: inaccessible_calendar, name: "Test", notes: "This is a test.", user_id: user.id)
          snapshot.run!
          post_as_user path, params: { name: "Test", interrupted_by_holidays: false, interrupted_by_weekends: false, template_calendar_id: template_calendar.id, template_snapshot_id: snapshot.result.id }
          expect(response.status).to eq(404)
          expect(response.headers['Content-Type']).to match('json')
          expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
          expect(response.headers['Access-Control-Request-Method']).to eq('*')
        end
      end
    end

    it 'returns a 401 if the token is expired', :skip_reqres do
      post_as_user path, params: { name: 'Test', interrupted_by_holidays: false, interrupted_by_weekends: false }, expired: true
      expect(response.status).to eq(401)
      expect(response.headers['Content-Type']).to match('json')
      expect(Calendar.count).to eq(0)
    end

    it 'returns a 403 if the user is not an admin', :skip_reqres do
      first_invitation.update_attribute(:is_admin, false)
      post_as_user path, params: { name: "Test", interrupted_by_holidays: false, interrupted_by_weekends: false }
      expect(response.status).to eq(403)
      expect(response.headers['Content-Type']).to match('json')
      expect(Calendar.count).to eq(0)
    end

    it 'returns a 404 if the user is not invited to the organization', :skip_reqres do
      first_invitation.update_attributes(accepted: false)
      post_as_user path, params: { name: "Test", interrupted_by_holidays: false, interrupted_by_weekends: false }
      expect(response.status).to eq(404)
      expect(response.headers['Content-Type']).to match('json')
      expect(Calendar.count).to eq(0)
    end

    it 'returns a 422 if the calendar is invalid', :skip_reqres do
      existing_calendar = create :calendar
      existing_organization.calendars << existing_calendar
      post_as_user path, params: { name: existing_calendar.name, interrupted_by_holidays: false, interrupted_by_weekends: false }
      expect(response.status).to eq(422)
      expect(response.headers['Content-Type']).to match('json')
      expect(Calendar.count).to eq(1) # Should not be 2
    end

  end

  context 'PUT /api/organizations/:id/calendars/:id' do

    let(:existing_calendar) { create :calendar }
    let(:path) { "/api/organizations/#{existing_organization.subdomain}/calendars/#{existing_calendar.id}" }

    before(:each) do
      existing_organization.calendars << existing_calendar
      first_invitation.update_attribute(:is_admin, true)
    end

    it 'returns a 200 if the calendar is valid', reqres_title: 'Update an existing calendar' do
      put_as_user path, params: { name: 'Test', interrupted_by_holidays: false, interrupted_by_weekends: false }
      expect(response.status).to eq(200)
      expect(response.headers['Content-Type']).to match('json')
      expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
      expect(response.headers['Access-Control-Request-Method']).to eq('*')
      expect(Calendar.find(existing_calendar.id).name).to eq('Test')
    end

    it 'returns a 401 if the token is expired', :skip_reqres do
      put_as_user path, params: { name: 'Test', interrupted_by_holidays: false, interrupted_by_weekends: false }, expired: true
      expect(response.status).to eq(401)
      expect(response.headers['Content-Type']).to match('json')
      expect(Calendar.find(existing_calendar.id).name).not_to eq('Test')
    end


    it 'returns a 403 if the user is not an admin', :skip_reqres do
      first_invitation.update_attribute(:is_admin, false)
      put_as_user path, params: { name: "Test", interrupted_by_holidays: false, interrupted_by_weekends: false }
      expect(response.status).to eq(403)
      expect(response.headers['Content-Type']).to match('json')
      expect(Calendar.find(existing_calendar.id).name).not_to eq('Test')
    end

    it 'returns a 404 if the calendar does not exist', :skip_reqres do
      put_as_user "/api/organizations/#{existing_organization.subdomain}/calendars/#{existing_calendar.id+999}" , params: { name: 'Test', interrupted_by_holidays: false, interrupted_by_weekends: false }
      expect(response.status).to eq(404)
      expect(response.headers['Content-Type']).to match('json')
    end

    it 'returns a 403 if the user is not invited to the organization', :skip_reqres do
      first_invitation.update_attributes(accepted: false)
      put_as_user path, params: { name: 'Test', interrupted_by_holidays: false, interrupted_by_weekends: false }
      expect(response.status).to eq(403)
      expect(response.headers['Content-Type']).to match('json')
    end

    it 'returns a 422 if the calendar is invalid', :skip_reqres do
      conflicting_calendar = create :calendar
      existing_organization.calendars << conflicting_calendar
      put_as_user path, params: { name: conflicting_calendar.name, interrupted_by_holidays: false, interrupted_by_weekends: false }
      expect(response.status).to eq(422)
      expect(response.headers['Content-Type']).to match('json')
      expect(Calendar.find(existing_calendar.id).name).not_to eq('Test')
    end

  end

  context 'DELETE /api/organizations/:id/calendars/:id' do

    let(:existing_calendar) { create :calendar }
    let(:path) { "/api/organizations/#{existing_organization.subdomain}/calendars/#{existing_calendar.id}" }

    before(:each) do
      existing_organization.calendars << existing_calendar
      first_invitation.update_attribute(:is_admin, true)
    end

    it 'returns a 200 if the calendar exists', reqres_title: 'Delete an existing calendar' do
      expect(Calendar.where(id: existing_calendar.id).count).to eq(1)
      delete_as_user path
      expect(response.status).to eq(200)
      expect(response.headers['Content-Type']).to match('json')
      expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
      expect(response.headers['Access-Control-Request-Method']).to eq('*')
      expect(Calendar.where(id: existing_calendar.id).count).to eq(0)
      expect(JSON.parse(response.body)["is_deleted"]).to eq(true)
    end

    it 'returns a 200 if the calendar exists and the user is a super admin without any connection to the organization' do
      first_invitation.update_attributes(accepted: false)
      user.update_attributes(is_super_admin: true)
      expect(Calendar.where(id: existing_calendar.id).count).to eq(1)
      delete_as_user path
      expect(response.status).to eq(200)
      expect(response.headers['Content-Type']).to match('json')
      expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
      expect(response.headers['Access-Control-Request-Method']).to eq('*')
      expect(Calendar.where(id: existing_calendar.id).count).to eq(0)
    end

    it 'returns a 401 if the user is not authorized' do
      expect(Calendar.where(id: existing_calendar.id).count).to eq(1)
      delete path
      expect(response.status).to eq(401)
      expect(response.headers['Content-Type']).to match('json')
      expect(Calendar.where(id: existing_calendar.id).count).to eq(1)
    end

    it 'returns a 401 if the user uses an expired token' do
      expect(Calendar.where(id: existing_calendar.id).count).to eq(1)
      delete_as_user path, expired: true
      expect(response.status).to eq(401)
      expect(response.headers['Content-Type']).to match('json')
      expect(Calendar.where(id: existing_calendar.id).count).to eq(1)
    end

    it 'returns a 403 if the calendar exists and the user is not an admin' do
      first_invitation.update_attributes(is_admin: false)
      expect(Calendar.where(id: existing_calendar.id).count).to eq(1)
      delete_as_user path
      expect(response.status).to eq(403)
      expect(response.headers['Content-Type']).to match('json')
      expect(Calendar.where(id: existing_calendar.id).count).to eq(1)
    end

    it 'returns a 403 if the calendar exists and the user is an admin without any connection to the organization' do
      first_invitation.update_attributes(accepted: false)
      expect(Calendar.where(id: existing_calendar.id).count).to eq(1)
      delete_as_user path
      expect(response.status).to eq(403)
      expect(response.headers['Content-Type']).to match('json')
      expect(Calendar.where(id: existing_calendar.id).count).to eq(1)
    end

    it 'returns a 404 if the calendar does not exist' do
      existing_calendar.destroy
      delete_as_user path
      expect(response.status).to eq(404)
      expect(response.headers['Content-Type']).to match('json')
    end


  end

  context 'PATCH /api/organizations/:id/calendars/:id/push' do
    let(:calendar) { create :calendar, organization_id: existing_organization.id }
    let(:item_1) { create :item, calendar_id: calendar.id, locked: false}
    let(:item_2) { create :item, calendar_id: calendar.id, locked: false}
    let(:event_1) { create :event, calendar_id: calendar.id, starts_at: 0.days.from_now, length: 2, item_id: item_1.id }
    let(:event_2) { create :event, calendar_id: calendar.id, starts_at: 2.days.from_now, length: 3, item_id: item_2.id }
    let(:event_3) { create :event, calendar_id: calendar.id, starts_at: 6 .days.from_now, length: 1, item_id: item_1.id }
    let(:path) { "/api/organizations/#{existing_organization.subdomain}/calendars/#{calendar.id}/push" }

    before(:each) do
      first_invitation.update_attribute(:is_admin, true)
      calendar.items << [item_1, item_2]
      calendar.events << [event_1, event_2, event_3]
      calendar.update_attributes(interrupted_by_weekends: true)
    end

    it 'returns an updated list of affected events', reqres_title: 'Push all events on a calendar after a specific date' do
      patch_as_user path, params: {date: 3.days.from_now, offset: 2, fill_unused_days: false}
      expect(response.status).to eq(200)
      body = JSON.parse(response.body)
      event_ids = body.map{|e| e["id"]}
      expect(event_ids.include?(event_1.id)).to eq false
      expect(event_ids.include?(event_2.id)).to eq true
      expect(event_ids.include?(event_3.id)).to eq true
      expect(response.headers['Content-Type']).to match('json')
      expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
      expect(response.headers['Access-Control-Request-Method']).to eq('*')
    end

    it 'should not include locked events', :skip_reqres do
      item_2.update_attributes!(locked: true)
      patch_as_user path, params: {date: 3.days.from_now, offset: 2, fill_unused_days: false}
      expect(response.status).to eq(200)
      body = JSON.parse(response.body)
      event_ids = body.map{|e| e["id"]}
      expect(event_ids.include?(event_1.id)).to eq false
      expect(event_ids.include?(event_2.id)).to eq false
      expect(event_ids.include?(event_3.id)).to eq true
      expect(response.headers['Content-Type']).to match('json')
      expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
      expect(response.headers['Access-Control-Request-Method']).to eq('*')
    end

    it 'should only push events that match category filters', :skip_reqres do
      # Testing inheritance
      category_1 = create(:category, calendar_id: calendar.id)
      category_2 = create(:category, calendar_id: calendar.id, category_id: category_1.id)
      category_3 = create(:category, calendar_id: calendar.id, category_id: category_2.id)
      item_2.update_attributes!(category_id: category_3.id)
      patch_as_user path, params: {date: 1.days.from_now, offset: 2, fill_unused_days: false, categories: [category_1.id]}
      expect(response.status).to eq(200)
      body = JSON.parse(response.body)
      event_ids = body.map{|e| e["id"]}
      expect(event_ids.include?(event_1.id)).to eq false
      expect(event_ids.include?(event_2.id)).to eq true
      expect(event_ids.include?(event_3.id)).to eq false
      expect(response.headers['Content-Type']).to match('json')
      expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
      expect(response.headers['Access-Control-Request-Method']).to eq('*')
    end

    it 'should only push events that match item filters', :skip_reqres do
      patch_as_user path, params: {date: 0.days.from_now, offset: 2, fill_unused_days: false, items: [item_1.id]}
      expect(response.status).to eq(200)
      body = JSON.parse(response.body)
      event_ids = body.map{|e| e["id"]}
      expect(event_ids.include?(event_1.id)).to eq true
      expect(event_ids.include?(event_2.id)).to eq false
      expect(event_ids.include?(event_3.id)).to eq true
      expect(response.headers['Content-Type']).to match('json')
      expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
      expect(response.headers['Access-Control-Request-Method']).to eq('*')
    end
  end

end
