require 'rails_helper'

describe "API V1 Items", type: :request, reqres_section: 'Items' do

  let(:accept_header) { 'application/vnd.film_cal-v1+json' }
  let(:user) { create :user, email: 'user@example.com', password: '12345678' }
  let(:existing_organization) { create :organization }
  let(:irrelevant_organization) { create :organization }
  let(:first_invitation) { create :invitation,
    user_id: user.id,
    organization_id: existing_organization.id,
    accepted: true }
  let(:second_invitation) { create :invitation,
    user_id: user.id,
    organization_id: irrelevant_organization.id,
    accepted: true }
  let(:sample_params) {{
    name: 'Test',
    use_custom_styles: false,
    active: false,
    locked: false
  }}
  let(:style_sample_params) {{
    use_custom_styles: true,
    style: {
      fill_color: '#fff',
      outline_color: '#000',
      text_decoration: 'none',
      text_color: '#000',
      text_alignment: 'left',
      font: 'Default',
      font_weight: 'normal',
      font_size: 12,
      font_style: 'italic'
    }
  }}
  let(:invalid_style_sample_params) {{
    use_custom_styles: true,
    name: 'customized',
    style: {
      fill_color: '#fff',
      outline_color: '#000',
      text_decoration: 'none',
      text_color: '#000',
      text_alignment: 'NONE',
      font: 'Sonoma Light TTF',
      font_weight: 'light',
      font_size: 28,
      font_style: 'italic'
    }
  }}

  before(:example) do
    [existing_organization, irrelevant_organization, first_invitation, second_invitation].each { |x| x.touch }
  end

  context 'GET /api/organizations/:id/calendars/:id/items' do

    let(:first_calendar) { create :calendar, organization_id: existing_organization.id }
    let(:second_calendar) { create :calendar, organization_id: existing_organization.id }
    let(:first_category) { create :category, calendar_id: first_calendar.id }
    let(:second_category) { create :category, calendar_id: second_calendar.id }
    let(:first_item) { create :item, category_id: first_category.id }
    let(:second_item) { create :item, category_id: first_category.id }
    let(:third_item) { create :item, category_id: second_category.id }
    let(:path) { "/api/organizations/#{existing_organization.subdomain}/calendars/#{first_calendar.id}/items?category_id=#{first_category.id}" }

    before(:each) do
      existing_organization.calendars << [first_calendar, second_calendar]
      irrelevant_organization.calendars << second_calendar
      first_calendar.categories << [first_category, second_category]
      first_category.items << [first_item, second_item]
      second_category.items << [third_item]
    end

    it 'returns a 200 with the existing items for the requested calendar if the user is an admin', :skip_reqres do
      first_invitation.update_attributes(is_admin: true)
      get_as_user path
      expect(response.status).to eq(200)
      body = JSON.parse(response.body)
      expect(body.length).to eq 2
      expect(body[0]["id"]).to eq first_item.id
      expect(body[1]["id"]).to eq second_item.id
      expect(response.headers['Content-Type']).to match('json')
      expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
      expect(response.headers['Access-Control-Request-Method']).to eq('*')
    end

    it 'returns a 200 with the existing items for the requested calendar if the user is a super admin', :skip_reqres do
      user.update_attributes(is_super_admin: true)
      get_as_user path
      expect(response.status).to eq(200)
      body = JSON.parse(response.body)
      expect(body.length).to eq 2
      expect(body[0]["id"]).to eq first_item.id
      expect(body[1]["id"]).to eq second_item.id
      expect(response.headers['Content-Type']).to match('json')
      expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
      expect(response.headers['Access-Control-Request-Method']).to eq('*')
    end

    it 'returns a 404 if the user does not have permission', :skip_reqres do
      get_as_user path
      expect(response.status).to eq(404)
      expect(response.headers['Content-Type']).to match('json')
      expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
      expect(response.headers['Access-Control-Request-Method']).to eq('*')
    end

    it 'returns a 401 not authorized error to an anonymous user', :skip_reqres do
      get path
      expect(response.response_code).to eq(401)
      expect(response.body).to match('OAuth')
      expect(response.headers['Content-Type']).to match('json')
      expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
      expect(response.headers['Access-Control-Request-Method']).to eq('*')
    end

    it 'returns a 401 if the current token has expired', :skip_reqres do
      get_as_user path, expired: true
      expect(response.status).to eq(401)
      expect(response.headers['Content-Type']).to match('json')
      expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
      expect(response.headers['Access-Control-Request-Method']).to eq('*')
    end

    context 'when the user is not invited' do

      before(:each) do
        first_invitation.update_attributes(accepted: false)
      end

      it 'returns a 200 with the existing calendar for the requested ID if the user is a super admin', :skip_reqres do
        user.update_attributes(is_super_admin: true)
        get_as_user path
        expect(response.status).to eq(200)
        body = JSON.parse(response.body)
        expect(body.length).to eq 2
        expect(body[0]["id"]).to eq first_item.id
        expect(body[1]["id"]).to eq second_item.id
        expect(response.headers['Content-Type']).to match('json')
        expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
        expect(response.headers['Access-Control-Request-Method']).to eq('*')
      end

      it 'returns a 404 if the user is not an admin', :skip_reqres do
        get_as_user path
        expect(response.status).to eq(404)
        expect(response.headers['Content-Type']).to match('json')
        expect(response.headers['Content-Type']).to match('json')
        expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
        expect(response.headers['Access-Control-Request-Method']).to eq('*')
      end

    end

    context 'with permissions present' do
      let(:read_permission) { create :granular_permission, read: true, write: false, invitation_id: first_invitation.id, calendar_id: first_calendar.id }

      before(:each) do
        first_invitation.granular_permissions << [read_permission]
      end

      it 'returns the existing calendar for the requested ID if the user has read permissions', :skip_reqres do
        get_as_user path
        expect(response.status).to eq(200)
        body = JSON.parse(response.body)
        expect(body.length).to eq 2
        expect(body[0]["id"]).to eq first_item.id
        expect(body[1]["id"]).to eq second_item.id
        expect(response.headers['Content-Type']).to match('json')
        expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
        expect(response.headers['Access-Control-Request-Method']).to eq('*')
      end

      it 'returns a 404 if the user does not have read permission', :skip_reqres do
        read_permission.update_attributes(read: false)
        get_as_user path
        expect(response.status).to eq(404)
        expect(response.headers['Content-Type']).to match('json')
        expect(response.headers['Content-Type']).to match('json')
        expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
        expect(response.headers['Access-Control-Request-Method']).to eq('*')
      end

    end

  end

  context 'GET /api/organizations/:id/calendars/:id/items/:id' do

    let(:first_calendar) { create :calendar, organization_id: existing_organization.id }
    let(:first_category) { create :category, calendar_id: first_calendar.id }
    let(:first_item) { create :item, category_id: first_category.id, calendar_id: first_calendar.id }
    let(:path) { "/api/organizations/#{existing_organization.subdomain}/calendars/#{first_calendar.id}/items/#{first_item.id}" }

    before(:each) do
      existing_organization.calendars << first_calendar
      first_calendar.items << first_item
      first_calendar.categories << first_category
      first_category.items << first_item
    end

    it 'returns a 200 with the existing item for the requested ID if the user is an admin', :skip_reqres do
      first_invitation.update_attributes(is_admin: true)
      get_as_user path
      expect(response.status).to eq(200)
      expect(JSON.parse(response.body)["id"]).to eq first_item.id
      expect(response.headers['Content-Type']).to match('json')
      expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
      expect(response.headers['Access-Control-Request-Method']).to eq('*')
    end

    it 'returns a 200 with the existing item for the requested ID if the user is a super admin', :skip_reqres do
      user.update_attributes(is_super_admin: true)
      get_as_user path
      expect(response.status).to eq(200)
      expect(JSON.parse(response.body)["id"]).to eq first_item.id
      expect(response.headers['Content-Type']).to match('json')
      expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
      expect(response.headers['Access-Control-Request-Method']).to eq('*')
    end

    it 'returns a 404 if the user does not have permission', :skip_reqres do
      get_as_user path
      expect(response.status).to eq(404)
      expect(response.headers['Content-Type']).to match('json')
      expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
      expect(response.headers['Access-Control-Request-Method']).to eq('*')
    end

    it 'returns a 401 not authorized error to an anonymous user', :skip_reqres do
      get path
      expect(response.response_code).to eq(401)
      expect(response.body).to match('OAuth')
      expect(response.headers['Content-Type']).to match('json')
      expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
      expect(response.headers['Access-Control-Request-Method']).to eq('*')
    end

    it 'returns a 401 if the current token has expired', :skip_reqres do
      get_as_user path, expired: true
      expect(response.status).to eq(401)
      expect(response.headers['Content-Type']).to match('json')
      expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
      expect(response.headers['Access-Control-Request-Method']).to eq('*')
    end

    context 'when the user is not invited' do

      before(:each) do
        first_invitation.update_attributes(accepted: false)
      end

      it 'returns a 200 with the existing item for the requested ID if the user is a super admin', :skip_reqres do
        user.update_attributes(is_super_admin: true)
        get_as_user path
        expect(response.status).to eq(200)
        expect(JSON.parse(response.body)["id"]).to eq first_item.id
        expect(response.headers['Content-Type']).to match('json')
        expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
        expect(response.headers['Access-Control-Request-Method']).to eq('*')
      end

      it 'returns a 404 if the user is an admin but has not accepted their invitation', :skip_reqres do
        first_invitation.update_attributes(is_admin: true)
        get_as_user path
        expect(response.status).to eq(404)
        expect(response.headers['Content-Type']).to match('json')
        expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
        expect(response.headers['Access-Control-Request-Method']).to eq('*')
      end

      it 'returns a 404 if the user is not an admin', :skip_reqres do
        get_as_user path
        expect(response.status).to eq(404)
        expect(response.headers['Content-Type']).to match('json')
        expect(response.headers['Content-Type']).to match('json')
        expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
        expect(response.headers['Access-Control-Request-Method']).to eq('*')
      end

    end

    context 'with permissions present' do
      let(:read_permission) { create :granular_permission, read: true, write: false, invitation_id: first_invitation.id, calendar_id: first_calendar.id }

      before(:each) do
        first_invitation.granular_permissions << [read_permission]
      end

      it 'returns the existing calendar for the requested ID if the user has read permissions', :skip_reqres do
        get_as_user path
        expect(response.status).to eq(200)
        expect(JSON.parse(response.body)["id"]).to eq first_item.id
        expect(response.headers['Content-Type']).to match('json')
        expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
        expect(response.headers['Access-Control-Request-Method']).to eq('*')
      end

      it 'returns a 404 if the user does not have read permission', :skip_reqres do
        read_permission.update_attributes(read: false)
        get_as_user path
        expect(response.status).to eq(404)
        expect(response.headers['Content-Type']).to match('json')
        expect(response.headers['Content-Type']).to match('json')
        expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
        expect(response.headers['Access-Control-Request-Method']).to eq('*')
      end

    end

  end

  context 'POST /api/organizations/:id/calendars/:id/items' do

    let(:first_calendar) { create :calendar, organization_id: existing_organization.id }
    let(:first_category) { create :category, calendar_id: first_calendar.id }
    let(:path) { "/api/organizations/#{existing_organization.subdomain}/calendars/#{first_calendar.id}/items?category_id=#{first_category.id}" }

    before(:each) do
      existing_organization.calendars << first_calendar
      first_calendar.categories << first_category
    end

    context 'if the user is an admin' do

      before(:each) do
        first_invitation.update_attribute(:is_admin, true)
      end

      it 'returns a 201 if the item is valid', reqres_title: 'Create a new item' do
        post_as_user path, params: sample_params
        expect(response.status).to eq(201)
        expect(response.headers['Content-Type']).to match('json')
        expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
        expect(response.headers['Access-Control-Request-Method']).to eq('*')
        expect(Item.count).to eq(1)
      end

      it 'returns a 201 if the category has specified styles', :skip_reqres do
        sample_params.merge!(style_sample_params)
        post_as_user path, params: sample_params
        expect(response.status).to eq(201)
        expect(response.headers['Content-Type']).to match('json')
        expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
        expect(response.headers['Access-Control-Request-Method']).to eq('*')
        expect(Item.count).to eq(1)
      end

      it 'returns a 401 if the token is expired', :skip_reqres do
        post_as_user path, params: sample_params, expired: true
        expect(response.status).to eq(401)
        expect(response.headers['Content-Type']).to match('json')
        expect(Item.count).to eq(0)
      end

      it 'returns a 404 if the user is not an admin', :skip_reqres do
        first_invitation.update_attribute(:is_admin, false)
        post_as_user path, params: sample_params
        expect(response.status).to eq(404)
        expect(response.headers['Content-Type']).to match('json')
        expect(Item.count).to eq(0)
      end

      it 'returns a 404 if the user is not invited to the organization', :skip_reqres do
        first_invitation.update_attributes(accepted: false)
        post_as_user path, params: sample_params
        expect(response.status).to eq(404)
        expect(response.headers['Content-Type']).to match('json')
        expect(Item.count).to eq(0)
      end

      it 'returns a 422 if the item is invalid', :skip_reqres do
        sample_params[:name] = ""
        post_as_user path, params: sample_params
        expect(response.status).to eq(422)
        expect(response.headers['Content-Type']).to match('json')
        expect(Item.count).to eq(0)
      end

      it 'returns a 422 if the category has specified styles but none are present', :skip_reqres do
        sample_params.merge!(invalid_style_sample_params)
        post_as_user path, params: sample_params
        expect(response.status).to eq(422)
        expect(response.headers['Content-Type']).to match('json')
        expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
        expect(response.headers['Access-Control-Request-Method']).to eq('*')
        expect(Item.count).to eq(0)
      end

    end

    context 'if the user only has read access' do
      let(:permission) { create :granular_permission, calendar_id: first_calendar.id, read: true }

      before(:each) do
        first_invitation.update_attribute(:is_admin, false)
        first_invitation.granular_permissions << permission
      end

      it 'returns a 401 if the token is expired', :skip_reqres do
        post_as_user path, params: sample_params, expired: true
        expect(response.status).to eq(401)
        expect(response.headers['Content-Type']).to match('json')
        expect(Item.count).to eq(0)
      end

      it 'returns a 404 if the user is not invited to the organization', :skip_reqres do
        first_invitation.update_attributes(accepted: false)
        post_as_user path, params: sample_params
        expect(response.status).to eq(404)
        expect(response.headers['Content-Type']).to match('json')
        expect(Item.count).to eq(0)
      end

    end

    context 'if the user only has write access' do
      let(:permission) { create :granular_permission, calendar_id: first_calendar.id, read: true, write: true }

      before(:each) do
        first_invitation.update_attribute(:is_admin, false)
        first_invitation.granular_permissions << permission
      end

      it 'returns a 401 if the token is expired', :skip_reqres do
        post_as_user path, params: sample_params, expired: true
        expect(response.status).to eq(401)
        expect(response.headers['Content-Type']).to match('json')
        expect(Item.count).to eq(0)
      end

      it 'returns a 404 if the user is not invited to the organization', :skip_reqres do
        first_invitation.update_attributes(accepted: false)
        post_as_user path, params: sample_params
        expect(response.status).to eq(404)
        expect(response.headers['Content-Type']).to match('json')
        expect(Item.count).to eq(0)
      end

      it 'returns a 201 if the item is valid', reqres_title: 'Create a new item' do
        post_as_user path, params: sample_params
        expect(response.status).to eq(201)
        expect(response.headers['Content-Type']).to match('json')
        expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
        expect(response.headers['Access-Control-Request-Method']).to eq('*')
        expect(Item.count).to eq(1)
      end

      it 'returns a 422 if the item is invalid', :skip_reqres do
        sample_params[:name] = ""
        post_as_user path, params: sample_params
        expect(response.status).to eq(422)
        expect(response.headers['Content-Type']).to match('json')
        expect(Item.count).to eq(0)
      end

    end

  end

  context 'PUT /api/organizations/:id/calendars/:id/items/:id' do

    let(:existing_calendar) { create :calendar }
    let(:existing_item) { create :item }
    let(:path) { "/api/organizations/#{existing_organization.subdomain}/calendars/#{existing_calendar.id}/items/#{existing_item.id}" }
    let(:permission) { create :granular_permission, calendar_id: existing_calendar.id, invitation_id: first_invitation.id }

    before(:each) do
      existing_organization.calendars << existing_calendar
      existing_calendar.items << existing_item
      existing_calendar.granular_permissions << permission
    end

    it 'returns a 200 if the item is valid and user is an admin', reqres_title: 'Update an existing item' do
      first_invitation.update_attribute(:is_admin, true)
      put_as_user path, params: sample_params
      expect(response.status).to eq(200)
      expect(response.headers['Content-Type']).to match('json')
      expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
      expect(response.headers['Access-Control-Request-Method']).to eq('*')
      expect(Item.find(existing_item.id).name).to eq('Test')
    end

    it 'returns a 200 if the item is valid and user is an admin and has specified styles', reqres_title: 'Update an existing item' do
      first_invitation.update_attribute(:is_admin, true)
      put_as_user path, params: sample_params.merge!(style_sample_params)
      expect(response.status).to eq(200)
      expect(response.headers['Content-Type']).to match('json')
      expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
      expect(response.headers['Access-Control-Request-Method']).to eq('*')
      expect(Item.find(existing_item.id).name).to eq('Test')
    end

    it 'returns a 200 if the item is valid and user has write access', reqres_title: 'Update an existing item' do
      permission.update_attributes(write: true)
      put_as_user path, params: sample_params
      expect(response.status).to eq(200)
      expect(response.headers['Content-Type']).to match('json')
      expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
      expect(response.headers['Access-Control-Request-Method']).to eq('*')
      expect(Item.find(existing_item.id).name).to eq('Test')
    end

    it 'returns a 401 if the token is expired', :skip_reqres do
      put_as_user path, params: { name: 'Test' }, expired: true
      expect(response.status).to eq(401)
      expect(response.headers['Content-Type']).to match('json')
      expect(Item.find(existing_item.id).name).not_to eq('Test')
    end

    it 'returns a 400 if the params are incomplete', :skip_reqres do
      first_invitation.update_attribute(:is_admin, false)
      put_as_user path, params: {name: 'new name'}
      expect(response.status).to eq(400)
      expect(response.headers['Content-Type']).to match('json')
      expect(Item.find(existing_item.id).name).not_to eq('Test')
    end

    it 'returns a 404 if the user does not have write access', :skip_reqres do
      put_as_user path, params: sample_params
      expect(response.status).to eq(404)
      expect(response.headers['Content-Type']).to match('json')
      expect(Item.find(existing_item.id).name).not_to eq('Test')
    end

    it 'returns a 404 if the item does not exist', :skip_reqres do
      put_as_user "/api/organizations/#{existing_organization.subdomain}/calendars/#{existing_calendar.id}/items/#{existing_item.id+999}/?category_id=#{category_id}" , params: sample_params
      expect(response.status).to eq(404)
      expect(response.headers['Content-Type']).to match('json')
    end

    it 'returns a 404 if the user is not invited to the organization', :skip_reqres do
      first_invitation.update_attributes(accepted: false)
      put_as_user path, params: sample_params
      expect(response.status).to eq(404)
      expect(response.headers['Content-Type']).to match('json')
    end

    it 'returns a 422 if the item is invalid', :skip_reqres do
      permission.update_attributes(write: true)
      sample_params[:name] = ""
      put_as_user path, params: sample_params
      expect(response.status).to eq(422)
      expect(response.headers['Content-Type']).to match('json')
      expect(Item.find(existing_item.id).name).not_to eq('Test')
    end

    it 'returns a 422 if the item is valid and has specified styles but none are present', reqres_title: 'Update an existing item' do
      first_invitation.update_attribute(:is_admin, true)
      sample_params.merge!(invalid_style_sample_params)
      put_as_user path, params: sample_params
      expect(response.status).to eq(422)
      expect(response.headers['Content-Type']).to match('json')
      expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
      expect(response.headers['Access-Control-Request-Method']).to eq('*')
      expect(Item.find(existing_item.id).name).not_to eq('Test')
    end

  end

  context 'PATCH /api/organizations/:id/calendars/:id/items/:id/lock' do

    let(:existing_calendar) { create :calendar }
    let(:existing_item) { create :item, locked: false }
    let(:path) { "/api/organizations/#{existing_organization.subdomain}/calendars/#{existing_calendar.id}/items/#{existing_item.id}/lock" }
    let(:permission) { create :granular_permission, calendar_id: existing_calendar.id, invitation_id: first_invitation.id }

    before(:each) do
      existing_organization.calendars << existing_calendar
      existing_calendar.items << [existing_item]
      existing_calendar.granular_permissions << permission
    end

    it 'returns a 200 if the item is valid and user is an admin', reqres_title: 'Update an existing item' do
      first_invitation.update_attribute(:is_admin, true)
      patch_as_user path, params: {locked: true}
      expect(response.status).to eq(200)
      expect(response.headers['Content-Type']).to match('json')
      expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
      expect(response.headers['Access-Control-Request-Method']).to eq('*')
      expect(Item.find(existing_item.id).locked).to eq(true)
    end

    it 'returns a 200 if the item is valid and user is an admin and the locked status is reverted', reqres_title: 'Update an existing item' do
      first_invitation.update_attribute(:is_admin, true)
      existing_item.update_attributes(locked: true)
      patch_as_user path, params: {locked: false}
      expect(response.status).to eq(200)
      expect(response.headers['Content-Type']).to match('json')
      expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
      expect(response.headers['Access-Control-Request-Method']).to eq('*')
      expect(Item.find(existing_item.id).locked).to eq(false)
    end

    it 'returns a 200 if the item is valid and user has write access', reqres_title: 'Update an existing item' do
      permission.update_attributes(write: true)
      patch_as_user path, params: {locked: true}
      expect(response.status).to eq(200)
      expect(response.headers['Content-Type']).to match('json')
      expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
      expect(response.headers['Access-Control-Request-Method']).to eq('*')
      expect(Item.find(existing_item.id).locked).to eq(true)
    end

    it 'returns a 401 if the token is expired', :skip_reqres do
      patch_as_user path, params: { name: 'Test' }, expired: true
      expect(response.status).to eq(401)
      expect(response.headers['Content-Type']).to match('json')
      expect(Item.find(existing_item.id).name).not_to eq('Test')
    end

    it 'returns a 400 if the params are incomplete', :skip_reqres do
      first_invitation.update_attribute(:is_admin, false)
      patch_as_user path, params: {}
      expect(response.status).to eq(400)
      expect(response.headers['Content-Type']).to match('json')
      expect(Item.find(existing_item.id).name).not_to eq('Test')
    end

    it 'returns a 404 if the user does not have write access', :skip_reqres do
      patch_as_user path, params: {locked: true}
      expect(response.status).to eq(404)
      expect(response.headers['Content-Type']).to match('json')
      expect(Item.find(existing_item.id).name).not_to eq('Test')
    end

    it 'returns a 404 if the item does not exist', :skip_reqres do
      patch_as_user "/api/organizations/#{existing_organization.subdomain}/calendars/#{existing_calendar.id}/items/#{existing_item.id+999}/lock" , params: {locked: true}
      expect(response.status).to eq(404)
      expect(response.headers['Content-Type']).to match('json')
    end

    it 'returns a 404 if the user is not invited to the organization', :skip_reqres do
      first_invitation.update_attributes(accepted: false)
      patch_as_user path, params: {locked: true}
      expect(response.status).to eq(404)
      expect(response.headers['Content-Type']).to match('json')
    end

  end

  context 'PATCH /api/organizations/:id/calendars/:id/items/:id/parent' do

    let(:existing_calendar) { create :calendar }
    let(:existing_category) { create :category, calendar_id: existing_calendar.id }
    let(:existing_item) { create :item, active: false }
    let(:path) { "/api/organizations/#{existing_organization.subdomain}/calendars/#{existing_calendar.id}/items/#{existing_item.id}/parent" }
    let(:permission) { create :granular_permission, calendar_id: existing_calendar.id, invitation_id: first_invitation.id }

    before(:each) do
      existing_organization.calendars << existing_calendar
      existing_calendar.categories << existing_category
      existing_calendar.items << [existing_item]
      existing_calendar.granular_permissions << permission
    end

    it 'returns a 200 if the item is valid and user is an admin', reqres_title: 'Update an existing item' do
      first_invitation.update_attribute(:is_admin, true)
      patch_as_user path, params: {category_id: existing_category.id}
      expect(response.status).to eq(200)
      expect(response.headers['Content-Type']).to match('json')
      expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
      expect(response.headers['Access-Control-Request-Method']).to eq('*')
      expect(Item.find(existing_item.id).category_id).to eq(existing_category.id)
    end

    it 'returns a 200 if the item is valid and user is an admin and the category_id sexisting_category.ids is reverted', reqres_title: 'Update an existing item' do
      first_invitation.update_attribute(:is_admin, true)
      existing_item.update_attributes(category_id: existing_category.id)
      patch_as_user path, params: {category_id: nil}
      expect(response.status).to eq(200)
      expect(response.headers['Content-Type']).to match('json')
      expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
      expect(response.headers['Access-Control-Request-Method']).to eq('*')
      expect(Item.find(existing_item.id).category_id).to eq(nil)
    end

    it 'returns a 200 if the item is valid and user has write access', reqres_title: 'Update an existing item' do
      permission.update_attributes(write: true)
      patch_as_user path, params: {category_id: existing_category.id}
      expect(response.status).to eq(200)
      expect(response.headers['Content-Type']).to match('json')
      expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
      expect(response.headers['Access-Control-Request-Method']).to eq('*')
      expect(Item.find(existing_item.id).category_id).to eq(existing_category.id)
    end

    it 'returns a 401 if the token is expired', :skip_reqres do
      patch_as_user path, params: { name: 'Test' }, expired: true
      expect(response.status).to eq(401)
      expect(response.headers['Content-Type']).to match('json')
      expect(Item.find(existing_item.id).name).not_to eq('Test')
    end

    it 'returns a 400 if the params are incomplete', :skip_reqres do
      first_invitation.update_attribute(:is_admin, false)
      patch_as_user path, params: {}
      expect(response.status).to eq(400)
      expect(response.headers['Content-Type']).to match('json')
      expect(Item.find(existing_item.id).name).not_to eq('Test')
    end

    it 'returns a 404 if the user does not have write access', :skip_reqres do
      patch_as_user path, params: {category_id: existing_category.id}
      expect(response.status).to eq(404)
      expect(response.headers['Content-Type']).to match('json')
      expect(Item.find(existing_item.id).name).not_to eq('Test')
    end

    it 'returns a 404 if the item does not exist', :skip_reqres do
      patch_as_user "/api/organizations/#{existing_organization.subdomain}/calendars/#{existing_calendar.id}/items/#{existing_item.id+999}/parent" , params: {category_id: existing_category.id}
      expect(response.status).to eq(404)
      expect(response.headers['Content-Type']).to match('json')
    end

    it 'returns a 404 if the user is not invited to the organization', :skip_reqres do
      first_invitation.update_attributes(accepted: false)
      patch_as_user path, params: {category_id: existing_category.id}
      expect(response.status).to eq(404)
      expect(response.headers['Content-Type']).to match('json')
    end

  end

  context 'PATCH /api/organizations/:id/calendars/:id/items/:id/activate' do

    let(:existing_calendar) { create :calendar }
    let(:existing_item) { create :item, active: false }
    let(:path) { "/api/organizations/#{existing_organization.subdomain}/calendars/#{existing_calendar.id}/items/#{existing_item.id}/activate" }
    let(:permission) { create :granular_permission, calendar_id: existing_calendar.id, invitation_id: first_invitation.id }

    before(:each) do
      existing_organization.calendars << existing_calendar
      existing_calendar.items << [existing_item]
      existing_calendar.granular_permissions << permission
    end

    it 'returns a 200 if the item is valid and user is an admin', reqres_title: 'Update an existing item' do
      first_invitation.update_attribute(:is_admin, true)
      patch_as_user path, params: {active: true}
      expect(response.status).to eq(200)
      expect(response.headers['Content-Type']).to match('json')
      expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
      expect(response.headers['Access-Control-Request-Method']).to eq('*')
      expect(Item.find(existing_item.id).active).to eq(true)
    end

    it 'returns a 200 if the item is valid and user is an admin and the active status is reverted', reqres_title: 'Update an existing item' do
      first_invitation.update_attribute(:is_admin, true)
      existing_item.update_attributes(active: true)
      patch_as_user path, params: {active: false}
      expect(response.status).to eq(200)
      expect(response.headers['Content-Type']).to match('json')
      expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
      expect(response.headers['Access-Control-Request-Method']).to eq('*')
      expect(Item.find(existing_item.id).active).to eq(false)
    end

    it 'returns a 200 if the item is valid and user has write access', reqres_title: 'Update an existing item' do
      permission.update_attributes(write: true)
      patch_as_user path, params: {active: true}
      expect(response.status).to eq(200)
      expect(response.headers['Content-Type']).to match('json')
      expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
      expect(response.headers['Access-Control-Request-Method']).to eq('*')
      expect(Item.find(existing_item.id).active).to eq(true)
    end

    it 'returns a 401 if the token is expired', :skip_reqres do
      patch_as_user path, params: { name: 'Test' }, expired: true
      expect(response.status).to eq(401)
      expect(response.headers['Content-Type']).to match('json')
      expect(Item.find(existing_item.id).name).not_to eq('Test')
    end

    it 'returns a 400 if the params are incomplete', :skip_reqres do
      first_invitation.update_attribute(:is_admin, false)
      patch_as_user path, params: {}
      expect(response.status).to eq(400)
      expect(response.headers['Content-Type']).to match('json')
      expect(Item.find(existing_item.id).name).not_to eq('Test')
    end

    it 'returns a 404 if the user does not have write access', :skip_reqres do
      patch_as_user path, params: {active: true}
      expect(response.status).to eq(404)
      expect(response.headers['Content-Type']).to match('json')
      expect(Item.find(existing_item.id).name).not_to eq('Test')
    end

    it 'returns a 404 if the item does not exist', :skip_reqres do
      patch_as_user "/api/organizations/#{existing_organization.subdomain}/calendars/#{existing_calendar.id}/items/#{existing_item.id+999}/activate" , params: {active: true}
      expect(response.status).to eq(404)
      expect(response.headers['Content-Type']).to match('json')
    end

    it 'returns a 404 if the user is not invited to the organization', :skip_reqres do
      first_invitation.update_attributes(accepted: false)
      patch_as_user path, params: {active: true}
      expect(response.status).to eq(404)
      expect(response.headers['Content-Type']).to match('json')
    end

  end

  context 'DELETE /api/organizations/:id/calendars/:id/items/:id' do

    let(:existing_calendar) { create :calendar }
    let(:existing_category) { create :category }
    let(:existing_item) { create :item }
    let(:path) { "/api/organizations/#{existing_organization.subdomain}/calendars/#{existing_calendar.id}/items/#{existing_item.id}?category_id=#{existing_category.id}" }

    before(:each) do
      existing_organization.calendars << existing_calendar
      existing_calendar.categories << existing_category
      existing_category.items << existing_item
      first_invitation.update_attribute(:is_admin, true)
    end

    it 'returns a 200 if the item exists', reqres_title: 'Delete an existing calendar' do
      expect(Item.where(id: existing_item.id).count).to eq(1)
      delete_as_user path
      expect(response.status).to eq(200)
      expect(response.headers['Content-Type']).to match('json')
      expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
      expect(response.headers['Access-Control-Request-Method']).to eq('*')
      expect(Item.where(id: existing_item.id).count).to eq(0)
      expect(JSON.parse(response.body)["is_deleted"]).to eq(true)
    end

    context 'with child events and categories' do

      let(:child_event) { create :event }
      let(:child_event_2) { create :event }

      before(:each) do
        existing_item.events << [child_event, child_event_2]
      end

      it 'should mark all child events and categories as deleted' do
        expect(Item.not_deleted.where(id: existing_item.id).count).to eq(1)
        expect(Event.not_deleted.where(id: child_event.id).count).to eq(1)
        expect(Event.not_deleted.where(id: child_event_2.id).count).to eq(1)
        delete_as_user path
        expect(response.status).to eq(200)
        expect(response.headers['Content-Type']).to match('json')
        expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
        expect(response.headers['Access-Control-Request-Method']).to eq('*')
        expect(Item.not_deleted.where(id: existing_item.id).count).to eq(0)
        expect(Event.not_deleted.where(id: child_event.id).count).to eq(0)
        expect(Event.not_deleted.where(id: child_event_2.id).count).to eq(0)
        expect(JSON.parse(response.body)["is_deleted"]).to eq(true)
      end

    end

    it 'returns a 200 if the item exists and the user is a super admin without any connection to the organization' do
      first_invitation.update_attributes(accepted: false)
      user.update_attributes(is_super_admin: true)
      expect(Item.where(id: existing_item.id).count).to eq(1)
      delete_as_user path
      expect(response.status).to eq(200)
      expect(response.headers['Content-Type']).to match('json')
      expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
      expect(response.headers['Access-Control-Request-Method']).to eq('*')
      expect(Item.where(id: existing_item.id).count).to eq(0)
    end

    it 'returns a 401 if the user is not authorized' do
      expect(Item.where(id: existing_item.id).count).to eq(1)
      delete path
      expect(response.status).to eq(401)
      expect(response.headers['Content-Type']).to match('json')
      expect(Item.where(id: existing_item.id).count).to eq(1)
    end

    it 'returns a 401 if the user uses an expired token' do
      expect(Item.where(id: existing_item.id).count).to eq(1)
      delete_as_user path, expired: true
      expect(response.status).to eq(401)
      expect(response.headers['Content-Type']).to match('json')
      expect(Item.where(id: existing_item.id).count).to eq(1)
    end

    it 'returns a 404 if the item exists and the user is not an admin' do
      first_invitation.destroy
      expect(Item.where(id: existing_item.id).count).to eq(1)
      delete_as_user path
      expect(response.status).to eq(404)
      expect(response.headers['Content-Type']).to match('json')
      expect(Item.where(id: existing_item.id).count).to eq(1)
    end

    it 'returns a 404 if the item exists and the user is an admin without any connection to the organization' do
      first_invitation.update_attributes(accepted: false)
      expect(Item.where(id: existing_item.id).count).to eq(1)
      delete_as_user path
      expect(response.status).to eq(404)
      expect(response.headers['Content-Type']).to match('json')
      expect(Item.where(id: existing_item.id).count).to eq(1)
    end

    it 'returns a 404 if the item does not exist' do
      existing_item.destroy
      delete_as_user path
      expect(response.status).to eq(404)
      expect(response.headers['Content-Type']).to match('json')
    end


  end
end
