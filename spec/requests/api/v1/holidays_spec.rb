require 'rails_helper'

describe "API V1 Holidays", type: :request, reqres_section: 'Holidays' do

  let(:accept_header) { 'application/vnd.film_cal-v1+json' }
  let(:user) { create :user, email: 'user@example.com', password: '12345678' }
  let(:existing_organization) { create :organization }
  let(:irrelevant_organization) { create :organization }
  let(:first_invitation) { create :invitation,
    user_id: user.id,
    organization_id: existing_organization.id,
    accepted: true }
  let(:second_invitation) { create :invitation,
    user_id: user.id,
    organization_id: irrelevant_organization.id,
    accepted: true }
  let(:sample_params) {{
    name: 'Test',
    starts_at: 2.days.from_now,
    length: 2
  }}

  before(:example) do
    [existing_organization, irrelevant_organization, first_invitation, second_invitation].each { |x| x.touch }
  end

  context 'GET /api/organizations/:id/calendars/:id/holidays' do

    let(:first_calendar) { create :calendar, organization_id: existing_organization.id }
    let(:second_calendar) { create :calendar, organization_id: existing_organization.id }
    let(:first_holiday) { create :holiday, calendar_id: first_calendar.id }
    let(:second_holiday) { create :holiday, calendar_id: first_calendar.id }
    let(:third_holiday) { create :holiday, calendar_id: second_calendar.id }
    let(:path) { "/api/organizations/#{existing_organization.subdomain}/calendars/#{first_calendar.id}/holidays" }

    before(:each) do
      existing_organization.calendars << [first_calendar, second_calendar]
      irrelevant_organization.calendars << second_calendar
      first_calendar.holidays << [first_holiday, second_holiday]
      second_calendar.holidays << [third_holiday]
    end

    it 'returns a 200 with the existing holidays for the requested calendar if the user is an admin', :skip_reqres do
      first_invitation.update_attributes(is_admin: true)
      get_as_user path
      expect(response.status).to eq(200)
      body = JSON.parse(response.body)
      expect(body.length).to eq 2
      result_ids = [body[0]["id"], body[1]["id"]]
      expect(result_ids.include?(first_holiday.id)).to eq true
      expect(result_ids.include?(second_holiday.id)).to eq true
      expect(response.headers['Content-Type']).to match('json')
      expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
      expect(response.headers['Access-Control-Request-Method']).to eq('*')
    end

    it 'returns a 200 with the existing holidays for the requested calendar if the user is a super admin', :skip_reqres do
      user.update_attributes(is_super_admin: true)
      get_as_user path
      expect(response.status).to eq(200)
      body = JSON.parse(response.body)
      expect(body.length).to eq 2
      result_ids = [body[0]["id"], body[1]["id"]]
      expect(result_ids.include?(first_holiday.id)).to eq true
      expect(result_ids.include?(second_holiday.id)).to eq true
      expect(response.headers['Content-Type']).to match('json')
      expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
      expect(response.headers['Access-Control-Request-Method']).to eq('*')
    end

    it 'returns a 404 if the user does not have permission', :skip_reqres do
      get_as_user path
      expect(response.status).to eq(404)
      expect(response.headers['Content-Type']).to match('json')
      expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
      expect(response.headers['Access-Control-Request-Method']).to eq('*')
    end

    it 'returns a 401 not authorized error to an anonymous user', :skip_reqres do
      get path
      expect(response.response_code).to eq(401)
      expect(response.body).to match('OAuth')
      expect(response.headers['Content-Type']).to match('json')
      expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
      expect(response.headers['Access-Control-Request-Method']).to eq('*')
    end

    it 'returns a 401 if the current token has expired', :skip_reqres do
      get_as_user path, expired: true
      expect(response.status).to eq(401)
      expect(response.headers['Content-Type']).to match('json')
      expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
      expect(response.headers['Access-Control-Request-Method']).to eq('*')
    end

    context 'when the user is not invited' do

      before(:each) do
        first_invitation.update_attributes(accepted: false)
      end

      it 'returns a 200 with the existing calendar for the requested ID if the user is a super admin', :skip_reqres do
        user.update_attributes(is_super_admin: true)
        get_as_user path
        expect(response.status).to eq(200)
        body = JSON.parse(response.body)
        expect(body.length).to eq 2
        result_ids = [body[0]["id"], body[1]["id"]]
        expect(result_ids.include?(first_holiday.id)).to eq true
        expect(result_ids.include?(second_holiday.id)).to eq true
        expect(response.headers['Content-Type']).to match('json')
        expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
        expect(response.headers['Access-Control-Request-Method']).to eq('*')
      end

      it 'returns a 404 if the user is not an admin', :skip_reqres do
        get_as_user path
        expect(response.status).to eq(404)
        expect(response.headers['Content-Type']).to match('json')
        expect(response.headers['Content-Type']).to match('json')
        expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
        expect(response.headers['Access-Control-Request-Method']).to eq('*')
      end

    end

    context 'with permissions present' do
      let(:read_permission) { create :granular_permission, read: true, write: false, invitation_id: first_invitation.id, calendar_id: first_calendar.id }

      before(:each) do
        first_invitation.granular_permissions << [read_permission]
      end

      it 'returns the existing calendar for the requested ID if the user has read permissions', :skip_reqres do
        get_as_user path
        expect(response.status).to eq(200)
        body = JSON.parse(response.body)
        expect(body.length).to eq 2
        result_ids = [body[0]["id"], body[1]["id"]]
        expect(result_ids.include?(first_holiday.id)).to eq true
        expect(result_ids.include?(second_holiday.id)).to eq true
        expect(response.headers['Content-Type']).to match('json')
        expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
        expect(response.headers['Access-Control-Request-Method']).to eq('*')
      end

      it 'returns a 404 if the user does not have read permission', :skip_reqres do
        read_permission.update_attributes(read: false)
        get_as_user path
        expect(response.status).to eq(404)
        expect(response.headers['Content-Type']).to match('json')
        expect(response.headers['Content-Type']).to match('json')
        expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
        expect(response.headers['Access-Control-Request-Method']).to eq('*')
      end

    end

  end

  context 'GET /api/organizations/:id/calendars/:id/holidays/:id' do

    let(:first_calendar) { create :calendar, organization_id: existing_organization.id }
    let(:second_calendar) { create :calendar, organization_id: existing_organization.id }
    let(:first_holiday) { create :holiday, calendar_id: first_calendar.id }
    let(:second_holiday) { create :holiday, calendar_id: first_calendar.id }
    let(:third_holiday) { create :holiday, calendar_id: second_calendar.id }
    let(:path) { "/api/organizations/#{existing_organization.subdomain}/calendars/#{first_calendar.id}/holidays/#{first_holiday.id}" }

    before(:each) do
      existing_organization.calendars << [first_calendar, second_calendar]
      irrelevant_organization.calendars << second_calendar
      first_calendar.holidays << [first_holiday, second_holiday]
      second_calendar.holidays << [third_holiday]
    end

    it 'returns a 200 with the existing holiday for the requested ID if the user is an admin', :skip_reqres do
      first_invitation.update_attributes(is_admin: true)
      get_as_user path
      expect(response.status).to eq(200)
      expect(JSON.parse(response.body)["id"]).to eq first_holiday.id
      expect(response.headers['Content-Type']).to match('json')
      expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
      expect(response.headers['Access-Control-Request-Method']).to eq('*')
    end

    it 'returns a 200 with the existing holiday for the requested ID if the user is a super admin', :skip_reqres do
      user.update_attributes(is_super_admin: true)
      get_as_user path
      expect(response.status).to eq(200)
      expect(JSON.parse(response.body)["id"]).to eq first_holiday.id
      expect(response.headers['Content-Type']).to match('json')
      expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
      expect(response.headers['Access-Control-Request-Method']).to eq('*')
    end

    it 'returns a 404 if the user does not have permission', :skip_reqres do
      get_as_user path
      expect(response.status).to eq(404)
      expect(response.headers['Content-Type']).to match('json')
      expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
      expect(response.headers['Access-Control-Request-Method']).to eq('*')
    end

    it 'returns a 401 not authorized error to an anonymous user', :skip_reqres do
      get path
      expect(response.response_code).to eq(401)
      expect(response.body).to match('OAuth')
      expect(response.headers['Content-Type']).to match('json')
      expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
      expect(response.headers['Access-Control-Request-Method']).to eq('*')
    end

    it 'returns a 401 if the current token has expired', :skip_reqres do
      get_as_user path, expired: true
      expect(response.status).to eq(401)
      expect(response.headers['Content-Type']).to match('json')
      expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
      expect(response.headers['Access-Control-Request-Method']).to eq('*')
    end

    context 'when the user is not invited' do

      before(:each) do
        first_invitation.update_attributes(accepted: false)
      end

      it 'returns a 200 with the existing holiday for the requested ID if the user is a super admin', :skip_reqres do
        user.update_attributes(is_super_admin: true)
        get_as_user path
        expect(response.status).to eq(200)
        expect(JSON.parse(response.body)["id"]).to eq first_holiday.id
        expect(response.headers['Content-Type']).to match('json')
        expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
        expect(response.headers['Access-Control-Request-Method']).to eq('*')
      end

      it 'returns a 404 if the user is an admin but has not accepted their invitation', :skip_reqres do
        first_invitation.update_attributes(is_admin: true)
        get_as_user path
        expect(response.status).to eq(404)
        expect(response.headers['Content-Type']).to match('json')
        expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
        expect(response.headers['Access-Control-Request-Method']).to eq('*')
      end

      it 'returns a 404 if the user is not an admin', :skip_reqres do
        get_as_user path
        expect(response.status).to eq(404)
        expect(response.headers['Content-Type']).to match('json')
        expect(response.headers['Content-Type']).to match('json')
        expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
        expect(response.headers['Access-Control-Request-Method']).to eq('*')
      end

    end

    context 'with permissions present' do
      let(:read_permission) { create :granular_permission, read: true, write: false, invitation_id: first_invitation.id, calendar_id: first_calendar.id }

      before(:each) do
        first_invitation.granular_permissions << [read_permission]
      end

      it 'returns the existing calendar for the requested ID if the user has read permissions', :skip_reqres do
        get_as_user path
        expect(response.status).to eq(200)
        expect(JSON.parse(response.body)["id"]).to eq first_holiday.id
        expect(response.headers['Content-Type']).to match('json')
        expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
        expect(response.headers['Access-Control-Request-Method']).to eq('*')
      end

      it 'returns a 404 if the user does not have read permission', :skip_reqres do
        read_permission.update_attributes(read: false)
        get_as_user path
        expect(response.status).to eq(404)
        expect(response.headers['Content-Type']).to match('json')
        expect(response.headers['Content-Type']).to match('json')
        expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
        expect(response.headers['Access-Control-Request-Method']).to eq('*')
      end

    end

  end

  context 'POST /api/organizations/:id/calendars/:id/holidays' do

    let(:first_calendar) { create :calendar, organization_id: existing_organization.id }
    let(:path) { "/api/organizations/#{existing_organization.subdomain}/calendars/#{first_calendar.id}/holidays" }

    before(:each) do
      existing_organization.calendars << first_calendar
    end

    context 'if the user is an admin' do

      before(:each) do
        first_invitation.update_attribute(:is_admin, true)
      end

      it 'returns a 201 if the holiday is valid', reqres_title: 'Create a new holiday' do
        post_as_user path, params: sample_params
        expect(response.status).to eq(201)
        expect(response.headers['Content-Type']).to match('json')
        expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
        expect(response.headers['Access-Control-Request-Method']).to eq('*')
        expect(Holiday.count).to eq(1)
      end

      it 'returns a 401 if the token is expired', :skip_reqres do
        post_as_user path, params: sample_params, expired: true
        expect(response.status).to eq(401)
        expect(response.headers['Content-Type']).to match('json')
        expect(Holiday.count).to eq(0)
      end

      it 'returns a 404 if the user is not an admin', :skip_reqres do
        first_invitation.update_attribute(:is_admin, false)
        post_as_user path, params: sample_params
        expect(response.status).to eq(404)
        expect(response.headers['Content-Type']).to match('json')
        expect(Holiday.count).to eq(0)
      end

      it 'returns a 404 if the user is not invited to the organization', :skip_reqres do
        first_invitation.update_attributes(accepted: false)
        post_as_user path, params: sample_params
        expect(response.status).to eq(404)
        expect(response.headers['Content-Type']).to match('json')
        expect(Holiday.count).to eq(0)
      end

      it 'returns a 422 if the holiday is invalid', :skip_reqres do
        post_as_user path, params: {name: '', length: 0, starts_at: 0.days.from_now}
        expect(response.status).to eq(422)
        expect(response.headers['Content-Type']).to match('json')
        expect(Holiday.count).to eq(0)
      end

    end

    context 'if the user only has read access' do
      let(:permission) { create :granular_permission, calendar_id: first_calendar.id, read: true }

      before(:each) do
        first_invitation.update_attribute(:is_admin, false)
        first_invitation.granular_permissions << permission
      end

      it 'returns a 401 if the token is expired', :skip_reqres do
        post_as_user path, params: sample_params, expired: true
        expect(response.status).to eq(401)
        expect(response.headers['Content-Type']).to match('json')
        expect(Holiday.count).to eq(0)
      end

      it 'returns a 404 if the user is not invited to the organization', :skip_reqres do
        first_invitation.update_attributes(accepted: false)
        post_as_user path, params: sample_params
        expect(response.status).to eq(404)
        expect(response.headers['Content-Type']).to match('json')
        expect(Holiday.count).to eq(0)
      end

    end

    context 'if the user only has write access' do
      let(:permission) { create :granular_permission, calendar_id: first_calendar.id, read: true, write: true }

      before(:each) do
        first_invitation.update_attribute(:is_admin, false)
        first_invitation.granular_permissions << permission
      end

      it 'returns a 401 if the token is expired', :skip_reqres do
        post_as_user path, params: sample_params, expired: true
        expect(response.status).to eq(401)
        expect(response.headers['Content-Type']).to match('json')
        expect(Holiday.count).to eq(0)
      end

      it 'returns a 404 if the user is not invited to the organization', :skip_reqres do
        first_invitation.update_attributes(accepted: false)
        post_as_user path, params: sample_params
        expect(response.status).to eq(404)
        expect(response.headers['Content-Type']).to match('json')
        expect(Holiday.count).to eq(0)
      end

      it 'returns a 201 if the holiday is valid', :skip_reqres do
        post_as_user path, params: sample_params
        expect(response.status).to eq(201)
        expect(response.headers['Content-Type']).to match('json')
        expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
        expect(response.headers['Access-Control-Request-Method']).to eq('*')
        expect(Holiday.count).to eq(1)
      end

      it 'returns a 422 if the holiday is invalid', :skip_reqres do
        post_as_user path, params: {name: '', length: 0, starts_at: 0.days.from_now}
        expect(response.status).to eq(422)
        expect(response.headers['Content-Type']).to match('json')
        expect(Holiday.count).to eq(0)
      end

    end

  end

  context 'PUT /api/organizations/:id/calendars/:id/holidays/:id' do

    let(:existing_calendar) { create :calendar }
    let(:existing_holiday) { create :holiday }
    let(:path) { "/api/organizations/#{existing_organization.subdomain}/calendars/#{existing_calendar.id}/holidays/#{existing_holiday.id}" }
    let(:permission) { create :granular_permission, calendar_id: existing_calendar.id, invitation_id: first_invitation.id }

    before(:each) do
      existing_organization.calendars << existing_calendar
      existing_calendar.holidays << [existing_holiday]
      existing_calendar.granular_permissions << permission
    end

    it 'returns a 200 if the holiday is valid and user is an admin', reqres_title: 'Update an existing holiday' do
      first_invitation.update_attribute(:is_admin, true)
      put_as_user path, params: sample_params
      expect(response.status).to eq(200)
      expect(response.headers['Content-Type']).to match('json')
      expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
      expect(response.headers['Access-Control-Request-Method']).to eq('*')
      expect(Holiday.find(existing_holiday.id).name).to eq('Test')
    end

    it 'returns a 200 if the holiday is valid and user has write access', :skip_reqres do
      permission.update_attributes(write: true)
      put_as_user path, params: sample_params
      expect(response.status).to eq(200)
      expect(response.headers['Content-Type']).to match('json')
      expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
      expect(response.headers['Access-Control-Request-Method']).to eq('*')
      expect(Holiday.find(existing_holiday.id).name).to eq('Test')
    end

    it 'returns a 401 if the token is expired', :skip_reqres do
      put_as_user path, params: { name: 'Test' }, expired: true
      expect(response.status).to eq(401)
      expect(response.headers['Content-Type']).to match('json')
      expect(Holiday.find(existing_holiday.id).name).not_to eq('Test')
    end

    it 'returns a 400 if the params are incomplete', :skip_reqres do
      first_invitation.update_attribute(:is_admin, false)
      put_as_user path, params: {}
      expect(response.status).to eq(400)
      expect(response.headers['Content-Type']).to match('json')
      expect(Holiday.find(existing_holiday.id).name).not_to eq('Test')
    end

    it 'returns a 404 if the user does not have write access', :skip_reqres do
      put_as_user path, params: sample_params
      expect(response.status).to eq(404)
      expect(response.headers['Content-Type']).to match('json')
      expect(Holiday.find(existing_holiday.id).name).not_to eq('Test')
    end

    it 'returns a 404 if the holiday does not exist', :skip_reqres do
      put_as_user "/api/organizations/#{existing_organization.subdomain}/calendars/#{existing_calendar.id}/holidays/#{existing_holiday.id+999}" , params: sample_params
      expect(response.status).to eq(404)
      expect(response.headers['Content-Type']).to match('json')
    end

    it 'returns a 404 if the user is not invited to the organization', :skip_reqres do
      first_invitation.update_attributes(accepted: false)
      put_as_user path, params: sample_params
      expect(response.status).to eq(404)
      expect(response.headers['Content-Type']).to match('json')
    end

  end

  context 'DELETE /api/organizations/:id/calendars/:id/holidays/:id' do

    let(:existing_calendar) { create :calendar }
    let(:existing_holiday) { create :holiday }
    let(:path) { "/api/organizations/#{existing_organization.subdomain}/calendars/#{existing_calendar.id}/holidays/#{existing_holiday.id}" }

    before(:each) do
      existing_organization.calendars << existing_calendar
      existing_calendar.holidays << existing_holiday
      first_invitation.update_attribute(:is_admin, true)
    end

    it 'returns a 200 if the holiday exists', reqres_title: 'Delete an existing calendar' do
      expect(Holiday.where(id: existing_holiday.id).count).to eq(1)
      delete_as_user path
      expect(response.status).to eq(200)
      expect(response.headers['Content-Type']).to match('json')
      expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
      expect(response.headers['Access-Control-Request-Method']).to eq('*')
      expect(Holiday.where(id: existing_holiday.id).count).to eq(0)
      expect(JSON.parse(response.body)["is_deleted"]).to eq(true)
    end

    it 'returns a 200 if the holiday exists and the user is a super admin without any connection to the organization' do
      first_invitation.update_attributes(accepted: false)
      user.update_attributes(is_super_admin: true)
      expect(Holiday.where(id: existing_holiday.id).count).to eq(1)
      delete_as_user path
      expect(response.status).to eq(200)
      expect(response.headers['Content-Type']).to match('json')
      expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
      expect(response.headers['Access-Control-Request-Method']).to eq('*')
      expect(Holiday.where(id: existing_holiday.id).count).to eq(0)
    end

    it 'returns a 401 if the user is not authorized' do
      expect(Holiday.where(id: existing_holiday.id).count).to eq(1)
      delete path
      expect(response.status).to eq(401)
      expect(response.headers['Content-Type']).to match('json')
      expect(Holiday.where(id: existing_holiday.id).count).to eq(1)
    end

    it 'returns a 401 if the user uses an expired token' do
      expect(Holiday.where(id: existing_holiday.id).count).to eq(1)
      delete_as_user path, expired: true
      expect(response.status).to eq(401)
      expect(response.headers['Content-Type']).to match('json')
      expect(Holiday.where(id: existing_holiday.id).count).to eq(1)
    end

    it 'returns a 404 if the holiday exists and the user is not an admin' do
      first_invitation.destroy
      expect(Holiday.where(id: existing_holiday.id).count).to eq(1)
      delete_as_user path
      expect(response.status).to eq(404)
      expect(response.headers['Content-Type']).to match('json')
      expect(Holiday.where(id: existing_holiday.id).count).to eq(1)
    end

    it 'returns a 404 if the holiday exists and the user is an admin without any connection to the organization' do
      first_invitation.update_attributes(accepted: false)
      expect(Holiday.where(id: existing_holiday.id).count).to eq(1)
      delete_as_user path
      expect(response.status).to eq(404)
      expect(response.headers['Content-Type']).to match('json')
      expect(Holiday.where(id: existing_holiday.id).count).to eq(1)
    end

    it 'returns a 404 if the holiday does not exist' do
      existing_holiday.destroy
      delete_as_user path
      expect(response.status).to eq(404)
      expect(response.headers['Content-Type']).to match('json')
    end


  end
end
