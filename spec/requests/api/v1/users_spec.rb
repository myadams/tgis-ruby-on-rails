require 'rails_helper'

describe "API V1 Users", type: :request, reqres_section: 'Users' do

  let(:accept_header) { 'application/vnd.film_cal-v1+json' }
  let(:user) { create :user, email: 'user@example.com', password: '12345678' }

  context 'POST /api/users/forgot_password' do

    let(:message_delivery) { instance_double(ActionMailer::MessageDelivery) }
    let(:path) { "/api/users/forgot_password" }

    before(:each) do
      user.touch
    end

    it 'returns a 201 if the email is valid', reqres_title: 'Generate a new reset password email.' do
      expect(NotificationsMailer).to receive(:forgot_password).and_return(message_delivery)
      expect(message_delivery).to receive(:deliver_now)
      post_as_user path, params: { email: user.email }
      expect(response.status).to eq(201)
      expect(response.headers['Content-Type']).to match('json')
      expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
      expect(response.headers['Access-Control-Request-Method']).to eq('*')
    end

    it 'returns a 404 if the email is invalid', :skip_reqres do
      expect(NotificationsMailer).not_to receive(:forgot_password)
      post_as_user path, params: { email: 'unknown@example.com' }
      expect(response.status).to eq(404)
      expect(response.headers['Content-Type']).to match('json')
      expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
      expect(response.headers['Access-Control-Request-Method']).to eq('*')
    end

  end

  context 'POST /api/users/reset_password' do

    let(:message_delivery) { instance_double(ActionMailer::MessageDelivery) }
    let(:path) { "/api/users/reset_password" }
    let(:new_password) { "anewpassword" }
    before(:each) do
      user.touch
    end

    it 'returns a 201 if the token is valid', reqres_title: 'Reset the users password.' do
      token = user.generate_token
      expect(NotificationsMailer).to receive(:confirm_password_reset).and_return(message_delivery)
      expect(message_delivery).to receive(:deliver_now)
      patch_as_user path, params: { password: new_password, token: token }
      expect(response.status).to eq(200)
      expect(response.headers['Content-Type']).to match('json')
      expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
      expect(response.headers['Access-Control-Request-Method']).to eq('*')
    end

    it 'returns a 403 if the token is invalid', :skip_reqres do
      token = user.generate_token(exp: 10.minutes.ago.to_i)
      expect(NotificationsMailer).not_to receive(:forgot_password)
      patch_as_user path, params: { password: new_password, token: token }
      expect(response.status).to eq(403)
      expect(response.headers['Content-Type']).to match('json')
      expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
      expect(response.headers['Access-Control-Request-Method']).to eq('*')
    end

  end

end
