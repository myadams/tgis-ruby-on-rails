require 'rails_helper'

describe "API V1 Events", type: :request, reqres_section: 'Events' do

  let(:accept_header) { 'application/vnd.film_cal-v1+json' }
  let(:user) { create :user, email: 'user@example.com', password: '12345678' }
  let(:existing_organization) { create :organization }
  let(:irrelevant_organization) { create :organization }
  let(:first_invitation) { create :invitation,
    user_id: user.id,
    organization_id: existing_organization.id,
    accepted: true }
  let(:second_invitation) { create :invitation,
    user_id: user.id,
    organization_id: irrelevant_organization.id,
    accepted: true }
  let(:sample_item) { create :item }
  let(:sample_params) {{
    starts_at: 3.days.ago,
    length: 5,
    position: 0,
    item_id: sample_item.id,
    partition: 0,
    use_custom_styles: false
  }}
  let(:style_sample_params) {{
    use_custom_styles: true,
    name: 'customized',
    style: {
      fill_color: '#fff',
      outline_color: '#000',
      text_color: '#000',
      text_decoration: 'none',
      text_alignment: 'left',
      font: 'Default',
      font_weight: 'normal',
      font_size: 12,
      font_style: 'italic'
    }
  }}
  let(:invalid_style_sample_params) {{
    use_custom_styles: true,
    name: 'customized',
    style: {
      fill_color: '#fff',
      outline_color: '#000',
      text_color: '#000',
      text_decoration: 'none',
      text_alignment: 'NONE',
      font: 'Sonoma Light TTF',
      font_weight: 'light',
      font_size: 28,
      font_style: 'italic'
    }
  }}

  before(:example) do
    [existing_organization, irrelevant_organization, first_invitation, second_invitation].each { |x| x.touch }
  end

  context 'GET /api/organizations/:id/calendars/:id/events' do

    let(:first_calendar) { create :calendar, organization_id: existing_organization.id }
    let(:second_calendar) { create :calendar, organization_id: existing_organization.id }
    let(:first_event) { create :event, calendar_id: first_calendar.id }
    let(:second_event) { create :event, calendar_id: first_calendar.id }
    let(:third_event) { create :event, calendar_id: second_calendar.id }
    let(:path) { "/api/organizations/#{existing_organization.subdomain}/calendars/#{first_calendar.id}/events" }

    before(:each) do
      existing_organization.calendars << [first_calendar, second_calendar]
      irrelevant_organization.calendars << second_calendar
      first_calendar.events << [first_event, second_event]
      second_calendar.events << [third_event]
    end

    it 'returns a 200 with the existing events for the requested calendar if the user is an admin', :skip_reqres do
      first_invitation.update_attributes(is_admin: true)
      get_as_user path
      expect(response.status).to eq(200)
      body = JSON.parse(response.body)
      expect(body.length).to eq 2
      result_ids = [body[0]["id"], body[1]["id"]]
      expect(result_ids.include?(first_event.id)).to eq true
      expect(result_ids.include?(second_event.id)).to eq true
      expect(response.headers['Content-Type']).to match('json')
      expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
      expect(response.headers['Access-Control-Request-Method']).to eq('*')
    end

    it 'returns a 200 with the existing events for the requested calendar if the user is a super admin', :skip_reqres do
      user.update_attributes(is_super_admin: true)
      get_as_user path
      expect(response.status).to eq(200)
      body = JSON.parse(response.body)
      expect(body.length).to eq 2
      result_ids = [body[0]["id"], body[1]["id"]]
      expect(result_ids.include?(first_event.id)).to eq true
      expect(result_ids.include?(second_event.id)).to eq true
      expect(response.headers['Content-Type']).to match('json')
      expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
      expect(response.headers['Access-Control-Request-Method']).to eq('*')
    end

    it 'returns a 404 if the user does not have permission', :skip_reqres do
      get_as_user path
      expect(response.status).to eq(404)
      expect(response.headers['Content-Type']).to match('json')
      expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
      expect(response.headers['Access-Control-Request-Method']).to eq('*')
    end

    it 'returns a 401 not authorized error to an anonymous user', :skip_reqres do
      get path
      expect(response.response_code).to eq(401)
      expect(response.body).to match('OAuth')
      expect(response.headers['Content-Type']).to match('json')
      expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
      expect(response.headers['Access-Control-Request-Method']).to eq('*')
    end

    it 'returns a 401 if the current token has expired', :skip_reqres do
      get_as_user path, expired: true
      expect(response.status).to eq(401)
      expect(response.headers['Content-Type']).to match('json')
      expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
      expect(response.headers['Access-Control-Request-Method']).to eq('*')
    end

    context 'when the user is not invited' do

      before(:each) do
        first_invitation.update_attributes(accepted: false)
      end

      it 'returns a 200 with the existing calendar for the requested ID if the user is a super admin', :skip_reqres do
        user.update_attributes(is_super_admin: true)
        get_as_user path
        expect(response.status).to eq(200)
        body = JSON.parse(response.body)
        expect(body.length).to eq 2
        result_ids = [body[0]["id"], body[1]["id"]]
        expect(result_ids.include?(first_event.id)).to eq true
        expect(result_ids.include?(second_event.id)).to eq true
        expect(response.headers['Content-Type']).to match('json')
        expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
        expect(response.headers['Access-Control-Request-Method']).to eq('*')
      end

      it 'returns a 404 if the user is not an admin', :skip_reqres do
        get_as_user path
        expect(response.status).to eq(404)
        expect(response.headers['Content-Type']).to match('json')
        expect(response.headers['Content-Type']).to match('json')
        expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
        expect(response.headers['Access-Control-Request-Method']).to eq('*')
      end

    end

    context 'with permissions present' do
      let(:read_permission) { create :granular_permission, read: true, write: false, invitation_id: first_invitation.id, calendar_id: first_calendar.id }

      before(:each) do
        first_invitation.granular_permissions << [read_permission]
      end

      it 'returns the existing calendar for the requested ID if the user has read permissions', :skip_reqres do
        get_as_user path
        expect(response.status).to eq(200)
        body = JSON.parse(response.body)
        expect(body.length).to eq 2
        result_ids = [body[0]["id"], body[1]["id"]]
        expect(result_ids.include?(first_event.id)).to eq true
        expect(result_ids.include?(second_event.id)).to eq true
        expect(response.headers['Content-Type']).to match('json')
        expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
        expect(response.headers['Access-Control-Request-Method']).to eq('*')
      end

      it 'returns a 404 if the user does not have read permission', :skip_reqres do
        read_permission.update_attributes(read: false)
        get_as_user path
        expect(response.status).to eq(404)
        expect(response.headers['Content-Type']).to match('json')
        expect(response.headers['Content-Type']).to match('json')
        expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
        expect(response.headers['Access-Control-Request-Method']).to eq('*')
      end

    end

  end

  context 'GET /api/organizations/:id/calendars/:id/events/:id' do

    let(:first_calendar) { create :calendar, organization_id: existing_organization.id }
    let(:second_calendar) { create :calendar, organization_id: existing_organization.id }
    let(:first_event) { create :event, calendar_id: first_calendar.id }
    let(:second_event) { create :event, calendar_id: first_calendar.id }
    let(:third_event) { create :event, calendar_id: second_calendar.id }
    let(:path) { "/api/organizations/#{existing_organization.subdomain}/calendars/#{first_calendar.id}/events/#{first_event.id}" }

    before(:each) do
      existing_organization.calendars << [first_calendar, second_calendar]
      irrelevant_organization.calendars << second_calendar
      first_calendar.events << [first_event, second_event]
      second_calendar.events << [third_event]
    end

    it 'returns a 200 with the existing event for the requested ID if the user is an admin', :skip_reqres do
      first_invitation.update_attributes(is_admin: true)
      get_as_user path
      expect(response.status).to eq(200)
      expect(JSON.parse(response.body)["id"]).to eq first_event.id
      expect(response.headers['Content-Type']).to match('json')
      expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
      expect(response.headers['Access-Control-Request-Method']).to eq('*')
    end

    it 'returns a 200 with the existing event for the requested ID if the user is a super admin', :skip_reqres do
      user.update_attributes(is_super_admin: true)
      get_as_user path
      expect(response.status).to eq(200)
      expect(JSON.parse(response.body)["id"]).to eq first_event.id
      expect(response.headers['Content-Type']).to match('json')
      expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
      expect(response.headers['Access-Control-Request-Method']).to eq('*')
    end

    it 'returns a 404 if the user does not have permission', :skip_reqres do
      get_as_user path
      expect(response.status).to eq(404)
      expect(response.headers['Content-Type']).to match('json')
      expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
      expect(response.headers['Access-Control-Request-Method']).to eq('*')
    end

    it 'returns a 401 not authorized error to an anonymous user', :skip_reqres do
      get path
      expect(response.response_code).to eq(401)
      expect(response.body).to match('OAuth')
      expect(response.headers['Content-Type']).to match('json')
      expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
      expect(response.headers['Access-Control-Request-Method']).to eq('*')
    end

    it 'returns a 401 if the current token has expired', :skip_reqres do
      get_as_user path, expired: true
      expect(response.status).to eq(401)
      expect(response.headers['Content-Type']).to match('json')
      expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
      expect(response.headers['Access-Control-Request-Method']).to eq('*')
    end

    context 'when the user is not invited' do

      before(:each) do
        first_invitation.update_attributes(accepted: false)
      end

      it 'returns a 200 with the existing event for the requested ID if the user is a super admin', :skip_reqres do
        user.update_attributes(is_super_admin: true)
        get_as_user path
        expect(response.status).to eq(200)
        expect(JSON.parse(response.body)["id"]).to eq first_event.id
        expect(response.headers['Content-Type']).to match('json')
        expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
        expect(response.headers['Access-Control-Request-Method']).to eq('*')
      end

      it 'returns a 404 if the user is an admin but has not accepted their invitation', :skip_reqres do
        first_invitation.update_attributes(is_admin: true)
        get_as_user path
        expect(response.status).to eq(404)
        expect(response.headers['Content-Type']).to match('json')
        expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
        expect(response.headers['Access-Control-Request-Method']).to eq('*')
      end

      it 'returns a 404 if the user is not an admin', :skip_reqres do
        get_as_user path
        expect(response.status).to eq(404)
        expect(response.headers['Content-Type']).to match('json')
        expect(response.headers['Content-Type']).to match('json')
        expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
        expect(response.headers['Access-Control-Request-Method']).to eq('*')
      end

    end

    context 'with permissions present' do
      let(:read_permission) { create :granular_permission, read: true, write: false, invitation_id: first_invitation.id, calendar_id: first_calendar.id }

      before(:each) do
        first_invitation.granular_permissions << [read_permission]
      end

      it 'returns the existing calendar for the requested ID if the user has read permissions', :skip_reqres do
        get_as_user path
        expect(response.status).to eq(200)
        expect(JSON.parse(response.body)["id"]).to eq first_event.id
        expect(response.headers['Content-Type']).to match('json')
        expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
        expect(response.headers['Access-Control-Request-Method']).to eq('*')
      end

      it 'returns a 404 if the user does not have read permission', :skip_reqres do
        read_permission.update_attributes(read: false)
        get_as_user path
        expect(response.status).to eq(404)
        expect(response.headers['Content-Type']).to match('json')
        expect(response.headers['Content-Type']).to match('json')
        expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
        expect(response.headers['Access-Control-Request-Method']).to eq('*')
      end

    end

  end

  context 'POST /api/organizations/:id/calendars/:id/events' do

    let(:first_calendar) { create :calendar, organization_id: existing_organization.id }
    let(:path) { "/api/organizations/#{existing_organization.subdomain}/calendars/#{first_calendar.id}/events" }

    before(:each) do
      existing_organization.calendars << first_calendar
    end

    context 'if the user is an admin' do

      before(:each) do
        first_invitation.update_attribute(:is_admin, true)
      end

      it 'returns a 201 if the event is valid', reqres_title: 'Create a new event' do
        post_as_user path, params: sample_params
        expect(response.status).to eq(201)
        expect(response.headers['Content-Type']).to match('json')
        expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
        expect(response.headers['Access-Control-Request-Method']).to eq('*')
        expect(Event.count).to eq(1)
      end

      it 'returns a 201 if the event is valid and uses custom styles', reqres_title: 'Create a new event' do
        sample_params.merge!(style_sample_params)
        post_as_user path, params: sample_params
        expect(response.status).to eq(201)
        expect(response.headers['Content-Type']).to match('json')
        expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
        expect(response.headers['Access-Control-Request-Method']).to eq('*')
        expect(Event.count).to eq(1)
      end

      it 'returns a 401 if the token is expired', :skip_reqres do
        post_as_user path, params: sample_params, expired: true
        expect(response.status).to eq(401)
        expect(response.headers['Content-Type']).to match('json')
        expect(Event.count).to eq(0)
      end

      it 'returns a 404 if the user is not an admin', :skip_reqres do
        first_invitation.update_attribute(:is_admin, false)
        post_as_user path, params: sample_params
        expect(response.status).to eq(404)
        expect(response.headers['Content-Type']).to match('json')
        expect(Event.count).to eq(0)
      end

      it 'returns a 404 if the user is not invited to the organization', :skip_reqres do
        first_invitation.update_attributes(accepted: false)
        post_as_user path, params: sample_params
        expect(response.status).to eq(404)
        expect(response.headers['Content-Type']).to match('json')
        expect(Event.count).to eq(0)
      end

      it 'returns a 422 if the event is invalid', :skip_reqres do
        sample_params[:length] = 0
        post_as_user path, params: sample_params
        expect(response.status).to eq(422)
        expect(response.headers['Content-Type']).to match('json')
        expect(Event.count).to eq(0)
      end

      it 'returns a 422 if the event has invalid custom styles', :skip_reqres do
        sample_params.merge!(invalid_style_sample_params)
        post_as_user path, params: sample_params
        expect(response.status).to eq(422)
        expect(response.headers['Content-Type']).to match('json')
        expect(Event.count).to eq(0)
      end

      it 'returns a 400 if the event is missing custom styles', :skip_reqres do
        sample_params.merge!({use_custom_styles: true})
        post_as_user path, params: sample_params
        expect(response.status).to eq(400)
        expect(response.headers['Content-Type']).to match('json')
        expect(Event.count).to eq(0)
      end

    end

    context 'if the user only has read access' do
      let(:permission) { create :granular_permission, calendar_id: first_calendar.id, read: true }

      before(:each) do
        first_invitation.update_attribute(:is_admin, false)
        first_invitation.granular_permissions << permission
      end

      it 'returns a 401 if the token is expired', :skip_reqres do
        post_as_user path, params: sample_params, expired: true
        expect(response.status).to eq(401)
        expect(response.headers['Content-Type']).to match('json')
        expect(Event.count).to eq(0)
      end

      it 'returns a 404 if the user is not invited to the organization', :skip_reqres do
        first_invitation.update_attributes(accepted: false)
        post_as_user path, params: sample_params
        expect(response.status).to eq(404)
        expect(response.headers['Content-Type']).to match('json')
        expect(Event.count).to eq(0)
      end

    end

    context 'if the user only has write access' do
      let(:permission) { create :granular_permission, calendar_id: first_calendar.id, read: true, write: true }

      before(:each) do
        first_invitation.update_attribute(:is_admin, false)
        first_invitation.granular_permissions << permission
      end

      it 'returns a 401 if the token is expired', :skip_reqres do
        post_as_user path, params: sample_params, expired: true
        expect(response.status).to eq(401)
        expect(response.headers['Content-Type']).to match('json')
        expect(Event.count).to eq(0)
      end

      it 'returns a 404 if the user is not invited to the organization', :skip_reqres do
        first_invitation.update_attributes(accepted: false)
        post_as_user path, params: sample_params
        expect(response.status).to eq(404)
        expect(response.headers['Content-Type']).to match('json')
        expect(Event.count).to eq(0)
      end

      it 'returns a 201 if the event is valid', reqres_title: 'Create a new event' do
        post_as_user path, params: sample_params
        expect(response.status).to eq(201)
        expect(response.headers['Content-Type']).to match('json')
        expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
        expect(response.headers['Access-Control-Request-Method']).to eq('*')
        expect(Event.count).to eq(1)
      end

      it 'returns a 422 if the event is invalid', :skip_reqres do
        sample_params[:length] = 0
        post_as_user path, params: sample_params
        expect(response.status).to eq(422)
        expect(response.headers['Content-Type']).to match('json')
        expect(Event.count).to eq(0)
      end

    end

  end

  context 'PUT /api/organizations/:id/calendars/:id/events/:id' do

    let(:existing_calendar) { create :calendar }
    let(:existing_event) { create :event }
    let(:path) { "/api/organizations/#{existing_organization.subdomain}/calendars/#{existing_calendar.id}/events/#{existing_event.id}" }
    let(:permission) { create :granular_permission, calendar_id: existing_calendar.id, invitation_id: first_invitation.id }

    before(:each) do
      existing_organization.calendars << existing_calendar
      existing_calendar.events << existing_event
      existing_calendar.granular_permissions << permission
    end

    it 'returns a 200 if the event is valid and user is an admin', reqres_title: 'Update an existing event' do
      first_invitation.update_attribute(:is_admin, true)
      put_as_user path, params: sample_params
      expect(response.status).to eq(200)
      expect(response.headers['Content-Type']).to match('json')
      expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
      expect(response.headers['Access-Control-Request-Method']).to eq('*')
      expect(Event.find(existing_event.id).starts_at.mday).to eq(sample_params[:starts_at].mday)
    end

    it 'returns a 200 if the event is valid and uses custom styles', reqres_title: 'Update an existing event' do
      first_invitation.update_attribute(:is_admin, true)
      sample_params.merge!(style_sample_params)
      put_as_user path, params: sample_params
      expect(response.status).to eq(200)
      expect(response.headers['Content-Type']).to match('json')
      expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
      expect(response.headers['Access-Control-Request-Method']).to eq('*')
      expect(Event.find(existing_event.id).starts_at.mday).to eq(sample_params[:starts_at].mday)
    end

    it 'returns a 200 if the event is valid and user has write access', reqres_title: 'Update an existing event' do
      permission.update_attributes(write: true)
      put_as_user path, params: sample_params
      expect(response.status).to eq(200)
      expect(response.headers['Content-Type']).to match('json')
      expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
      expect(response.headers['Access-Control-Request-Method']).to eq('*')
      expect(Event.find(existing_event.id).starts_at.mday).to eq(sample_params[:starts_at].mday)
    end

    it 'returns a 401 if the token is expired', :skip_reqres do
      put_as_user path, params: sample_params, expired: true
      expect(response.status).to eq(401)
      expect(response.headers['Content-Type']).to match('json')
      expect(Event.find(existing_event.id).starts_at.mday).not_to eq(sample_params[:starts_at].mday)
    end

    it 'returns a 400 if the params are incomplete', :skip_reqres do
      first_invitation.update_attribute(:is_admin, false)
      put_as_user path, params: {name: 'new name'}
      expect(response.status).to eq(400)
      expect(response.headers['Content-Type']).to match('json')
      expect(Event.find(existing_event.id).starts_at.mday).not_to eq(sample_params[:starts_at].mday)
    end

    it 'returns a 404 if the user does not have write access', :skip_reqres do
      put_as_user path, params: sample_params
      expect(response.status).to eq(404)
      expect(response.headers['Content-Type']).to match('json')
      expect(Event.find(existing_event.id).starts_at.mday).not_to eq(sample_params[:starts_at].mday)
    end

    it 'returns a 404 if the event does not exist', :skip_reqres do
      put_as_user "/api/organizations/#{existing_organization.subdomain}/calendars/#{existing_calendar.id}/events/#{existing_event.id+999}" , params: sample_params
      expect(response.status).to eq(404)
      expect(response.headers['Content-Type']).to match('json')
    end

    it 'returns a 404 if the user is not invited to the organization', :skip_reqres do
      first_invitation.update_attributes(accepted: false)
      put_as_user path, params: sample_params
      expect(response.status).to eq(404)
      expect(response.headers['Content-Type']).to match('json')
    end

    it 'returns a 422 if the event is invalid', :skip_reqres do
      permission.update_attributes(write: true)
      sample_params[:length] = 0
      put_as_user path, params: sample_params
      expect(response.status).to eq(422)
      expect(response.headers['Content-Type']).to match('json')
      expect(Event.find(existing_event.id).starts_at.mday).not_to eq(sample_params[:starts_at].mday)
    end

    it 'returns a 400 if the event uses custom styles and has invalid style params', :skip_reqres do
      permission.update_attributes(write: true)
      sample_params.merge!(use_custom_styles: true)
      put_as_user path, params: sample_params
      expect(response.status).to eq(400)
      expect(response.headers['Content-Type']).to match('json')
      expect(Event.find(existing_event.id).starts_at.mday).not_to eq(sample_params[:starts_at].mday)
    end

    it 'returns a 422 if the event uses custom styles and has invalid style params', :skip_reqres do
      permission.update_attributes(write: true)
      sample_params.merge!(invalid_style_sample_params)
      put_as_user path, params: sample_params
      expect(response.status).to eq(422)
      expect(response.headers['Content-Type']).to match('json')
      expect(Event.find(existing_event.id).starts_at.mday).not_to eq(sample_params[:starts_at].mday)
    end

  end


  context 'PATCH /api/organizations/:id/calendars/:id/events/:id/shift' do

    let(:existing_calendar) { create :calendar }
    let(:existing_event) { create :event, position: 1 }
    let(:existing_event_2) { create :event, position: 2 }
    let(:path) { "/api/organizations/#{existing_organization.subdomain}/calendars/#{existing_calendar.id}/events/#{existing_event.id}/shift" }
    let(:permission) { create :granular_permission, calendar_id: existing_calendar.id, invitation_id: first_invitation.id }
    let(:shift_params) {{
      starts_at: 6.days.from_now,
      length: 3,
      position: 2,
      partition: 0
    }}

    before(:each) do
      existing_organization.calendars << existing_calendar
      existing_calendar.events << [existing_event, existing_event_2]
      existing_calendar.granular_permissions << permission
    end

    it 'returns a 200 if the event is valid and user is an admin', reqres_title: 'Update an existing event' do
      first_invitation.update_attribute(:is_admin, true)
      patch_as_user path, params: shift_params
      expect(response.status).to eq(200)
      expect(response.headers['Content-Type']).to match('json')
      expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
      expect(response.headers['Access-Control-Request-Method']).to eq('*')
      expect(Event.find(existing_event.id).position).to eq(shift_params[:position])
      expect(Event.find(existing_event_2.id).position).to eq(1)
    end

    it 'returns a 200 if the event is valid and user has write access', reqres_title: 'Update an existing event' do
      permission.update_attributes(write: true)
      patch_as_user path, params: shift_params
      expect(response.status).to eq(200)
      expect(response.headers['Content-Type']).to match('json')
      expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
      expect(response.headers['Access-Control-Request-Method']).to eq('*')
      expect(Event.find(existing_event.id).position).to eq(shift_params[:position])
      expect(Event.find(existing_event_2.id).position).to eq(1)
    end

    it 'returns a 401 if the token is expired', :skip_reqres do
      patch_as_user path, params: { name: 'Test' }, expired: true
      expect(response.status).to eq(401)
      expect(response.headers['Content-Type']).to match('json')
      expect(Event.find(existing_event.id).name).not_to eq('Test')
    end

    it 'returns a 400 if the params are incomplete', :skip_reqres do
      first_invitation.update_attribute(:is_admin, false)
      patch_as_user path, params: {name: 'new name'}
      expect(response.status).to eq(400)
      expect(response.headers['Content-Type']).to match('json')
      expect(Event.find(existing_event.id).name).not_to eq('Test')
    end

    it 'returns a 404 if the user does not have write access', :skip_reqres do
      patch_as_user path, params: shift_params
      expect(response.status).to eq(404)
      expect(response.headers['Content-Type']).to match('json')
      expect(Event.find(existing_event.id).name).not_to eq('Test')
    end

    it 'returns a 404 if the event does not exist', :skip_reqres do
      patch_as_user "/api/organizations/#{existing_organization.subdomain}/calendars/#{existing_calendar.id}/events/#{existing_event.id+999}/shift" , params: shift_params
      expect(response.status).to eq(404)
      expect(response.headers['Content-Type']).to match('json')
    end

    it 'returns a 404 if the user is not invited to the organization', :skip_reqres do
      first_invitation.update_attributes(accepted: false)
      patch_as_user path, params: shift_params
      expect(response.status).to eq(404)
      expect(response.headers['Content-Type']).to match('json')
    end

    it 'returns a 422 if the event is invalid', :skip_reqres do
      permission.update_attributes(write: true)
      shift_params[:length] = 0
      patch_as_user path, params: shift_params
      expect(response.status).to eq(422)
      expect(response.headers['Content-Type']).to match('json')
      expect(Event.find(existing_event.id).name).not_to eq('Test')
    end

  end


  context 'PATCH /api/organizations/:id/calendars/:id/events/offset' do

    let(:existing_calendar) { create :calendar }
    let(:existing_event) { create :event, position: 1, starts_at: 1.days.from_now, length: 3 }
    let(:existing_event_2) { create :event, position: 2, starts_at: 5.days.from_now, length: 2 }
    let(:path) { "/api/organizations/#{existing_organization.subdomain}/calendars/#{existing_calendar.id}/events/offset" }
    let(:permission) { create :granular_permission, calendar_id: existing_calendar.id, invitation_id: first_invitation.id }
    let(:offset_params) {{
      ids: [existing_event.id, existing_event_2.id],
      offset: 3,
      duplicate: false
    }}

    before(:each) do
      existing_organization.calendars << existing_calendar
      existing_calendar.events << [existing_event, existing_event_2]
      existing_calendar.granular_permissions << permission
    end

    it 'returns a 200 if the events are valid and user is an admin', reqres_title: 'Update the positions of existing events' do
      first_invitation.update_attribute(:is_admin, true)
      patch_as_user path, params: offset_params
      expect(response.status).to eq(200)
      expect(response.headers['Content-Type']).to match('json')
      expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
      expect(response.headers['Access-Control-Request-Method']).to eq('*')
      expect(Event.find(existing_event.id).starts_at.yday()).to eq(4.days.from_now.yday())
      expect(Event.find(existing_event.id).ends_at.yday()).to eq(6.days.from_now.yday())
      expect(Event.find(existing_event_2.id).starts_at.yday()).to eq(8.days.from_now.yday())
      expect(Event.find(existing_event_2.id).ends_at.yday()).to eq(9.days.from_now.yday())
    end

    it 'returns a 200 with duplicated events if the duplicate flag is set to true', reqres_title: 'Update the positions of existing events' do
      first_invitation.update_attribute(:is_admin, true)
      offset_params[:duplicate] = true
      patch_as_user path, params: offset_params
      expect(response.status).to eq(200)
      expect(response.headers['Content-Type']).to match('json')
      expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
      expect(response.headers['Access-Control-Request-Method']).to eq('*')
      ids = JSON.parse(response.body).map{|e| e["id"]}
      expect(ids.include?(existing_event.id)).not_to eq(true)
      expect(ids.include?(existing_event_2.id)).not_to eq(true)
      expect(Event.find(existing_event.id).starts_at.yday()).not_to eq(4.days.from_now.yday())
      expect(Event.find(existing_event.id).ends_at.yday()).not_to eq(6.days.from_now.yday())
      expect(Event.find(existing_event_2.id).starts_at.yday()).not_to eq(8.days.from_now.yday())
      expect(Event.find(existing_event_2.id).ends_at.yday()).not_to eq(9.days.from_now.yday())
    end

    it 'returns a 200 if the event is valid and user has write access' do
      permission.update_attributes(write: true)
      patch_as_user path, params: offset_params
      expect(response.status).to eq(200)
      expect(response.headers['Content-Type']).to match('json')
      expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
      expect(response.headers['Access-Control-Request-Method']).to eq('*')
      expect(Event.find(existing_event.id).starts_at.yday()).to eq(4.days.from_now.yday())
      expect(Event.find(existing_event.id).ends_at.yday()).to eq(6.days.from_now.yday())
      expect(Event.find(existing_event_2.id).starts_at.yday()).to eq(8.days.from_now.yday())
      expect(Event.find(existing_event_2.id).ends_at.yday()).to eq(9.days.from_now.yday())
    end

    it 'returns a 200 if the events are shifted in a negative direction' do
      first_invitation.update_attribute(:is_admin, true)
      offset_params[:offset] = -7
      patch_as_user path, params: offset_params
      expect(response.status).to eq(200)
      expect(response.headers['Content-Type']).to match('json')
      expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
      expect(response.headers['Access-Control-Request-Method']).to eq('*')
      expect(Event.find(existing_event.id).starts_at.yday()).to eq(6.days.ago.yday())
      expect(Event.find(existing_event.id).ends_at.yday()).to eq(4.days.ago.yday())
      expect(Event.find(existing_event_2.id).starts_at.yday()).to eq(2.days.ago.yday())
      expect(Event.find(existing_event_2.id).ends_at.yday()).to eq(1.days.ago.yday())
    end

    it 'returns a 401 if the token is expired', :skip_reqres do
      patch_as_user path, params: { name: 'Test' }, expired: true
      expect(response.status).to eq(401)
      expect(response.headers['Content-Type']).to match('json')
      expect(Event.find(existing_event.id).name).not_to eq('Test')
    end

    it 'returns a 400 if the params are incomplete', :skip_reqres do
      first_invitation.update_attribute(:is_admin, false)
      patch_as_user path, params: {name: 'new name'}
      expect(response.status).to eq(400)
      expect(response.headers['Content-Type']).to match('json')
      expect(Event.find(existing_event.id).name).not_to eq('Test')
    end

    it 'returns a 404 if the user does not have write access', :skip_reqres do
      patch_as_user path, params: offset_params
      expect(response.status).to eq(404)
      expect(response.headers['Content-Type']).to match('json')
      expect(Event.find(existing_event.id).name).not_to eq('Test')
    end

    it 'returns a 404 if the event does not exist', :skip_reqres do
      offset_params[:ids] = [123,999]
      patch_as_user "/api/organizations/#{existing_organization.subdomain}/calendars/#{existing_calendar.id}/events/offset" , params: offset_params
      expect(response.status).to eq(404)
      expect(response.headers['Content-Type']).to match('json')
    end

    it 'returns a 404 if the user is not invited to the organization', :skip_reqres do
      first_invitation.update_attributes(accepted: false)
      patch_as_user path, params: offset_params
      expect(response.status).to eq(404)
      expect(response.headers['Content-Type']).to match('json')
    end

  end


  context 'DELETE /api/organizations/:id/calendars/:id/events/:id' do

    let(:existing_calendar) { create :calendar }
    let(:existing_event) { create :event }
    let(:path) { "/api/organizations/#{existing_organization.subdomain}/calendars/#{existing_calendar.id}/events/#{existing_event.id}" }

    before(:each) do
      existing_organization.calendars << existing_calendar
      existing_calendar.events << existing_event
      first_invitation.update_attribute(:is_admin, true)
    end

    it 'returns a 200 if the event exists', reqres_title: 'Delete an existing calendar' do
      expect(Event.where(id: existing_event.id).count).to eq(1)
      delete_as_user path
      expect(response.status).to eq(200)
      expect(response.headers['Content-Type']).to match('json')
      expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
      expect(response.headers['Access-Control-Request-Method']).to eq('*')
      expect(Event.where(id: existing_event.id).count).to eq(0)
      expect(JSON.parse(response.body)["is_deleted"]).to eq(true)
    end

    it 'returns a 200 if the event exists and the user is a super admin without any connection to the organization' do
      first_invitation.update_attributes(accepted: false)
      user.update_attributes(is_super_admin: true)
      expect(Event.where(id: existing_event.id).count).to eq(1)
      delete_as_user path
      expect(response.status).to eq(200)
      expect(response.headers['Content-Type']).to match('json')
      expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
      expect(response.headers['Access-Control-Request-Method']).to eq('*')
      expect(Event.where(id: existing_event.id).count).to eq(0)
    end

    it 'returns a 401 if the user is not authorized' do
      expect(Event.where(id: existing_event.id).count).to eq(1)
      delete path
      expect(response.status).to eq(401)
      expect(response.headers['Content-Type']).to match('json')
      expect(Event.where(id: existing_event.id).count).to eq(1)
    end

    it 'returns a 401 if the user uses an expired token' do
      expect(Event.where(id: existing_event.id).count).to eq(1)
      delete_as_user path, expired: true
      expect(response.status).to eq(401)
      expect(response.headers['Content-Type']).to match('json')
      expect(Event.where(id: existing_event.id).count).to eq(1)
    end

    it 'returns a 404 if the event exists and the user is not an admin' do
      first_invitation.destroy
      expect(Event.where(id: existing_event.id).count).to eq(1)
      delete_as_user path
      expect(response.status).to eq(404)
      expect(response.headers['Content-Type']).to match('json')
      expect(Event.where(id: existing_event.id).count).to eq(1)
    end

    it 'returns a 404 if the event exists and the user is an admin without any connection to the organization' do
      first_invitation.update_attributes(accepted: false)
      expect(Event.where(id: existing_event.id).count).to eq(1)
      delete_as_user path
      expect(response.status).to eq(404)
      expect(response.headers['Content-Type']).to match('json')
      expect(Event.where(id: existing_event.id).count).to eq(1)
    end

    it 'returns a 404 if the event does not exist' do
      existing_event.destroy
      delete_as_user path
      expect(response.status).to eq(404)
      expect(response.headers['Content-Type']).to match('json')
    end

  end


  context 'DELETE /api/organizations/:id/calendars/:id/events' do

    let(:existing_calendar) { create :calendar }
    let(:existing_event) { create :event }
    let(:another_existing_event) { create :event }
    let(:yet_another_existing_event) { create :event }
    let(:path) { "/api/organizations/#{existing_organization.subdomain}/calendars/#{existing_calendar.id}/events" }

    before(:each) do
      existing_organization.calendars << existing_calendar
      existing_calendar.events << [existing_event, another_existing_event, yet_another_existing_event]
      first_invitation.update_attribute(:is_admin, true)
    end

    it 'returns a 200 if the events exist', reqres_title: 'Delete a group of existing calendars' do
      expect(Calendar.find(existing_calendar.id).events.not_deleted.count).to eq(3)
      patch_as_user path, params: {ids: existing_calendar.events.map{|e| e.id}}
      expect(response.status).to eq(200)
      expect(response.headers['Content-Type']).to match('json')
      expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
      expect(response.headers['Access-Control-Request-Method']).to eq('*')
      expect(Calendar.find(existing_calendar.id).events.not_deleted.count).to eq(0)
    end

    it 'returns a 200 if the event exists and the user is a super admin without any connection to the organization' do
      first_invitation.update_attributes(accepted: false)
      user.update_attributes(is_super_admin: true)
      expect(Calendar.find(existing_calendar.id).events.not_deleted.count).to eq(3)
      patch_as_user path, params: {ids: existing_calendar.events.map{|e| e.id}}
      expect(response.status).to eq(200)
      expect(response.headers['Content-Type']).to match('json')
      expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
      expect(response.headers['Access-Control-Request-Method']).to eq('*')
      expect(Calendar.find(existing_calendar.id).events.not_deleted.count).to eq(0)
    end

    it 'returns a 401 if the user is not authorized' do
      expect(Calendar.find(existing_calendar.id).events.not_deleted.count).to eq(3)
      patch path, params: {ids: existing_calendar.events.map{|e| e.id}}
      expect(response.status).to eq(401)
      expect(response.headers['Content-Type']).to match('json')
      expect(Calendar.find(existing_calendar.id).events.not_deleted.count).to eq(3)
    end

    it 'returns a 401 if the user uses an expired token' do
      expect(Calendar.find(existing_calendar.id).events.not_deleted.count).to eq(3)
      patch_as_user path, params: {ids: existing_calendar.events.map{|e| e.id}}, expired: true
      expect(response.status).to eq(401)
      expect(response.headers['Content-Type']).to match('json')
      expect(Calendar.find(existing_calendar.id).events.not_deleted.count).to eq(3)
    end

    it 'returns a 404 if the event exists and the user is not an admin' do
      first_invitation.destroy
      expect(Calendar.find(existing_calendar.id).events.not_deleted.count).to eq(3)
      patch_as_user path, params: {ids: existing_calendar.events.map{|e| e.id}}
      expect(response.status).to eq(404)
      expect(response.headers['Content-Type']).to match('json')
      expect(Calendar.find(existing_calendar.id).events.not_deleted.count).to eq(3)
    end

    it 'returns a 404 if the event exists and the user is an admin without any connection to the organization' do
      first_invitation.update_attributes(accepted: false)
      expect(Calendar.find(existing_calendar.id).events.not_deleted.count).to eq(3)
      patch_as_user path, params: {ids: existing_calendar.events.map{|e| e.id}}
      expect(response.status).to eq(404)
      expect(response.headers['Content-Type']).to match('json')
      expect(Calendar.find(existing_calendar.id).events.not_deleted.count).to eq(3)
    end

    it 'returns a 404 if one of the events does not exist' do
      existing_event.destroy
      patch_as_user path, params: {ids: existing_calendar.events.map{|e| e.id}}
      expect(response.status).to eq(404)
      expect(response.headers['Content-Type']).to match('json')
    end

  end


end
