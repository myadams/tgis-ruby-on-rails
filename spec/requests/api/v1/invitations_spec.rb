require 'rails_helper'

describe "API V1 Invitations", type: :request, reqres_section: 'Invitations' do

  let(:accept_header) { 'application/vnd.film_cal-v1+json' }
  let(:user) { create :user, email: 'user@example.com', password: '12345678', is_super_admin: true }
  let(:email) { 'shout@jimjeffers.com' }
  let(:name) { 'Jim Jeffers' }

  context 'POST /api/organizations/:organization_subdomain/invitations' do

    let(:message_delivery) { instance_double(ActionMailer::MessageDelivery) }
    let(:existing_organization) { create :organization }
    let(:invitation) { create :invitation, user_id: user.id, organization_id: existing_organization.id, accepted: true }
    let(:path) { "/api/organizations/#{existing_organization.subdomain}/invitations" }

    before(:each) do
      existing_organization.touch
    end

    it 'returns a 201 if the invitation is valid and the user is a super admin', reqres_title: 'Create a new invitation' do
      expect(NotificationsMailer).to receive(:invitation).and_return(message_delivery)
      expect(message_delivery).to receive(:deliver_now)
      post_as_user path, params: { email: email, name: name }
      expect(response.status).to eq(201)
      expect(response.headers['Content-Type']).to match('json')
      expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
      expect(response.headers['Access-Control-Request-Method']).to eq('*')
      expect(Invitation.count).to eq(1)
    end

    it 'returns a 201 if the invitation is for a super admin and the user is a super admin', :skip_reqres do
      expect(NotificationsMailer).to receive(:invitation).and_return(message_delivery)
      expect(message_delivery).to receive(:deliver_now)
      post_as_user path, params: { email: email, name: name, is_super_admin: true }
      expect(response.status).to eq(201)
      expect(response.headers['Content-Type']).to match('json')
      expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
      expect(response.headers['Access-Control-Request-Method']).to eq('*')
      expect(Invitation.count).to eq(1)
    end

    context 'As an admin' do

      before(:each) do
        user.update_attributes(is_super_admin: false)
        Invitation.create!(
          email: user.email,
          name: "Example User",
          user_id: user.id,
          organization_id: existing_organization.id,
          accepted: true,
          is_admin: true,
          is_super_admin: false
        )
      end

      it 'returns a 201 if the invitation is valid and the user is an admin', :skip_reqres do
        expect(NotificationsMailer).to receive(:invitation).and_return(message_delivery)
        expect(message_delivery).to receive(:deliver_now)
        post_as_user path, params: { email: email, name: name }
        expect(response.status).to eq(201)
        expect(response.headers['Content-Type']).to match('json')
        expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
        expect(response.headers['Access-Control-Request-Method']).to eq('*')
        expect(Invitation.count).to eq(2)
      end

      it 'returns a 403 if the invitation is for a super admin and the user is an admin', :skip_reqres do
        expect(NotificationsMailer).not_to receive(:invitation)
        post_as_user path, params: { email: email, name: name, access_level: 'super' }
        expect(response.status).to eq(403)
        expect(response.headers['Content-Type']).to match('json')
        expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
        expect(response.headers['Access-Control-Request-Method']).to eq('*')
        expect(Invitation.count).to eq(1)
      end
    end

    it 'returns a 422 if an invitation has an invalid email', :skip_reqres do
      expect(NotificationsMailer).not_to receive(:invitation)
      post_as_user path, params: { email: '', name: name  }
      expect(response.status).to eq(422)
      expect(response.headers['Content-Type']).to match('json')
      expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
      expect(response.headers['Access-Control-Request-Method']).to eq('*')
      expect(Invitation.count).to eq(0)
    end

    it 'returns a 422 if an invitation has an invalid name', :skip_reqres do
      expect(NotificationsMailer).not_to receive(:invitation)
      post_as_user path, params: { email: email, name: ''  }
      expect(response.status).to eq(422)
      expect(response.headers['Content-Type']).to match('json')
      expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
      expect(response.headers['Access-Control-Request-Method']).to eq('*')
      expect(Invitation.count).to eq(0)
    end

    context 'when other invitations are present', :skip_reqres do

      let(:additional_organization) { create :organization }
      let(:existing_invitation) { create :invitation, email: email, organization_id: existing_organization.id }

      before :each do
        additional_organization.touch
        existing_invitation.touch
      end

      it 'returns a 201 if an invitation exists for another organization' do
        expect(NotificationsMailer).to receive(:invitation).and_return(message_delivery)
        expect(message_delivery).to receive(:deliver_now)
        post_as_user "/api/organizations/#{additional_organization.subdomain}/invitations", params: { email: email, name: name }
        expect(response.status).to eq(201)
        expect(response.headers['Content-Type']).to match('json')
        expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
        expect(response.headers['Access-Control-Request-Method']).to eq('*')
        expect(additional_organization.invitations.count).to eq(1)
        expect(Invitation.count).to eq(2)
      end

      it 'returns a 422 if an invitation has already been issued for this organization' do
        expect(NotificationsMailer).not_to receive(:invitation).with(pending_invitation)
        post_as_user path, params: { email: email, name: name }
        expect(response.status).to eq(422)
        expect(response.headers['Content-Type']).to match('json')
        expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
        expect(response.headers['Access-Control-Request-Method']).to eq('*')
        expect(Invitation.count).to eq(1)
      end

    end

  end

  context 'DELETE /api/organizations/:organization_subdomain/invitations/:invitation_id' do

    let(:message_delivery) { instance_double(ActionMailer::MessageDelivery) }
    let(:existing_organization) { create :organization }
    let(:accepted_invitation) { create :invitation, accepted: true, organization_id: existing_organization.id }
    let(:path) { "/api/organizations/#{existing_organization.subdomain}/invitations/#{accepted_invitation.id}" }
    let(:invitation) { create :invitation, user_id: user.id, organization_id: existing_organization.id, accepted: true }

    before(:each) do
      existing_organization.touch
      accepted_invitation.touch
    end

    it 'returns a 200 if the invitation was deleted by a super admin', reqres_title: 'Deletes an existing invitation' do
      expect(Invitation.count).to eq(1)
      delete_as_user path
      expect(response.status).to eq(200)
      expect(response.headers['Content-Type']).to match('json')
      expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
      expect(response.headers['Access-Control-Request-Method']).to eq('*')
      expect(JSON.parse(response.body)["is_deleted"]).to eq(true)
      expect(Invitation.count).to eq(0)
    end

    it 'returns a 403 if the invitation was attempted to be deleted by a normal user', :skip_reqres do
      expect(Invitation.count).to eq(1)
      user.update_attributes(is_super_admin: false)
      delete_as_user path
      expect(response.status).to eq(403)
      expect(response.headers['Content-Type']).to match('json')
      expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
      expect(response.headers['Access-Control-Request-Method']).to eq('*')
      expect(Invitation.count).to eq(1)
    end

    context 'As an admin' do

      before(:each) do
        user.update_attributes(is_super_admin: false)
        Invitation.create!(
          email: user.email,
          name: "Example User",
          user_id: user.id,
          organization_id: existing_organization.id,
          accepted: true,
          is_admin: true,
          is_super_admin: false
        )
      end

      it 'returns a 200 if the invitation was deleted by an admin', :skip_reqres do
        expect(Invitation.count).to eq(2)
        delete_as_user path
        expect(response.status).to eq(200)
        expect(response.headers['Content-Type']).to match('json')
        expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
        expect(response.headers['Access-Control-Request-Method']).to eq('*')
        expect(JSON.parse(response.body)["is_deleted"]).to eq(true)
        expect(Invitation.count).to eq(1)
      end

    end

  end

  context 'GET /api/organizations/:organization_subdomain/invitations/validations?name=%s' do

    context 'when results are present', :skip_reqres do
      let(:first_organization) { create :organization }
      let(:accepted_invitation) { create :invitation, accepted: true, organization_id: first_organization.id }

      before(:example) do
        [first_organization].each { |x| x.touch }
      end

      it 'returns a 200 with a valid response if the name is valid amongst existing organizations' do
        user.update_attribute(:is_super_admin, true)
        get_as_user "/api/organizations/#{first_organization.subdomain}/invitations/validations?email=jim@sumocreations.com"
        expect(response.status).to eq(200)
        expect(response.headers['Content-Type']).to match('json')
        expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
        expect(response.headers['Access-Control-Request-Method']).to eq('*')
        expect(JSON.parse(response.body)["valid"]).to eq true
      end

      it 'returns a 200 with an invalid response if the invalid credentials are passed but always_ok is set to true' do
        user.update_attribute(:is_super_admin, true)
        get_as_user "/api/organizations/#{first_organization.subdomain}/invitations/validations?email=#{accepted_invitation.email}&always_ok=true"
        expect(response.status).to eq(200)
        expect(response.headers['Content-Type']).to match('json')
        expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
        expect(response.headers['Access-Control-Request-Method']).to eq('*')
        expect(JSON.parse(response.body)["valid"]).to eq false
      end

      it 'returns a 422 with an invalid response if the email is invalid amongst existing invitations for the organization' do
        user.update_attribute(:is_super_admin, true)
        get_as_user "/api/organizations/#{first_organization.subdomain}/invitations/validations?email=#{accepted_invitation.email}"
        expect(response.status).to eq(422)
        expect(response.headers['Content-Type']).to match('json')
        expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
        expect(response.headers['Access-Control-Request-Method']).to eq('*')
        expect(JSON.parse(response.body)["valid"]).to eq nil
        expect(JSON.parse(response.body)["errors"]["email"].nil?).to eq false
      end

      it 'returns a 401 not authorized error to an anonymous user', :skip_reqres do
        get "/api/organizations/#{first_organization.subdomain}/invitations/validations?email=jim@sumocreations.com"
        expect(response.response_code).to eq(401)
        expect(response.body).to match('OAuth')
        expect(response.headers['Content-Type']).to match('json')
        expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
        expect(response.headers['Access-Control-Request-Method']).to eq('*')
      end

      it 'returns a 401 if the current token has expired', :skip_reqres do
        get_as_user  "/api/organizations/#{first_organization.subdomain}/invitations/validations?email=jim@sumocreations.com", expired: true
        expect(response.status).to eq(401)
        expect(response.headers['Content-Type']).to match('json')
        expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
        expect(response.headers['Access-Control-Request-Method']).to eq('*')
      end

    end

  end

  context 'GET /api/organizations/:organization_subdomain/invitations' do

    let(:existing_organization) { create :organization }
    let(:invitation) { create :invitation, user_id: user.id, organization_id: existing_organization.id, accepted: true }
    let(:pending_invitation) { create :invitation, email: email, organization_id: existing_organization.id }
    let(:accepted_invitation) { create :invitation, accepted: true, organization_id: existing_organization.id }
    let(:irrelevant_invitation) { create :invitation }
    let(:path) { "/api/organizations/#{existing_organization.subdomain}/invitations/" }

    before(:each) do
      [existing_organization,
        accepted_invitation,
        pending_invitation,
        irrelevant_invitation
      ].each { |i| i.touch }
    end

    it 'returns all invitations for the organization', reqres_title: 'List all invitations for an organization.' do
      get_as_user path+'?filter=all'
      expect(response.status).to eq(200)
      expect(response.headers['Content-Type']).to match('json')
      expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
      expect(response.headers['Access-Control-Request-Method']).to eq('*')
      expect(JSON.parse(response.body).count).to eq 2
    end

    it 'returns all invitations for the organization if left blank', :skip_reqres do
      get_as_user path
      expect(response.status).to eq(200)
      expect(response.headers['Content-Type']).to match('json')
      expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
      expect(response.headers['Access-Control-Request-Method']).to eq('*')
      expect(JSON.parse(response.body).count).to eq 2
    end

    it 'should only return accepted invitations when filtering for accepted', reqres_title: 'List accepted invitations for an organization.' do
      get_as_user path+'?filter=accepted'
      expect(response.status).to eq(200)
      expect(response.headers['Content-Type']).to match('json')
      expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
      expect(response.headers['Access-Control-Request-Method']).to eq('*')
      expect(JSON.parse(response.body).count).to eq 1
      expect(JSON.parse(response.body).first["id"]).to eq accepted_invitation.id
    end

    it 'should only return pending invitations when filtering for pending', reqres_title: 'List pending invitations for an organization.' do
      get_as_user path+'?filter=pending'
      expect(response.status).to eq(200)
      expect(response.headers['Content-Type']).to match('json')
      expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
      expect(response.headers['Access-Control-Request-Method']).to eq('*')
      expect(JSON.parse(response.body).count).to eq 1
      expect(JSON.parse(response.body).first["id"]).to eq pending_invitation.id
    end

    it 'returns a 400 if the filter is invalid', :skip_reqres do
      get_as_user path+'?filter=acptecpted'
      expect(response.status).to eq(400)
      expect(response.headers['Content-Type']).to match('json')
      expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
      expect(response.headers['Access-Control-Request-Method']).to eq('*')
    end

    it 'returns a 404 if the organization does not exist', :skip_reqres do
      get_as_user '/api/organizations/9999/invitations?filter=all'
      expect(response.status).to eq(404)
      expect(response.headers['Content-Type']).to match('json')
      expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
      expect(response.headers['Access-Control-Request-Method']).to eq('*')
    end

    it 'returns a 401 if the access token has expired', :skip_reqres do
      get_as_user path+'?filter=all', expired: true
      expect(response.status).to eq(401)
      expect(response.headers['Content-Type']).to match('json')
      expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
      expect(response.headers['Access-Control-Request-Method']).to eq('*')
    end

    it 'returns a 403 if the user is not an admin', :skip_reqres do
      user.update_attributes(is_super_admin: false)
      get_as_user path+'?filter=all'
      expect(response.status).to eq(403)
      expect(response.headers['Content-Type']).to match('json')
      expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
      expect(response.headers['Access-Control-Request-Method']).to eq('*')
    end

  end

  context 'GET /api/organizations/:organization_subdomain/invitations/:invitation_id' do

    let(:existing_organization) { create :organization }
    let(:invitation) { create :invitation, user_id: user.id, organization_id: existing_organization.id, accepted: true }
    let(:irrelevant_organization) { create :organization }
    let(:pending_invitation) { create :invitation, email: email, organization_id: existing_organization.id }
    let(:path) { "/api/organizations/#{existing_organization.subdomain}/invitations/" }

    before(:each) do
      [existing_organization,
        pending_invitation,
        irrelevant_organization
      ].each { |i| i.touch }
    end

    it 'returns an existing invitation for the organization', reqres_title: 'Provides details for an existing invitation.' do
      get_as_user path+pending_invitation.id.to_s
      expect(response.status).to eq(200)
      expect(response.headers['Content-Type']).to match('json')
      expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
      expect(response.headers['Access-Control-Request-Method']).to eq('*')
      expect(JSON.parse(response.body)["id"]).to eq pending_invitation.id
    end

    it 'returns a 404 if the invitation exists but not for the supplied organization', :skip_reqres do
      get_as_user '/api/organizations/'+irrelevant_organization.subdomain+'/invitations/'+pending_invitation.id.to_s
      expect(response.status).to eq(404)
      expect(response.headers['Content-Type']).to match('json')
      expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
      expect(response.headers['Access-Control-Request-Method']).to eq('*')
    end

    it 'returns a 401 if the access token has expired', :skip_reqres do
      get_as_user path+pending_invitation.id.to_s, expired: true
      expect(response.status).to eq(401)
      expect(response.headers['Content-Type']).to match('json')
      expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
      expect(response.headers['Access-Control-Request-Method']).to eq('*')
    end

    it 'returns a 403 if the user is not an admin', :skip_reqres do
      user.update_attributes(is_super_admin: false)
      get_as_user path+pending_invitation.id.to_s
      expect(response.status).to eq(403)
      expect(response.headers['Content-Type']).to match('json')
      expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
      expect(response.headers['Access-Control-Request-Method']).to eq('*')
    end

  end

  context 'POST /api/organizations/:organization_id/invitations/:invitation_id/resend' do

    let(:message_delivery) { instance_double(ActionMailer::MessageDelivery) }
    let(:existing_organization) { create :organization }
    let(:invitation) { create :invitation, user_id: user.id, organization_id: existing_organization.id, accepted: true }
    let(:pending_invitation) { create :invitation, email: email, organization_id: existing_organization.id }
    let(:path) { "/api/organizations/#{existing_organization.subdomain}/invitations/#{pending_invitation.id}/resend" }

    before(:each) do
      [existing_organization, pending_invitation].each { |i| i.touch }
    end

    it 'should return a 201 if a pending invitation exists', reqres_title: 'Resend an invitation to join an organization' do
      expect(NotificationsMailer).to receive(:invitation).and_return(message_delivery)
      expect(message_delivery).to receive(:deliver_now)
      post_as_user path
      expect(response.status).to eq(201)
      expect(response.headers['Content-Type']).to match('json')
      expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
      expect(response.headers['Access-Control-Request-Method']).to eq('*')
      expect(JSON.parse(response.body)['id']).to eq(pending_invitation.id)
    end

    it 'should return a 404 if the invitation is not pending', :skip_reqres do
      pending_invitation.update_attributes(accepted: true)
      expect(NotificationsMailer).not_to receive(:invitation)
      post_as_user path
      expect(response.status).to eq(404)
      expect(response.headers['Content-Type']).to match('json')
      expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
      expect(response.headers['Access-Control-Request-Method']).to eq('*')
    end

    it 'should return a 404 if the organization does not exist', :skip_reqres do
      expect(NotificationsMailer).not_to receive(:invitation)
      post_as_user "/api/organizations/9999/invitations/#{pending_invitation.id}/resend"
      expect(response.status).to eq(404)
      expect(response.headers['Content-Type']).to match('json')
      expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
      expect(response.headers['Access-Control-Request-Method']).to eq('*')
    end

    it 'should return a 404 if the invitation does not exist', :skip_reqres do
      expect(NotificationsMailer).not_to receive(:invitation)
      post_as_user "/api/organizations/#{existing_organization.subdomain}/invitations/9999/resend"
      expect(response.status).to eq(404)
      expect(response.headers['Content-Type']).to match('json')
      expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
      expect(response.headers['Access-Control-Request-Method']).to eq('*')
    end

    it 'returns a 401 if the access token has expired', :skip_reqres do
      expect(NotificationsMailer).not_to receive(:invitation)
      post_as_user path, expired: true
      expect(response.status).to eq(401)
      expect(response.headers['Content-Type']).to match('json')
      expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
      expect(response.headers['Access-Control-Request-Method']).to eq('*')
    end

    it 'returns a 403 if the user is not an admin', :skip_reqres do
      expect(NotificationsMailer).not_to receive(:invitation)
      user.update_attributes(is_super_admin: false)
      post_as_user path
      expect(response.status).to eq(403)
      expect(response.headers['Content-Type']).to match('json')
      expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
      expect(response.headers['Access-Control-Request-Method']).to eq('*')
    end

  end

  context 'PATCH /api/organizations/:organization_id/invitations/:invitation_id/accept' do

    let(:message_delivery) { instance_double(ActionMailer::MessageDelivery) }
    let(:existing_organization) { create :organization }
    let(:pending_invitation) { create :invitation, email: email, organization_id: existing_organization.id }
    let(:path) { "/api/organizations/#{existing_organization.subdomain}/invitations/accept" }
    let(:token) { pending_invitation.generate_token }

    before(:each) do
      [existing_organization, pending_invitation, user].each { |i| i.touch }
    end


    it 'should return a 200 if a pending invitation exists', reqres_title: 'Accept an invitation to join an organization' do
      patch_as_user path, params: {token: token}
      expect(response.status).to eq(200)
      expect(response.headers['Content-Type']).to match('json')
      expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
      expect(response.headers['Access-Control-Request-Method']).to eq('*')
      expect(JSON.parse(response.body)['id']).to eq(pending_invitation.id)
      expect(Invitation.find(pending_invitation.id).accepted).to eq(true)
      expect(Invitation.find(pending_invitation.id).user_id).to eq(user.id)
    end

    it 'should upgrade the user to a super admin if the invite permits', :skip_reqres do
      user.update_attributes(is_super_admin: false)
      expect(user.is_super_admin).to eq(false)
      pending_invitation.update_attributes(is_super_admin: true)
      expect(pending_invitation.is_super_admin).to eq(true)
      patch_as_user path, params: {token: pending_invitation.generate_token}
      expect(response.status).to eq(200)
      expect(response.headers['Content-Type']).to match('json')
      expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
      expect(response.headers['Access-Control-Request-Method']).to eq('*')
      expect(JSON.parse(response.body)['id']).to eq(pending_invitation.id)
      expect(Invitation.find(pending_invitation.id).accepted).to eq(true)
      expect(Invitation.find(pending_invitation.id).user_id).to eq(user.id)
      expect(User.find(user.id).is_super_admin).to eq(true)
    end

    it 'should return a 403 if the invitation has already been accepted', :skip_reqres do
      pending_invitation.update_attributes(accepted: true)
      patch_as_user path, params: {token: token, user: user}
      expect(response.status).to eq(403)
      expect(response.headers['Content-Type']).to match('json')
      expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
      expect(response.headers['Access-Control-Request-Method']).to eq('*')
    end

    it 'should return a 401 if the auth token has expired', :skip_reqres do
      patch_as_user path, params: {token: token, user: user}, expired: true
      expect(response.status).to eq(401)
      expect(response.headers['Content-Type']).to match('json')
      expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
      expect(response.headers['Access-Control-Request-Method']).to eq('*')
    end

    it 'should return a 401 if the invitation token has expired', :skip_reqres do
      patch_as_user path, params: {token: pending_invitation.generate_token(exp: -1000), user: user}, expired: true
      expect(response.status).to eq(401)
      expect(response.headers['Content-Type']).to match('json')
      expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
      expect(response.headers['Access-Control-Request-Method']).to eq('*')
    end

  end

end
