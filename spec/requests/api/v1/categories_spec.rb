require 'rails_helper'

describe "API V1 Categories", type: :request, reqres_section: 'Categories' do

  let(:accept_header) { 'application/vnd.film_cal-v1+json' }
  let(:user) { create :user, email: 'user@example.com', password: '12345678' }
  let(:existing_organization) { create :organization }
  let(:irrelevant_organization) { create :organization }
  let(:first_invitation) { create :invitation,
    user_id: user.id,
    organization_id: existing_organization.id,
    accepted: true }
  let(:second_invitation) { create :invitation,
    user_id: user.id,
    organization_id: irrelevant_organization.id,
    accepted: true }
  let(:sample_params) {{
    name: 'Test',
    active: true,
    open: true,
    use_custom_styles: false
  }}
  let(:style_sample_params) {{
    use_custom_styles: true,
    style: {
      fill_color: '#fff',
      outline_color: '#000',
      text_color: '#000',
      text_decoration: 'none',
      text_alignment: 'left',
      font: 'Default',
      font_weight: 'normal',
      font_size: 12,
      font_style: 'italic'
    }
  }}
  let(:invalid_style_sample_params) {{
    use_custom_styles: true,
    name: 'customized',
    style: {
      fill_color: '#fff',
      outline_color: '#000',
      text_color: '#000',
      text_decoration: 'none',
      text_alignment: 'NONE',
      font: 'Sonoma Light TTF',
      font_weight: 'light',
      font_size: 28,
      font_style: 'italic'
    }
  }}

  before(:example) do
    [existing_organization, irrelevant_organization, first_invitation, second_invitation].each { |x| x.touch }
  end

  context 'GET /api/organizations/:id/calendars/:id/categories' do

    let(:first_calendar) { create :calendar, organization_id: existing_organization.id }
    let(:second_calendar) { create :calendar, organization_id: existing_organization.id }
    let(:first_category) { create :category, calendar_id: first_calendar.id }
    let(:second_category) { create :category, calendar_id: first_calendar.id }
    let(:third_category) { create :category, calendar_id: second_calendar.id }
    let(:path) { "/api/organizations/#{existing_organization.subdomain}/calendars/#{first_calendar.id}/categories" }

    before(:each) do
      existing_organization.calendars << [first_calendar, second_calendar]
      irrelevant_organization.calendars << second_calendar
      first_calendar.categories << [first_category, second_category]
      second_calendar.categories << [third_category]
    end

    it 'returns a 200 with the existing categorys for the requested calendar if the user is an admin', :skip_reqres do
      first_invitation.update_attributes(is_admin: true)
      get_as_user path
      expect(response.status).to eq(200)
      body = JSON.parse(response.body)
      expect(body.length).to eq 2
      result_ids = [body[0]["id"], body[1]["id"]]
      expect(result_ids.include?(first_category.id)).to eq true
      expect(result_ids.include?(second_category.id)).to eq true
      expect(response.headers['Content-Type']).to match('json')
      expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
      expect(response.headers['Access-Control-Request-Method']).to eq('*')
    end

    it 'returns a 200 with the existing categories for the requested calendar if the user is a super admin', :skip_reqres do
      user.update_attributes(is_super_admin: true)
      get_as_user path
      expect(response.status).to eq(200)
      body = JSON.parse(response.body)
      expect(body.length).to eq 2
      result_ids = [body[0]["id"], body[1]["id"]]
      expect(result_ids.include?(first_category.id)).to eq true
      expect(result_ids.include?(second_category.id)).to eq true
      expect(response.headers['Content-Type']).to match('json')
      expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
      expect(response.headers['Access-Control-Request-Method']).to eq('*')
    end

    it 'returns a 404 if the user does not have permission', :skip_reqres do
      get_as_user path
      expect(response.status).to eq(404)
      expect(response.headers['Content-Type']).to match('json')
      expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
      expect(response.headers['Access-Control-Request-Method']).to eq('*')
    end

    it 'returns a 401 not authorized error to an anonymous user', :skip_reqres do
      get path
      expect(response.response_code).to eq(401)
      expect(response.body).to match('OAuth')
      expect(response.headers['Content-Type']).to match('json')
      expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
      expect(response.headers['Access-Control-Request-Method']).to eq('*')
    end

    it 'returns a 401 if the current token has expired', :skip_reqres do
      get_as_user path, expired: true
      expect(response.status).to eq(401)
      expect(response.headers['Content-Type']).to match('json')
      expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
      expect(response.headers['Access-Control-Request-Method']).to eq('*')
    end

    context 'when the user is not invited' do

      before(:each) do
        first_invitation.update_attributes(accepted: false)
      end

      it 'returns a 200 with the existing calendar for the requested ID if the user is a super admin', :skip_reqres do
        user.update_attributes(is_super_admin: true)
        get_as_user path
        expect(response.status).to eq(200)
        body = JSON.parse(response.body)
        expect(body.length).to eq 2
        result_ids = [body[0]["id"], body[1]["id"]]
        expect(result_ids.include?(first_category.id)).to eq true
        expect(result_ids.include?(second_category.id)).to eq true
        expect(response.headers['Content-Type']).to match('json')
        expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
        expect(response.headers['Access-Control-Request-Method']).to eq('*')
      end

      it 'returns a 404 if the user is not an admin', :skip_reqres do
        get_as_user path
        expect(response.status).to eq(404)
        expect(response.headers['Content-Type']).to match('json')
        expect(response.headers['Content-Type']).to match('json')
        expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
        expect(response.headers['Access-Control-Request-Method']).to eq('*')
      end

    end

    context 'with permissions present' do
      let(:read_permission) { create :granular_permission, read: true, write: false, invitation_id: first_invitation.id, calendar_id: first_calendar.id }

      before(:each) do
        first_invitation.granular_permissions << [read_permission]
      end

      it 'returns the existing calendar for the requested ID if the user has read permissions', :skip_reqres do
        get_as_user path
        expect(response.status).to eq(200)
        body = JSON.parse(response.body)
        expect(body.length).to eq 2
        result_ids = [body[0]["id"], body[1]["id"]]
        expect(result_ids.include?(first_category.id)).to eq true
        expect(result_ids.include?(second_category.id)).to eq true
        expect(response.headers['Content-Type']).to match('json')
        expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
        expect(response.headers['Access-Control-Request-Method']).to eq('*')
      end

      it 'returns a 404 if the user does not have read permission', :skip_reqres do
        read_permission.update_attributes(read: false)
        get_as_user path
        expect(response.status).to eq(404)
        expect(response.headers['Content-Type']).to match('json')
        expect(response.headers['Content-Type']).to match('json')
        expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
        expect(response.headers['Access-Control-Request-Method']).to eq('*')
      end

    end

  end

  context 'GET /api/organizations/:id/calendars/:id/categories/:id' do

    let(:first_calendar) { create :calendar, organization_id: existing_organization.id }
    let(:second_calendar) { create :calendar, organization_id: existing_organization.id }
    let(:first_category) { create :category, calendar_id: first_calendar.id }
    let(:second_category) { create :category, calendar_id: first_calendar.id }
    let(:third_category) { create :category, calendar_id: second_calendar.id }
    let(:path) { "/api/organizations/#{existing_organization.subdomain}/calendars/#{first_calendar.id}/categories/#{first_category.id}" }

    before(:each) do
      existing_organization.calendars << [first_calendar, second_calendar]
      irrelevant_organization.calendars << second_calendar
      first_calendar.categories << [first_category, second_category]
      second_calendar.categories << [third_category]
    end

    it 'returns a 200 with the existing category for the requested ID if the user is an admin', :skip_reqres do
      first_invitation.update_attributes(is_admin: true)
      get_as_user path
      expect(response.status).to eq(200)
      expect(JSON.parse(response.body)["id"]).to eq first_category.id
      expect(response.headers['Content-Type']).to match('json')
      expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
      expect(response.headers['Access-Control-Request-Method']).to eq('*')
    end

    it 'returns a 200 with the existing category for the requested ID if the user is a super admin', :skip_reqres do
      user.update_attributes(is_super_admin: true)
      get_as_user path
      expect(response.status).to eq(200)
      expect(JSON.parse(response.body)["id"]).to eq first_category.id
      expect(response.headers['Content-Type']).to match('json')
      expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
      expect(response.headers['Access-Control-Request-Method']).to eq('*')
    end

    it 'returns a 404 if the user does not have permission', :skip_reqres do
      get_as_user path
      expect(response.status).to eq(404)
      expect(response.headers['Content-Type']).to match('json')
      expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
      expect(response.headers['Access-Control-Request-Method']).to eq('*')
    end

    it 'returns a 401 not authorized error to an anonymous user', :skip_reqres do
      get path
      expect(response.response_code).to eq(401)
      expect(response.body).to match('OAuth')
      expect(response.headers['Content-Type']).to match('json')
      expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
      expect(response.headers['Access-Control-Request-Method']).to eq('*')
    end

    it 'returns a 401 if the current token has expired', :skip_reqres do
      get_as_user path, expired: true
      expect(response.status).to eq(401)
      expect(response.headers['Content-Type']).to match('json')
      expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
      expect(response.headers['Access-Control-Request-Method']).to eq('*')
    end

    context 'when the user is not invited' do

      before(:each) do
        first_invitation.update_attributes(accepted: false)
      end

      it 'returns a 200 with the existing category for the requested ID if the user is a super admin', :skip_reqres do
        user.update_attributes(is_super_admin: true)
        get_as_user path
        expect(response.status).to eq(200)
        expect(JSON.parse(response.body)["id"]).to eq first_category.id
        expect(response.headers['Content-Type']).to match('json')
        expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
        expect(response.headers['Access-Control-Request-Method']).to eq('*')
      end

      it 'returns a 404 if the user is an admin but has not accepted their invitation', :skip_reqres do
        first_invitation.update_attributes(is_admin: true)
        get_as_user path
        expect(response.status).to eq(404)
        expect(response.headers['Content-Type']).to match('json')
        expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
        expect(response.headers['Access-Control-Request-Method']).to eq('*')
      end

      it 'returns a 404 if the user is not an admin', :skip_reqres do
        get_as_user path
        expect(response.status).to eq(404)
        expect(response.headers['Content-Type']).to match('json')
        expect(response.headers['Content-Type']).to match('json')
        expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
        expect(response.headers['Access-Control-Request-Method']).to eq('*')
      end

    end

    context 'with permissions present' do
      let(:read_permission) { create :granular_permission, read: true, write: false, invitation_id: first_invitation.id, calendar_id: first_calendar.id }

      before(:each) do
        first_invitation.granular_permissions << [read_permission]
      end

      it 'returns the existing calendar for the requested ID if the user has read permissions', :skip_reqres do
        get_as_user path
        expect(response.status).to eq(200)
        expect(JSON.parse(response.body)["id"]).to eq first_category.id
        expect(response.headers['Content-Type']).to match('json')
        expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
        expect(response.headers['Access-Control-Request-Method']).to eq('*')
      end

      it 'returns a 404 if the user does not have read permission', :skip_reqres do
        read_permission.update_attributes(read: false)
        get_as_user path
        expect(response.status).to eq(404)
        expect(response.headers['Content-Type']).to match('json')
        expect(response.headers['Content-Type']).to match('json')
        expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
        expect(response.headers['Access-Control-Request-Method']).to eq('*')
      end

    end

  end

  context 'POST /api/organizations/:id/calendars/:id/categories' do

    let(:first_calendar) { create :calendar, organization_id: existing_organization.id }
    let(:path) { "/api/organizations/#{existing_organization.subdomain}/calendars/#{first_calendar.id}/categories" }

    before(:each) do
      existing_organization.calendars << first_calendar
    end

    context 'if the user is an admin' do

      before(:each) do
        first_invitation.update_attribute(:is_admin, true)
      end

      it 'returns a 201 if the category is valid', reqres_title: 'Create a new category' do
        post_as_user path, params: sample_params
        expect(response.status).to eq(201)
        expect(response.headers['Content-Type']).to match('json')
        expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
        expect(response.headers['Access-Control-Request-Method']).to eq('*')
        expect(Category.count).to eq(1)
      end

      it 'returns a 201 if the category has specified styles', :skip_reqres do
        sample_params.merge!(style_sample_params)
        post_as_user path, params: sample_params
        expect(response.status).to eq(201)
        expect(response.headers['Content-Type']).to match('json')
        expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
        expect(response.headers['Access-Control-Request-Method']).to eq('*')
        expect(Category.count).to eq(1)
      end

      it 'returns a 401 if the token is expired', :skip_reqres do
        post_as_user path, params: sample_params, expired: true
        expect(response.status).to eq(401)
        expect(response.headers['Content-Type']).to match('json')
        expect(Category.count).to eq(0)
      end

      it 'returns a 404 if the user is not an admin', :skip_reqres do
        first_invitation.update_attribute(:is_admin, false)
        post_as_user path, params: sample_params
        expect(response.status).to eq(404)
        expect(response.headers['Content-Type']).to match('json')
        expect(Category.count).to eq(0)
      end

      it 'returns a 404 if the user is not invited to the organization', :skip_reqres do
        first_invitation.update_attributes(accepted: false)
        post_as_user path, params: sample_params
        expect(response.status).to eq(404)
        expect(response.headers['Content-Type']).to match('json')
        expect(Category.count).to eq(0)
      end

      it 'returns a 422 if the category is invalid', :skip_reqres do
        sample_params[:name] = ""
        post_as_user path, params: sample_params
        expect(response.status).to eq(422)
        expect(response.headers['Content-Type']).to match('json')
        expect(Category.count).to eq(0)
      end

      it 'returns a 422 if the category is needs missing styles', :skip_reqres do
        sample_params.merge!(invalid_style_sample_params)
        post_as_user path, params: sample_params
        expect(response.status).to eq(422)
        expect(response.headers['Content-Type']).to match('json')
        expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
        expect(response.headers['Access-Control-Request-Method']).to eq('*')
        expect(Category.count).to eq(0)
      end

    end

    context 'if the user only has read access' do
      let(:permission) { create :granular_permission, calendar_id: first_calendar.id, read: true }

      before(:each) do
        first_invitation.update_attribute(:is_admin, false)
        first_invitation.granular_permissions << permission
      end

      it 'returns a 401 if the token is expired', :skip_reqres do
        post_as_user path, params: sample_params, expired: true
        expect(response.status).to eq(401)
        expect(response.headers['Content-Type']).to match('json')
        expect(Category.count).to eq(0)
      end

      it 'returns a 404 if the user is not invited to the organization', :skip_reqres do
        first_invitation.update_attributes(accepted: false)
        post_as_user path, params: sample_params
        expect(response.status).to eq(404)
        expect(response.headers['Content-Type']).to match('json')
        expect(Category.count).to eq(0)
      end

    end

    context 'if the user only has write access' do
      let(:permission) { create :granular_permission, calendar_id: first_calendar.id, read: true, write: true }

      before(:each) do
        first_invitation.update_attribute(:is_admin, false)
        first_invitation.granular_permissions << permission
      end

      it 'returns a 401 if the token is expired', :skip_reqres do
        post_as_user path, params: sample_params, expired: true
        expect(response.status).to eq(401)
        expect(response.headers['Content-Type']).to match('json')
        expect(Category.count).to eq(0)
      end

      it 'returns a 404 if the user is not invited to the organization', :skip_reqres do
        first_invitation.update_attributes(accepted: false)
        post_as_user path, params: sample_params
        expect(response.status).to eq(404)
        expect(response.headers['Content-Type']).to match('json')
        expect(Category.count).to eq(0)
      end

      it 'returns a 201 if the category is valid', :skip_reqres do
        post_as_user path, params: sample_params
        expect(response.status).to eq(201)
        expect(response.headers['Content-Type']).to match('json')
        expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
        expect(response.headers['Access-Control-Request-Method']).to eq('*')
        expect(Category.count).to eq(1)
      end

      it 'returns a 422 if the category is invalid', :skip_reqres do
        sample_params[:name] = ""
        post_as_user path, params: sample_params
        expect(response.status).to eq(422)
        expect(response.headers['Content-Type']).to match('json')
        expect(Category.count).to eq(0)
      end

    end

  end

  context 'PUT /api/organizations/:id/calendars/:id/categories/:id' do

    let(:existing_calendar) { create :calendar }
    let(:existing_category) { create :category }
    let(:child_category) { create :category, category_id: existing_category.id }
    let(:path) { "/api/organizations/#{existing_organization.subdomain}/calendars/#{existing_calendar.id}/categories/#{existing_category.id}" }
    let(:permission) { create :granular_permission, calendar_id: existing_calendar.id, invitation_id: first_invitation.id }

    before(:each) do
      existing_organization.calendars << existing_calendar
      existing_calendar.categories << [existing_category, child_category]
      existing_calendar.granular_permissions << permission
    end

    it 'returns a 200 if the category is valid and user is an admin', reqres_title: 'Update an existing category' do
      first_invitation.update_attribute(:is_admin, true)
      put_as_user path, params: sample_params
      expect(response.status).to eq(200)
      expect(response.headers['Content-Type']).to match('json')
      expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
      expect(response.headers['Access-Control-Request-Method']).to eq('*')
      expect(Category.find(existing_category.id).name).to eq('Test')
    end

    it 'returns a 200 if the category has specified styles', :skip_reqres do
      first_invitation.update_attribute(:is_admin, true)
      sample_params.merge!(style_sample_params)
      put_as_user path, params: sample_params
      expect(response.status).to eq(200)
      expect(response.headers['Content-Type']).to match('json')
      expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
      expect(response.headers['Access-Control-Request-Method']).to eq('*')
      expect(Category.count).to eq(2)
    end

    it 'returns a 200 if the category is valid and user has write access', :skip_reqres do
      permission.update_attributes(write: true)
      put_as_user path, params: sample_params
      expect(response.status).to eq(200)
      expect(response.headers['Content-Type']).to match('json')
      expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
      expect(response.headers['Access-Control-Request-Method']).to eq('*')
      expect(Category.find(existing_category.id).name).to eq('Test')
    end

    it 'returns a 401 if the token is expired', :skip_reqres do
      put_as_user path, params: { name: 'Test' }, expired: true
      expect(response.status).to eq(401)
      expect(response.headers['Content-Type']).to match('json')
      expect(Category.find(existing_category.id).name).not_to eq('Test')
    end

    it 'returns a 400 if the params are incomplete', :skip_reqres do
      first_invitation.update_attribute(:is_admin, false)
      put_as_user path, params: {}
      expect(response.status).to eq(400)
      expect(response.headers['Content-Type']).to match('json')
      expect(Category.find(existing_category.id).name).not_to eq('Test')
    end

    it 'returns a 404 if the user does not have write access', :skip_reqres do
      put_as_user path, params: sample_params
      expect(response.status).to eq(404)
      expect(response.headers['Content-Type']).to match('json')
      expect(Category.find(existing_category.id).name).not_to eq('Test')
    end

    it 'returns a 404 if the category does not exist', :skip_reqres do
      put_as_user "/api/organizations/#{existing_organization.subdomain}/calendars/#{existing_calendar.id}/categories/#{existing_category.id+999}" , params: sample_params
      expect(response.status).to eq(404)
      expect(response.headers['Content-Type']).to match('json')
    end

    it 'returns a 404 if the user is not invited to the organization', :skip_reqres do
      first_invitation.update_attributes(accepted: false)
      put_as_user path, params: sample_params
      expect(response.status).to eq(404)
      expect(response.headers['Content-Type']).to match('json')
    end

    it 'returns a 422 if the category is invalid due to a cyclical parent category_id', :skip_reqres do
      permission.update_attributes(write: true)
      sample_params[:category_id] = child_category.id
      put_as_user path, params: sample_params
      expect(response.status).to eq(422)
      expect(response.headers['Content-Type']).to match('json')
      expect(Category.find(existing_category.id).name).not_to eq('Test')
    end

    it 'returns a 422 if the category has specified styles but they are not valid', :skip_reqres do
      first_invitation.update_attribute(:is_admin, true)
      sample_params.merge!(invalid_style_sample_params)
      put_as_user path, params: sample_params
      expect(response.status).to eq(422)
      expect(response.headers['Content-Type']).to match('json')
      expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
      expect(response.headers['Access-Control-Request-Method']).to eq('*')
      expect(Category.find(existing_category.id).name).not_to eq('Test')
    end

  end

  context 'PATCH /api/organizations/:id/calendars/:id/categories/:id/parent' do

    let(:existing_calendar) { create :calendar }
    let(:irrelevant_calendar) { create :calendar }
    let(:irrelevant_category) { create :category, calendar_id: irrelevant_calendar.id }
    let(:existing_category) { create :category, calendar_id: existing_calendar.id }
    let(:adjacent_category) { create :category, calendar_id: existing_calendar.id }
    let(:nested_category) { create :category, category_id: existing_category.id }
    let(:double_nested_category) { create :category, category_id: nested_category.id }
    let(:path) { "/api/organizations/#{existing_organization.subdomain}/calendars/#{existing_calendar.id}/categories/#{existing_category.id}/parent" }
    let(:permission) { create :granular_permission, calendar_id: existing_calendar.id, invitation_id: first_invitation.id }

    before(:each) do
      existing_organization.calendars << existing_calendar
      existing_calendar.categories << [existing_category, adjacent_category, nested_category, double_nested_category]
      existing_calendar.granular_permissions << permission
      irrelevant_category.touch
    end

    it 'returns a 200 if the category is valid and user is an admin', reqres_title: 'Update an existing category' do
      first_invitation.update_attribute(:is_admin, true)
      patch_as_user path, params: {category_id: adjacent_category.id}
      expect(response.status).to eq(200)
      expect(response.headers['Content-Type']).to match('json')
      expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
      expect(response.headers['Access-Control-Request-Method']).to eq('*')
      expect(Category.find(existing_category.id).category_id).to eq(adjacent_category.id)
    end

    it 'returns a 200 if the category is valid and user has write access', :skip_reqres do
      permission.update_attributes(write: true)
      patch_as_user path, params: {category_id: adjacent_category.id}
      expect(response.status).to eq(200)
      expect(response.headers['Content-Type']).to match('json')
      expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
      expect(response.headers['Access-Control-Request-Method']).to eq('*')
      expect(Category.find(existing_category.id).category_id).to eq(adjacent_category.id)
    end

    it 'returns a 401 if the token is expired', :skip_reqres do
      patch_as_user path, params: {category_id: adjacent_category.id}, expired: true
      expect(response.status).to eq(401)
      expect(response.headers['Content-Type']).to match('json')
      expect(Category.find(existing_category.id).category_id).not_to eq(adjacent_category.id)
    end

    it 'returns a 401 if the token is expired', :skip_reqres do
      patch_as_user path, params: {category_id: adjacent_category.id}, expired: true
      expect(response.status).to eq(401)
      expect(response.headers['Content-Type']).to match('json')
      expect(Category.find(existing_category.id).category_id).not_to eq(adjacent_category.id)
    end

    it 'returns a 200 if the params are nil', :skip_reqres do
      first_invitation.update_attribute(:is_admin, true)
      patch_as_user path, params: {category_id: nil}
      expect(response.status).to eq(200)
      expect(response.headers['Content-Type']).to match('json')
      expect(Category.find(existing_category.id).category_id).to eq(nil)
    end

    it 'returns a 404 if the user does not have write access', :skip_reqres do
      patch_as_user path, params: {category_id: adjacent_category.id}
      expect(response.status).to eq(404)
      expect(response.headers['Content-Type']).to match('json')
      expect(Category.find(existing_category.id).name).not_to eq('Test')
      expect(Category.find(existing_category.id).category_id).not_to eq(adjacent_category.id)
    end

    it 'returns a 404 if the category does not exist', :skip_reqres do
      patch_as_user "/api/organizations/#{existing_organization.subdomain}/calendars/#{existing_calendar.id}/categories/#{existing_category.id+999}/parent" ,
        params: {category_id: nil}
      expect(response.status).to eq(404)
      expect(response.headers['Content-Type']).to match('json')
      expect(Category.find(existing_category.id).category_id).not_to eq(adjacent_category.id)
    end

    it 'returns a 404 if the user is not invited to the organization', :skip_reqres do
      first_invitation.update_attributes(accepted: false)
      patch_as_user path, params: {category_id: adjacent_category.id}
      expect(response.status).to eq(404)
      expect(response.headers['Content-Type']).to match('json')
      expect(Category.find(existing_category.id).category_id).not_to eq(adjacent_category.id)
    end

    it 'returns a 200 but ignores the update if the parent is a nested category', :skip_reqres do
      permission.update_attributes(write: true)
      patch_as_user path, params: {category_id: nested_category.id}
      expect(response.status).to eq(200)
      expect(response.headers['Content-Type']).to match('json')
      expect(Category.find(existing_category.id).category_id).not_to eq(nested_category.id)
    end

    it 'returns a 200 if the category is attempting to nest within a deep nested category but ignores the update', :skip_reqres do
      permission.update_attributes(write: true)
      patch_as_user path, params: {category_id: double_nested_category.id}
      expect(response.status).to eq(200)
      expect(response.headers['Content-Type']).to match('json')
      expect(Category.find(existing_category.id).category_id).not_to eq(double_nested_category.id)
    end

    it 'returns a 404 if the category is attempting to nest within an exterrnal category', :skip_reqres do
      permission.update_attributes(write: true)
      patch_as_user path, params: {category_id: irrelevant_category.id}
      expect(response.status).to eq(404)
      expect(response.headers['Content-Type']).to match('json')
      expect(Category.find(existing_category.id).category_id).not_to eq(irrelevant_category.id)
    end

  end

  context 'PATCH /api/organizations/:id/calendars/:id/categories/:id/open' do

    let(:existing_calendar) { create :calendar }
    let(:irrelevant_category) { create :category }
    let(:existing_category) { create :category }
    let(:adjacent_category) { create :category }
    let(:nested_category) { create :category, category_id: existing_category.id }
    let(:double_nested_category) { create :category, category_id: nested_category.id }
    let(:path) { "/api/organizations/#{existing_organization.subdomain}/calendars/#{existing_calendar.id}/categories/#{existing_category.id}/open" }
    let(:permission) { create :granular_permission, calendar_id: existing_calendar.id, invitation_id: first_invitation.id }

    before(:each) do
      existing_organization.calendars << existing_calendar
      existing_calendar.categories << [existing_category, adjacent_category, nested_category, double_nested_category]
      existing_calendar.granular_permissions << permission
      irrelevant_category.touch
    end

    it 'returns a 200 if the category is valid and user is an admin', reqres_title: 'Open or close a category' do
      first_invitation.update_attribute(:is_admin, true)
      existing_category.update_attributes(open: false)
      patch_as_user path, params: {open: true}
      expect(response.status).to eq(200)
      expect(response.headers['Content-Type']).to match('json')
      expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
      expect(response.headers['Access-Control-Request-Method']).to eq('*')
      expect(Category.find(existing_category.id).open).to eq(true)
    end

    it 'returns a 200 if the category is valid and user is an admin and the open attribute is set to false' do
      first_invitation.update_attribute(:is_admin, true)
      existing_category.update_attributes(open: true)
      patch_as_user path, params: {open: false}
      expect(response.status).to eq(200)
      expect(response.headers['Content-Type']).to match('json')
      expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
      expect(response.headers['Access-Control-Request-Method']).to eq('*')
      expect(Category.find(existing_category.id).open).to eq(false)
    end

    it 'returns a 200 if the category is valid and user has write access', :skip_reqres do
      permission.update_attributes(write: true)
      existing_category.update_attributes(open: true)
      patch_as_user path, params: {open: false}
      expect(response.status).to eq(200)
      expect(response.headers['Content-Type']).to match('json')
      expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
      expect(response.headers['Access-Control-Request-Method']).to eq('*')
      expect(Category.find(existing_category.id).open).to eq(false)
    end

    it 'returns a 401 if the token is expired', :skip_reqres do
      existing_category.update_attributes(open: true)
      patch_as_user path, params: {open: false}, expired: true
      expect(response.status).to eq(401)
      expect(response.headers['Content-Type']).to match('json')
      expect(Category.find(existing_category.id).open).not_to eq(false)
    end

    it 'returns a 401 if the token is expired', :skip_reqres do
      existing_category.update_attributes(open: true)
      patch_as_user path, params: {open: false}, expired: true
      expect(response.status).to eq(401)
      expect(response.headers['Content-Type']).to match('json')
      expect(Category.find(existing_category.id).open).not_to eq(false)
    end

    it 'returns a 400 if the params do not include "open"', :skip_reqres do
      existing_category.update_attributes(open: true)
      first_invitation.update_attribute(:is_admin, true)
      patch_as_user path, params: {}
      expect(response.status).to eq(400)
      expect(response.headers['Content-Type']).to match('json')
      expect(Category.find(existing_category.id).open).to eq(true)
    end

    it 'returns a 404 if the user does not have write access', :skip_reqres do
      existing_category.update_attributes(open: true)
      patch_as_user path, params: {open: false}
      expect(response.status).to eq(404)
      expect(response.headers['Content-Type']).to match('json')
      expect(Category.find(existing_category.id).name).not_to eq('Test')
      expect(Category.find(existing_category.id).open).not_to eq(false)
    end

    it 'returns a 404 if the category does not exist', :skip_reqres do
      existing_category.update_attributes(open: true)
      patch_as_user "/api/organizations/#{existing_organization.subdomain}/calendars/#{existing_calendar.id}/categories/#{existing_category.id+999}/open" ,
        params: {open: false}
      expect(response.status).to eq(404)
      expect(response.headers['Content-Type']).to match('json')
      expect(Category.find(existing_category.id).open).not_to eq(false)
    end

  end

  context 'PATCH /api/organizations/:id/calendars/:id/categories/:id/activate' do

    let(:existing_calendar) { create :calendar }
    let(:irrelevant_category) { create :category }
    let(:existing_category) { create :category }
    let(:adjacent_category) { create :category }
    let(:nested_category) { create :category, category_id: existing_category.id }
    let(:double_nested_category) { create :category, category_id: nested_category.id }
    let(:path) { "/api/organizations/#{existing_organization.subdomain}/calendars/#{existing_calendar.id}/categories/#{existing_category.id}/activate" }
    let(:permission) { create :granular_permission, calendar_id: existing_calendar.id, invitation_id: first_invitation.id }

    before(:each) do
      existing_organization.calendars << existing_calendar
      existing_calendar.categories << [existing_category, adjacent_category, nested_category, double_nested_category]
      existing_calendar.granular_permissions << permission
      irrelevant_category.touch
    end

    it 'returns a 200 if the category is valid and user is an admin', reqres_title: 'Open or close a category' do
      first_invitation.update_attribute(:is_admin, true)
      existing_category.update_attributes(active: false)
      patch_as_user path, params: {active: true}
      expect(response.status).to eq(200)
      expect(response.headers['Content-Type']).to match('json')
      expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
      expect(response.headers['Access-Control-Request-Method']).to eq('*')
      expect(Category.find(existing_category.id).active).to eq(true)
    end

    it 'returns a 200 if the category is valid and user is an admin and the active attribute is set to false' do
      first_invitation.update_attribute(:is_admin, true)
      existing_category.update_attributes(active: true)
      patch_as_user path, params: {active: false}
      expect(response.status).to eq(200)
      expect(response.headers['Content-Type']).to match('json')
      expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
      expect(response.headers['Access-Control-Request-Method']).to eq('*')
      expect(Category.find(existing_category.id).active).to eq(false)
    end

    it 'returns a 200 if the category is valid and user has write access', :skip_reqres do
      permission.update_attributes(write: true)
      existing_category.update_attributes(active: true)
      patch_as_user path, params: {active: false}
      expect(response.status).to eq(200)
      expect(response.headers['Content-Type']).to match('json')
      expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
      expect(response.headers['Access-Control-Request-Method']).to eq('*')
      expect(Category.find(existing_category.id).active).to eq(false)
    end

    it 'returns a 401 if the token is expired', :skip_reqres do
      existing_category.update_attributes(active: true)
      patch_as_user path, params: {active: false}, expired: true
      expect(response.status).to eq(401)
      expect(response.headers['Content-Type']).to match('json')
      expect(Category.find(existing_category.id).active).not_to eq(false)
    end

    it 'returns a 401 if the token is expired', :skip_reqres do
      existing_category.update_attributes(active: true)
      patch_as_user path, params: {active: false}, expired: true
      expect(response.status).to eq(401)
      expect(response.headers['Content-Type']).to match('json')
      expect(Category.find(existing_category.id).active).not_to eq(false)
    end

    it 'returns a 400 if the params do not include "active"', :skip_reqres do
      existing_category.update_attributes(active: true)
      first_invitation.update_attribute(:is_admin, true)
      patch_as_user path, params: {}
      expect(response.status).to eq(400)
      expect(response.headers['Content-Type']).to match('json')
      expect(Category.find(existing_category.id).active).to eq(true)
    end

    it 'returns a 404 if the user does not have write access', :skip_reqres do
      existing_category.update_attributes(active: true)
      patch_as_user path, params: {active: false}
      expect(response.status).to eq(404)
      expect(response.headers['Content-Type']).to match('json')
      expect(Category.find(existing_category.id).name).not_to eq('Test')
      expect(Category.find(existing_category.id).active).not_to eq(false)
    end

    it 'returns a 404 if the category does not exist', :skip_reqres do
      existing_category.update_attributes(active: true)
      patch_as_user "/api/organizations/#{existing_organization.subdomain}/calendars/#{existing_calendar.id}/categories/#{existing_category.id+999}/activate" ,
        params: {active: false}
      expect(response.status).to eq(404)
      expect(response.headers['Content-Type']).to match('json')
      expect(Category.find(existing_category.id).active).not_to eq(false)
    end

  end

  context 'DELETE /api/organizations/:id/calendars/:id/categories/:id' do

    let(:existing_calendar) { create :calendar }
    let(:existing_category) { create :category }
    let(:path) { "/api/organizations/#{existing_organization.subdomain}/calendars/#{existing_calendar.id}/categories/#{existing_category.id}" }

    before(:each) do
      existing_organization.calendars << existing_calendar
      existing_calendar.categories << existing_category
      first_invitation.update_attribute(:is_admin, true)
    end

    it 'returns a 200 if the category exists', reqres_title: 'Delete an existing calendar' do
      expect(Category.where(id: existing_category.id).count).to eq(1)
      delete_as_user path
      expect(response.status).to eq(200)
      expect(response.headers['Content-Type']).to match('json')
      expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
      expect(response.headers['Access-Control-Request-Method']).to eq('*')
      expect(Category.where(id: existing_category.id).count).to eq(0)
      expect(JSON.parse(response.body)["is_deleted"]).to eq(true)
    end

    context 'with child events and categories' do

      let(:child_category) { create :category }
      let(:nested_child_category) { create :category }
      let(:child_item) { create :item }
      let(:child_event) { create :event }
      let(:nested_child_item) { create :item }
      let(:nested_child_event) { create :event }

      before(:each) do
        existing_category.categories << child_category
        existing_category.items << child_item
        child_item.events << child_event
        child_category.categories << nested_child_category
        nested_child_category.items << nested_child_item
        nested_child_item.events << nested_child_event
      end

      it 'should mark all child events and categories as deleted' do
        expect(Category.not_deleted.where(id: existing_category.id).count).to eq(1)
        expect(Category.not_deleted.where(id: child_category.id).count).to eq(1)
        expect(Category.not_deleted.where(id: nested_child_category.id).count).to eq(1)
        expect(Item.not_deleted.where(id: child_item.id).count).to eq(1)
        expect(Item.not_deleted.where(id: nested_child_item.id).count).to eq(1)
        expect(Item.not_deleted.where(id: child_item.id).count).to eq(1)
        expect(Event.not_deleted.where(id: child_event.id).count).to eq(1)
        expect(Event.not_deleted.where(id: nested_child_event.id).count).to eq(1)
        delete_as_user path
        expect(response.status).to eq(200)
        expect(response.headers['Content-Type']).to match('json')
        expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
        expect(response.headers['Access-Control-Request-Method']).to eq('*')
        expect(Category.not_deleted.where(id: existing_category.id).count).to eq(0)
        expect(Category.not_deleted.where(id: child_category.id).count).to eq(0)
        expect(Category.not_deleted.where(id: nested_child_category.id).count).to eq(0)
        expect(Item.not_deleted.where(id: existing_category.id).count).to eq(0)
        expect(Item.not_deleted.where(id: child_item.id).count).to eq(0)
        expect(Item.not_deleted.where(id: nested_child_item.id).count).to eq(0)
        expect(Event.not_deleted.where(id: child_event.id).count).to eq(0)
        expect(Event.not_deleted.where(id: nested_child_event.id).count).to eq(0)
        expect(JSON.parse(response.body)["is_deleted"]).to eq(true)
      end

    end

    it 'returns a 200 if the category exists and the user is a super admin without any connection to the organization' do
      first_invitation.update_attributes(accepted: false)
      user.update_attributes(is_super_admin: true)
      expect(Category.where(id: existing_category.id).count).to eq(1)
      delete_as_user path
      expect(response.status).to eq(200)
      expect(response.headers['Content-Type']).to match('json')
      expect(response.headers['Access-Control-Allow-Origin']).to eq('*')
      expect(response.headers['Access-Control-Request-Method']).to eq('*')
      expect(Category.where(id: existing_category.id).count).to eq(0)
    end

    it 'returns a 401 if the user is not authorized' do
      expect(Category.where(id: existing_category.id).count).to eq(1)
      delete path
      expect(response.status).to eq(401)
      expect(response.headers['Content-Type']).to match('json')
      expect(Category.where(id: existing_category.id).count).to eq(1)
    end

    it 'returns a 401 if the user uses an expired token' do
      expect(Category.where(id: existing_category.id).count).to eq(1)
      delete_as_user path, expired: true
      expect(response.status).to eq(401)
      expect(response.headers['Content-Type']).to match('json')
      expect(Category.where(id: existing_category.id).count).to eq(1)
    end

    it 'returns a 404 if the category exists and the user is not an admin' do
      first_invitation.destroy
      expect(Category.where(id: existing_category.id).count).to eq(1)
      delete_as_user path
      expect(response.status).to eq(404)
      expect(response.headers['Content-Type']).to match('json')
      expect(Category.where(id: existing_category.id).count).to eq(1)
    end

    it 'returns a 404 if the category exists and the user is an admin without any connection to the organization' do
      first_invitation.update_attributes(accepted: false)
      expect(Category.where(id: existing_category.id).count).to eq(1)
      delete_as_user path
      expect(response.status).to eq(404)
      expect(response.headers['Content-Type']).to match('json')
      expect(Category.where(id: existing_category.id).count).to eq(1)
    end

    it 'returns a 404 if the category does not exist' do
      existing_category.destroy
      delete_as_user path
      expect(response.status).to eq(404)
      expect(response.headers['Content-Type']).to match('json')
    end


  end
end
