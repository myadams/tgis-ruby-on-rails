FactoryGirl.define do
  factory :calendar do
    sequence(:name) { |n| "Calendar-#{n}" }
  end
end
