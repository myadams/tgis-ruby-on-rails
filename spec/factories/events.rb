FactoryGirl.define do
  factory :event do
    sequence(:name) { |n| "Event-#{n}" }
    starts_at { 0.days.from_now }
    length 1
    position 0
    partition 0
  end
end
