FactoryGirl.define do
  factory :granular_permission do
    read false
    write false
  end
end
