FactoryGirl.define do
  factory :style do
    outline_color "#ff9900"
    fill_color "#000000"
    text_decoration "none"
    text_alignment "left"
    font_style "normal"
    font_size 12
    font_weight "normal"
    font "Default"
  end
end
