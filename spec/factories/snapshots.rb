FactoryGirl.define do
  factory :snapshot do
    sequence(:name) { |n| "Snapshot-#{n}" }
    sequence(:notes) { |n| "Notes about snapshot #{n}"}
  end
end
