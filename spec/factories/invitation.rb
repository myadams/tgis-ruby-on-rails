FactoryGirl.define do
  factory :invitation do
    sequence(:user_id) { |n| n }
    sequence(:organization_id) { |n| n }
    sequence(:email) { |n| "email-#{n}@example.com" }
    sequence(:name) { |n| "Johnny-#{n} Appleseed"}
  end
end
