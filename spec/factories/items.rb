FactoryGirl.define do
  factory :item do
    sequence(:name) { |n| "Name-#{n}" }
    use_custom_styles false
  end
end
