FactoryGirl.define do
  factory :access_grant, class: Doorkeeper::AccessGrant do
    sequence(:resource_owner_id) { |n| n }
    application { Factory.create :client_application }
    redirect_uri 'https://app.com/callback'
    expires_in 100
  end

  factory :access_token, class: Doorkeeper::AccessToken do
    sequence(:resource_owner_id) { |n| n }
    application { Factory.create :client_application }
    expires_in 2.hours.to_i
    scopes :public

    factory :clientless_access_token do
      application nil
    end

    factory :expired_access_token do
      expires_in -2.hours
    end
  end

  factory :client_application, class: Doorkeeper::Application do
    sequence(:name) { |n| "Application #{n}" }
    redirect_uri 'https://app.com/callback'
  end

  # do not name this factory :user, otherwise it will conflict with factories
  # from applications that use doorkeeper factories in their own tests
  factory :doorkeeper_testing_user, class: :user
end
