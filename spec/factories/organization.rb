FactoryGirl.define do
  factory :organization do
    sequence(:name) { |n| "Organization-#{n}" }
    sequence(:subdomain) { |n| "organization-#{n}"}
    color "#ff9900"
    uses_granular_permissions false
  end
end
