FactoryGirl.define do
  factory :holiday do
    sequence(:name) { |n| "Holiday-#{n}" }
    starts_at { 0.hours.from_now }
    length 1
  end
end
