RSpec.configure do |config|
  config.include FactoryGirl::Syntax::Methods
  config.before(:suite) do
    factories_to_lint = FactoryGirl.factories.reject do |factory|
      factory.name =~ /access_|^doorkeeper/
    end
    begin
      DatabaseCleaner.start
      FactoryGirl.lint factories_to_lint
    ensure
      DatabaseCleaner.clean
    end
  end
end
