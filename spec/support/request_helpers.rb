module Requests
  module JsonHelpers
    attr_reader :response

    def json
      JSON.parse(response.body)
    end

    def method_missing(method, *args)
      return unless method =~ /_helper$/

      raise "#{method} expects a valid argument, '#{args.first}' passed" if args.first.blank?
      raise "#{args.first.class} is not persisted" if args.first.try(:id).blank?

      method.to_s.gsub(/_helper$/, 'Serializer').classify.constantize.new(args.first, root: nil).attributes.as_json
    end
  end

  module AuthenticationHelpers
    def get_as_user(url, params: {}, expired: false)
      get(url, params: params, headers: _user_auth_headers(expired))
    end

    def post_as_user(url, params: {}, expired: false)
      post(url, params: _params_unless_upload(params), headers: _user_auth_headers(expired))
    end

    def put_as_user(url, params: {}, expired: false)
      put(url, params: _params_unless_upload(params), headers: _user_auth_headers(expired))
    end

    def delete_as_user(url, params: {}, expired: false)
      delete(url, params: _params_unless_upload(params), headers: _user_auth_headers(expired))
    end

    def patch_as_user(url, params: {}, expired: false)
      patch(url, params: _params_unless_upload(params), headers: _user_auth_headers(expired))
    end

  private

    def _params_unless_upload(params)
      params = {} if params.blank?
      params = params.to_json unless params.values.any? { |v| v.class == Rack::Test::UploadedFile }
    end

    def _user_auth_headers(expired)
      token = expired ? _expired_access_token : _access_token
      {
        'Content-Type' => 'application/json',
        'Accept' => @_accept_header,
        'Authorization' => "Bearer #{token.token}",
        'User-Agent' => 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_6_8) AppleWebKit/536.5 (KHTML, like Gecko) ' \
          'Chrome/19.0.1084.56 Safari/536.5',
        'X-API-Client' => 'ExampleApp/TestSuite 0.0.1',
        'X-API-Device' => 'iPhone 5,1 (iOS 8.1.3)'
      }
    end

    def _accept_header
      @_accept_header ||= try(:accept_header) || 'application/json'
    end

    def _user
      raise '"user" variable in request tests is not defined' unless respond_to?(:user) || user.present?
      user
    end

    def _access_token
      @_access_token ||= create(:access_token, application: _client_application, resource_owner_id: _user.id)
    end

    def _expired_access_token
      @_access_token ||= create(:expired_access_token, application: _client_application, resource_owner_id: _user.id)
    end

    def _client_application
      @_client_application ||= try(:client_application) || create(:client_application)
    end
  end
end
