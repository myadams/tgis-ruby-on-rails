ReqresRspec.configure do |c|
  # c.templates_path = Rails.root.join('spec/support/reqres/templates') # Path to custom templates
  # c.output_path = 'some path' # by default it will use doc/reqres
  # c.formatters = %w(MyCustomFormatter) # List of custom formatters, these can be inherited from ReqresRspec::Formatters::HTML
  c.title = 'NFPA Calendar API Documentation' # Title for your documentation
  # c.amazon_s3 = {
  #   credentials: {
  #     access_key_id: ENV['AWS_ACCESS_KEY_ID'], # by default it will use AWS_ACCESS_KEY_ID env var
  #     secret_access_key: ENV['AWS_SECRET_ACCESS_KEY'], # by default it will use AWS_SECRET_ACCESS_KEY env var
  #     region: (ENV['AWS_REGION'] || 'us-east-1'),
  #   },
  #   bucket: ENV['AWS_REQRES_BUCKET'], # by default it will use AWS_REQRES_BUCKET env for bucket name
  #   enabled: false # Enable upload (only with REQRES_UPLOAD env var set)
  # }
end
