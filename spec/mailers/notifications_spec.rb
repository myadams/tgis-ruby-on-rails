require "rails_helper"

RSpec.describe NotificationsMailer, type: :mailer do
  describe "invitation" do
    let(:organization) { Organization.create(name: "Sumo Creations", subdomain: "sumo-creations", color: "#ff0000")}
    let(:invitation) { Invitation.create(email: ENV["SES_DEFAULT_EMAIL_ADDRESS"], organization_id: organization.id)}
    let(:mail) { NotificationsMailer.invitation(invitation, invitation.generate_token) }

    it "renders the headers" do
      expect(mail.subject).to eq("[NPA CAL] Invitation to the Sumo Creations Schedule")
      expect(mail.to).to eq([invitation.email])
      expect(mail.from).to eq([ENV["SES_DEFAULT_EMAIL_ADDRESS"]])
    end

    it "renders the body" do
      expect(mail.body.encoded).to match("You have been invited to Sumo Creations's")
    end
  end

  describe "forgot_password" do
    let(:user) { User.create(email: ENV["SES_DEFAULT_EMAIL_ADDRESS"], password: "password", password_confirmation: "password")}
    let(:mail) { NotificationsMailer.forgot_password(user, user.generate_token) }

    it "renders the headers" do
      expect(mail.subject).to eq("[NPA CAL] Reset Your Password")
      expect(mail.to).to eq([user.email])
      expect(mail.from).to eq([ENV["SES_DEFAULT_EMAIL_ADDRESS"]])
    end

    it "renders the body" do
      expect(mail.body.encoded).to match("You have 15 minutes before the access link expires.")
    end
  end

  describe "confirm_password_reset" do
    let(:user) { User.create(email: ENV["SES_DEFAULT_EMAIL_ADDRESS"], password: "password", password_confirmation: "password")}
    let(:mail) { NotificationsMailer.confirm_password_reset(user) }

    it "renders the headers" do
      expect(mail.subject).to eq("[NPA CAL] Your Password was Successfully Reset!")
      expect(mail.to).to eq([user.email])
      expect(mail.from).to eq([ENV["SES_DEFAULT_EMAIL_ADDRESS"]])
    end

    it "renders the body" do
      expect(mail.body.encoded).to match("Your Password has Been Reset")
    end
  end

end
