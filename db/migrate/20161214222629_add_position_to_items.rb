class AddPositionToItems < ActiveRecord::Migration[5.0]
  def change
    add_column :items, :position, :int, index: true, default: 0
  end
end
