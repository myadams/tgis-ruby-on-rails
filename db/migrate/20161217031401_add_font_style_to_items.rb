class AddFontStyleToItems < ActiveRecord::Migration[5.0]
  def change
    add_column :items, :font_style, :string, default: 'normal'
  end
end
