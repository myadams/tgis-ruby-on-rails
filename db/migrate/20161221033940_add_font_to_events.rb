class AddFontToEvents < ActiveRecord::Migration[5.0]
  def change
    add_column :events, :font, :string
  end
end
