class AddSnapshotIdToHolidays < ActiveRecord::Migration[5.0]
  def change
    add_column :holidays, :snapshot_id, :integer, index: true
  end
end
