class AddIsDeletedFlagToEntities < ActiveRecord::Migration[5.0]
  def change
    add_column :organizations, :is_deleted, :boolean, default: false, index: true
    add_column :invitations, :is_deleted, :boolean, default: false, index: true
    add_column :calendars, :is_deleted, :boolean, default: false, index: true
    add_column :users, :is_deleted, :boolean, default: false, index: true
  end
end
