class AddActiveToCategories < ActiveRecord::Migration[5.0]
  def change
    add_column :categories, :active, :boolean, default: true
    add_index :categories, :active
  end
end
