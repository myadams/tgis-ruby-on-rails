class AddTextColorToStyles < ActiveRecord::Migration[5.0]
  def change
    add_column :styles, :text_color, :string
  end
end
