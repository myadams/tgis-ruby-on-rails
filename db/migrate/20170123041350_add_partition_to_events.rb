class AddPartitionToEvents < ActiveRecord::Migration[5.0]
  def change
    add_column :events, :partition, :int, default: 0
    add_index :events, :partition
  end
end
