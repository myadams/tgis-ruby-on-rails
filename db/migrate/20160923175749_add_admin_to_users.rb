class AddAdminToUsers < ActiveRecord::Migration[5.0]
  def change
    change_table(:users) do |t|
      t.boolean :is_admin, default: false
    end
    add_index :users, :is_admin
  end
end
