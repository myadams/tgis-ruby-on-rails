class AddDefaultValueToStylesTextColor < ActiveRecord::Migration[5.0]
  def change
    change_column :styles, :text_color, :string, default: "#000000"
  end
end
