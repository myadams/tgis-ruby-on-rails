class AddOpenStateToCategories < ActiveRecord::Migration[5.0]
  def change
    add_column :categories, :open, :boolean, default: false
    add_index :categories, :open
  end
end
