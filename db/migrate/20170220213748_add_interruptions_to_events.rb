class AddInterruptionsToEvents < ActiveRecord::Migration[5.0]
  def change
    add_column :events, :interruptions, :text, array: true
  end
end
