class AddWeekendsExemptToCalendars < ActiveRecord::Migration[5.0]
  def change
    add_column :calendars, :weekends_exempt, :bool, default: false
  end
end
