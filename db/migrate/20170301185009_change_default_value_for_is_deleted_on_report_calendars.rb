class ChangeDefaultValueForIsDeletedOnReportCalendars < ActiveRecord::Migration[5.0]
  def change
    change_column :report_calendars, :is_deleted, :boolean, default: false
  end
end
