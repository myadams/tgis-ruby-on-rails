class CreateSnapshots < ActiveRecord::Migration[5.0]
  def change
    create_table :snapshots do |t|
      t.string :name
      t.integer :calendar_id
      t.boolean :is_deleted
      t.timestamps
    end
    
    add_index :snapshots, :calendar_id
    add_index :snapshots, :is_deleted

    add_column :styles, :snapshot_id, :integer, index: true
    add_column :categories, :snapshot_id, :integer, index: true
    add_column :items, :snapshot_id, :integer, index: true
    add_column :events, :snapshot_id, :integer, index: true
  end
end
