class CreateHolidays < ActiveRecord::Migration[5.0]
  def change
    create_table :holidays do |t|
      t.datetime :starts_at, index: true
      t.datetime :ends_at, index: true
      t.integer :length
      t.belongs_to :calendar, index: true
      t.boolean :is_deleted, default: false, index: true
      t.string :name
      t.timestamps
    end
  end
end
