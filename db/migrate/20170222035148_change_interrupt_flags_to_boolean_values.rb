class ChangeInterruptFlagsToBooleanValues < ActiveRecord::Migration[5.0]
  def change
    remove_column :categories, :interrupted_by_holidays, :integer
    remove_column :items, :interrupted_by_holidays, :integer
    remove_column :events, :interrupted_by_holidays, :integer
    remove_column :categories, :interrupted_by_weekends, :integer
    remove_column :items, :interrupted_by_weekends, :integer
    remove_column :events, :interrupted_by_weekends, :integer
    add_column :categories, :interrupted_by_holidays, :boolean, default: false, index: true
    add_column :items, :interrupted_by_holidays, :boolean, default: false, index: true
    add_column :events, :interrupted_by_holidays, :boolean, default: false, index: true
    add_column :categories, :interrupted_by_weekends, :boolean, default: false, index: true
    add_column :items, :interrupted_by_weekends, :boolean, default: false, index: true
    add_column :events, :interrupted_by_weekends, :boolean, default: false, index: true
  end
end
