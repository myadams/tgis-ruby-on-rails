class AddLengthToEvents < ActiveRecord::Migration[5.0]
  def change
    add_column :events, :length, :integer, default: 1
  end
end
