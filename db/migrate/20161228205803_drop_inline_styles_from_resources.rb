class DropInlineStylesFromResources < ActiveRecord::Migration[5.0]
  def change
    remove_column :categories, :fill_color, :string
    remove_column :categories, :outline_color, :string
    remove_column :categories, :font, :string
    remove_column :categories, :font_size, :string
    remove_column :categories, :font_style, :string
    remove_column :categories, :font_weight, :string
    remove_column :categories, :text_alignment, :string
    remove_column :categories, :text_decoration, :string

    remove_column :items, :fill_color, :string
    remove_column :items, :outline_color, :string
    remove_column :items, :font, :string
    remove_column :items, :font_size, :string
    remove_column :items, :font_style, :string
    remove_column :items, :font_weight, :string
    remove_column :items, :text_alignment, :string
    remove_column :items, :text_decoration, :string

    remove_column :events, :fill_color, :string
    remove_column :events, :outline_color, :string
    remove_column :events, :font, :string
    remove_column :events, :font_size, :string
    remove_column :events, :font_style, :string
    remove_column :events, :font_weight, :string
    remove_column :events, :text_alignment, :string
    remove_column :events, :text_decoration, :string
  end
end
