class AddUserInvitationMetaData < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :name, :string, default: "", length: 32
    add_column :invitations, :name, :string, default: "", length: 32
    add_column :invitations, :is_admin, :boolean, default: false
    add_column :invitations, :is_super_admin, :boolean, default: false
    add_column :invitations, :last_invited_at, :datetime
  end
end
