class AddPendingStatusToCalendars < ActiveRecord::Migration[5.0]
  def change
    add_column :calendars, :pending, :boolean, default: false
  end
end
