class CreateItems < ActiveRecord::Migration[5.0]
  def change
    create_table :items do |t|
      t.string :name
      t.string :fill_color
      t.string :outline_color
      t.string :font, default: "system"
      t.string :font_size, default: "12"
      t.string :font_weight, default: "normal"
      t.string :text_alignment, default: "left"
      t.belongs_to :calendar, index: true
      t.timestamps
    end
  end
end
