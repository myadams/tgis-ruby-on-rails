class AddPartitionToItems < ActiveRecord::Migration[5.0]
  def change
    add_column :items, :partition, :integer, default: 0
  end
end
