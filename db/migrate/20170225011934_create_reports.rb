class CreateReports < ActiveRecord::Migration[5.0]
  def change
    create_table :reports do |t|
      t.integer :organization_id, index: true
      t.string :name
      t.boolean :is_deleted, default: false, index: true
      t.timestamps
    end
  end
end
