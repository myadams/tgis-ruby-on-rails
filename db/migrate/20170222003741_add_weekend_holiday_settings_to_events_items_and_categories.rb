class AddWeekendHolidaySettingsToEventsItemsAndCategories < ActiveRecord::Migration[5.0]
  def change
    add_column :categories, :interrupted_by_holidays, :integer, default: false, index: true
    add_column :items, :interrupted_by_holidays, :integer, default: false, index: true
    add_column :events, :interrupted_by_holidays, :integer, default: false, index: true
    rename_column :calendars, :holidays_exempt, :interrupted_by_holidays

    add_column :categories, :interrupted_by_weekends, :integer, default: false, index: true
    add_column :items, :interrupted_by_weekends, :integer, default: false, index: true
    add_column :events, :interrupted_by_weekends, :integer, default: false, index: true
    rename_column :calendars, :weekends_exempt, :interrupted_by_weekends
  end
end
