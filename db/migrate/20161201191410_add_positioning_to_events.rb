class AddPositioningToEvents < ActiveRecord::Migration[5.0]
  def change
    add_column :events, :position, :integer, default: 0, index: true
    add_column :events, :late_day, :boolean, default: false, index: true
  end
end
