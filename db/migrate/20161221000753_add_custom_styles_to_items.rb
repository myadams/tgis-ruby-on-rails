class AddCustomStylesToItems < ActiveRecord::Migration[5.0]
  def change
    add_column :items, :use_custom_styles, :boolean, default: false
    add_index :items, :use_custom_styles
  end
end
