class AddUseCustomStylesToEvents < ActiveRecord::Migration[5.0]
  def change
    add_column :events, :font_size, :string
    add_column :events, :font_style, :string
    add_column :events, :font_weight, :string
    add_column :events, :text_alignment, :string
    add_column :events, :text_decoration, :string
    add_column :events, :use_custom_styles, :boolean, default: false
    add_index :events, :use_custom_styles
  end
end
