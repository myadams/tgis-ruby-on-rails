class AddHolidayExemptionFlagToCalendars < ActiveRecord::Migration[5.0]
  def change
    add_column :calendars, :holidays_exempt, :boolean, default: false
  end
end
