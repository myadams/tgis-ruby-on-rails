class AddLockedToItems < ActiveRecord::Migration[5.0]
  def change
    add_column :items, :locked, :boolean, index: true, default: false
  end
end
