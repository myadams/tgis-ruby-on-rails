class AddUseCustomStylesToCategories < ActiveRecord::Migration[5.0]
  def change
    add_column :categories, :fill_color, :string
    add_column :categories, :outline_color, :string
    add_column :categories, :font, :string
    add_column :categories, :font_size, :string
    add_column :categories, :font_style, :string
    add_column :categories, :font_weight, :string
    add_column :categories, :text_alignment, :string
    add_column :categories, :text_decoration, :string
    add_column :categories, :use_custom_styles, :boolean, default: false
    add_index :categories, :use_custom_styles
  end
end
