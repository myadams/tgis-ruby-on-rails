class AddSuperadminToUsers < ActiveRecord::Migration[5.0]
  def change
    change_table(:users) do |t|
      t.boolean :is_super_admin, default: false, index: true
    end
  end
end
