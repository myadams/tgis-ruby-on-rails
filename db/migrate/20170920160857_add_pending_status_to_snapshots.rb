class AddPendingStatusToSnapshots < ActiveRecord::Migration[5.0]
  def change
    add_column :snapshots, :pending, :boolean
  end
end
