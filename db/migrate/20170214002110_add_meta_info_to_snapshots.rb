class AddMetaInfoToSnapshots < ActiveRecord::Migration[5.0]
  def change
    add_column :snapshots, :notes, :string
    add_column :snapshots, :user_id, :integer
  end
end
