class CreateReportCalendars < ActiveRecord::Migration[5.0]
  def change
    create_table :report_calendars do |t|
      t.integer :calendar_id, index: true
      t.integer :report_id, index: true
      t.boolean :is_deleted, index: true

      t.timestamps
    end
  end
end
