class RemoveOrganizationFromGranularPermission < ActiveRecord::Migration[5.0]
  def change
    change_table(:granular_permissions) do |t|
      t.remove :organization_id
    end
  end
end
