class RemoveEmailFromGranularPermissions < ActiveRecord::Migration[5.0]
  def change
    change_table(:granular_permissions) do |t|
      t.remove :email
    end
  end
end
