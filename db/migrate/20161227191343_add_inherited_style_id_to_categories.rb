class AddInheritedStyleIdToCategories < ActiveRecord::Migration[5.0]
  def change
    add_column :categories, :inherited_style_id, :integer
    add_index :categories, :inherited_style_id
  end
end
