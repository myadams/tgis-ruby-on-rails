class AddCategoryIdToItems < ActiveRecord::Migration[5.0]
  def change
    add_column :items, :category_id, :int, index: true
  end
end
