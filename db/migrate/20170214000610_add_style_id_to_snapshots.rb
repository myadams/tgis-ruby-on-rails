class AddStyleIdToSnapshots < ActiveRecord::Migration[5.0]
  def change
    add_column :snapshots, :style_id, :integer
    add_index :snapshots, :style_id
  end
end
