class CreateEvents < ActiveRecord::Migration[5.0]
  def change
    create_table :events do |t|
      t.string :name
      t.string :fill_color
      t.string :outline_color
      t.datetime :starts_at, index: true
      t.datetime :ends_at, index: true
      t.belongs_to :calendar, index: true
      t.timestamps
    end
  end
end
