class AddTextDecorationToItems < ActiveRecord::Migration[5.0]
  def change
    add_column :items, :text_decoration, :string, default: 'none'
  end
end
