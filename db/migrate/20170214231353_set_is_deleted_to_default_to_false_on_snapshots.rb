class SetIsDeletedToDefaultToFalseOnSnapshots < ActiveRecord::Migration[5.0]
  def change
    change_column_default :snapshots, :is_deleted, false
  end
end
