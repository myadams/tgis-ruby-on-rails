class CreateGranularPermissions < ActiveRecord::Migration[5.0]
  def change
    create_table :granular_permissions do |t|
      t.belongs_to :organization, index: true
      t.belongs_to :invitation, index: true
      t.belongs_to :calendar, index: true
      t.boolean :read, default: false
      t.boolean :write, default: false
      t.string :email, index: true
      t.timestamps
    end
  end
end
