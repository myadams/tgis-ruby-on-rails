class CreateCategories < ActiveRecord::Migration[5.0]
  def change
    create_table :categories do |t|
      t.string :name
      t.belongs_to :calendar, index: true

      t.timestamps
    end
  end
end
