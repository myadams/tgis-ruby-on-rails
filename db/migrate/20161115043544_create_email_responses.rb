class CreateEmailResponses < ActiveRecord::Migration[5.0]
  def change
    create_table :email_responses do |t|
      t.string :email, index: true
      t.text :extra_info
      t.integer :response_type

      t.timestamps null: false
    end
  end
end
