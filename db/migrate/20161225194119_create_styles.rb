class CreateStyles < ActiveRecord::Migration[5.0]
  def change
    create_table :styles do |t|
      t.string :fill_color, default: '#FFFFFF'
      t.string :outline_color, default: '#FFFFFF'
      t.string :font, default: 'Default'
      t.integer :font_size, default: 12
      t.string :font_style, default: 'normal'
      t.string :font_weight, default: 'normal'
      t.string :text_alignment, default: 'left'
      t.string :text_decoration, default: 'none'
      t.integer :calendar_id
      t.timestamps
    end
    add_column :items, :style_id, :integer
    add_column :events, :style_id, :integer
    add_column :calendars, :style_id, :integer
    add_column :categories, :style_id, :integer
    add_index :styles, :calendar_id
    add_index :items, :style_id
    add_index :events, :style_id
    add_index :calendars, :style_id
    add_index :categories, :style_id
  end
end
