class AddSubdomainAndColorToOrganizations < ActiveRecord::Migration[5.0]
  def change
    change_table(:organizations) do |t|
      t.string :color
      t.string :subdomain, index: true
    end
  end
end
