class CreateInvitations < ActiveRecord::Migration[5.0]
  def change
    create_table :invitations do |t|
      t.belongs_to :organization, index: true
      t.belongs_to :user, index: true
      t.boolean :accepted, default: false
      t.string :email, index: true
      t.timestamps
    end
  end
end
