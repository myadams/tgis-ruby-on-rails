class AddInheritedStyleIdToCalendars < ActiveRecord::Migration[5.0]
  def change
    add_column :calendars, :inherited_style_id, :integer
    add_index :calendars, :inherited_style_id
  end
end
