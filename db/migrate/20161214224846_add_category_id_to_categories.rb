class AddCategoryIdToCategories < ActiveRecord::Migration[5.0]
  def change
    add_column :categories, :category_id, :int, index: true
  end
end
