class AddInheritedStyleIdToItems < ActiveRecord::Migration[5.0]
  def change
    add_column :items, :inherited_style_id, :integer
    add_index :items, :inherited_style_id
  end
end
