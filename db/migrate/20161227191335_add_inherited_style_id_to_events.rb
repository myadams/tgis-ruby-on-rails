class AddInheritedStyleIdToEvents < ActiveRecord::Migration[5.0]
  def change
    add_column :events, :inherited_style_id, :integer
    add_index :events, :inherited_style_id
  end
end
