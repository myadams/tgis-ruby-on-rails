class AddPartitionToCategories < ActiveRecord::Migration[5.0]
  def change
    add_column :categories, :partition, :integer, default: 0
  end
end
