class AddIsDeletedToCategories < ActiveRecord::Migration[5.0]
  def change
    add_column :categories, :is_deleted, :boolean, index: true, default: false
  end
end
