class AddItemToEvents < ActiveRecord::Migration[5.0]
  def change
    add_column :events, :item_id, :integer, index: true
  end
end
