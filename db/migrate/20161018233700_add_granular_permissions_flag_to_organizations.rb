class AddGranularPermissionsFlagToOrganizations < ActiveRecord::Migration[5.0]
  def change
    change_table(:organizations) do |t|
      t.boolean :uses_granular_permissions, default: false, index: true
    end
  end
end
