class AddPositionToCategories < ActiveRecord::Migration[5.0]
  def change
    add_column :categories, :position, :int, index: true
  end
end
