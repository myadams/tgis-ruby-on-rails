# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170920160857) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "calendars", force: :cascade do |t|
    t.string   "name"
    t.integer  "organization_id"
    t.datetime "created_at",                              null: false
    t.datetime "updated_at",                              null: false
    t.boolean  "is_deleted",              default: false
    t.boolean  "interrupted_by_holidays", default: false
    t.integer  "style_id"
    t.integer  "inherited_style_id"
    t.boolean  "interrupted_by_weekends", default: false
    t.boolean  "pending",                 default: false
    t.index ["inherited_style_id"], name: "index_calendars_on_inherited_style_id", using: :btree
    t.index ["organization_id"], name: "index_calendars_on_organization_id", using: :btree
    t.index ["style_id"], name: "index_calendars_on_style_id", using: :btree
  end

  create_table "categories", force: :cascade do |t|
    t.string   "name"
    t.integer  "calendar_id"
    t.datetime "created_at",                              null: false
    t.datetime "updated_at",                              null: false
    t.boolean  "is_deleted",              default: false
    t.integer  "position"
    t.integer  "category_id"
    t.boolean  "active",                  default: true
    t.boolean  "open",                    default: false
    t.boolean  "use_custom_styles",       default: false
    t.integer  "style_id"
    t.integer  "inherited_style_id"
    t.integer  "snapshot_id"
    t.integer  "partition",               default: 0
    t.boolean  "interrupted_by_holidays", default: false
    t.boolean  "interrupted_by_weekends", default: false
    t.index ["active"], name: "index_categories_on_active", using: :btree
    t.index ["calendar_id"], name: "index_categories_on_calendar_id", using: :btree
    t.index ["inherited_style_id"], name: "index_categories_on_inherited_style_id", using: :btree
    t.index ["open"], name: "index_categories_on_open", using: :btree
    t.index ["style_id"], name: "index_categories_on_style_id", using: :btree
    t.index ["use_custom_styles"], name: "index_categories_on_use_custom_styles", using: :btree
  end

  create_table "email_responses", force: :cascade do |t|
    t.string   "email"
    t.text     "extra_info"
    t.integer  "response_type"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.index ["email"], name: "index_email_responses_on_email", using: :btree
  end

  create_table "events", force: :cascade do |t|
    t.string   "name"
    t.datetime "starts_at"
    t.datetime "ends_at"
    t.integer  "calendar_id"
    t.datetime "created_at",                              null: false
    t.datetime "updated_at",                              null: false
    t.boolean  "is_deleted",              default: false
    t.integer  "position",                default: 0
    t.boolean  "late_day",                default: false
    t.integer  "item_id"
    t.boolean  "use_custom_styles",       default: false
    t.integer  "style_id"
    t.integer  "inherited_style_id"
    t.integer  "partition",               default: 0
    t.integer  "snapshot_id"
    t.integer  "length",                  default: 1
    t.text     "interruptions",                                        array: true
    t.boolean  "interrupted_by_holidays", default: false
    t.boolean  "interrupted_by_weekends", default: false
    t.index ["calendar_id"], name: "index_events_on_calendar_id", using: :btree
    t.index ["ends_at"], name: "index_events_on_ends_at", using: :btree
    t.index ["inherited_style_id"], name: "index_events_on_inherited_style_id", using: :btree
    t.index ["partition"], name: "index_events_on_partition", using: :btree
    t.index ["starts_at"], name: "index_events_on_starts_at", using: :btree
    t.index ["style_id"], name: "index_events_on_style_id", using: :btree
    t.index ["use_custom_styles"], name: "index_events_on_use_custom_styles", using: :btree
  end

  create_table "granular_permissions", force: :cascade do |t|
    t.integer  "invitation_id"
    t.integer  "calendar_id"
    t.boolean  "read",          default: false
    t.boolean  "write",         default: false
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
    t.index ["calendar_id"], name: "index_granular_permissions_on_calendar_id", using: :btree
    t.index ["invitation_id"], name: "index_granular_permissions_on_invitation_id", using: :btree
  end

  create_table "holidays", force: :cascade do |t|
    t.datetime "starts_at"
    t.datetime "ends_at"
    t.integer  "length"
    t.integer  "calendar_id"
    t.boolean  "is_deleted",  default: false
    t.string   "name"
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
    t.integer  "snapshot_id"
    t.index ["calendar_id"], name: "index_holidays_on_calendar_id", using: :btree
    t.index ["ends_at"], name: "index_holidays_on_ends_at", using: :btree
    t.index ["is_deleted"], name: "index_holidays_on_is_deleted", using: :btree
    t.index ["starts_at"], name: "index_holidays_on_starts_at", using: :btree
  end

  create_table "invitations", force: :cascade do |t|
    t.integer  "organization_id"
    t.integer  "user_id"
    t.boolean  "accepted",        default: false
    t.string   "email"
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
    t.boolean  "is_deleted",      default: false
    t.string   "name",            default: ""
    t.boolean  "is_admin",        default: false
    t.boolean  "is_super_admin",  default: false
    t.datetime "last_invited_at"
    t.index ["email"], name: "index_invitations_on_email", using: :btree
    t.index ["organization_id"], name: "index_invitations_on_organization_id", using: :btree
    t.index ["user_id"], name: "index_invitations_on_user_id", using: :btree
  end

  create_table "items", force: :cascade do |t|
    t.string   "name"
    t.integer  "calendar_id"
    t.datetime "created_at",                              null: false
    t.datetime "updated_at",                              null: false
    t.boolean  "active",                  default: true
    t.integer  "category_id"
    t.boolean  "is_deleted",              default: false
    t.integer  "position",                default: 0
    t.boolean  "locked",                  default: false
    t.boolean  "use_custom_styles",       default: false
    t.integer  "style_id"
    t.integer  "inherited_style_id"
    t.integer  "snapshot_id"
    t.integer  "partition",               default: 0
    t.boolean  "interrupted_by_holidays", default: false
    t.boolean  "interrupted_by_weekends", default: false
    t.index ["calendar_id"], name: "index_items_on_calendar_id", using: :btree
    t.index ["inherited_style_id"], name: "index_items_on_inherited_style_id", using: :btree
    t.index ["style_id"], name: "index_items_on_style_id", using: :btree
    t.index ["use_custom_styles"], name: "index_items_on_use_custom_styles", using: :btree
  end

  create_table "oauth_access_grants", force: :cascade do |t|
    t.integer  "resource_owner_id", null: false
    t.integer  "application_id",    null: false
    t.string   "token",             null: false
    t.integer  "expires_in",        null: false
    t.text     "redirect_uri",      null: false
    t.datetime "created_at",        null: false
    t.datetime "revoked_at"
    t.string   "scopes"
    t.index ["token"], name: "index_oauth_access_grants_on_token", unique: true, using: :btree
  end

  create_table "oauth_access_tokens", force: :cascade do |t|
    t.integer  "resource_owner_id"
    t.integer  "application_id"
    t.string   "token",                               null: false
    t.string   "refresh_token"
    t.integer  "expires_in"
    t.datetime "revoked_at"
    t.datetime "created_at",                          null: false
    t.string   "scopes"
    t.string   "previous_refresh_token", default: "", null: false
    t.index ["refresh_token"], name: "index_oauth_access_tokens_on_refresh_token", unique: true, using: :btree
    t.index ["resource_owner_id"], name: "index_oauth_access_tokens_on_resource_owner_id", using: :btree
    t.index ["token"], name: "index_oauth_access_tokens_on_token", unique: true, using: :btree
  end

  create_table "oauth_applications", force: :cascade do |t|
    t.string   "name",                      null: false
    t.string   "uid",                       null: false
    t.string   "secret",                    null: false
    t.text     "redirect_uri",              null: false
    t.string   "scopes",       default: "", null: false
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
    t.index ["uid"], name: "index_oauth_applications_on_uid", unique: true, using: :btree
  end

  create_table "organizations", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at",                                null: false
    t.datetime "updated_at",                                null: false
    t.boolean  "uses_granular_permissions", default: false
    t.string   "color"
    t.string   "subdomain"
    t.boolean  "is_deleted",                default: false
  end

  create_table "report_calendars", force: :cascade do |t|
    t.integer  "calendar_id"
    t.integer  "report_id"
    t.boolean  "is_deleted",  default: false
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
    t.index ["calendar_id"], name: "index_report_calendars_on_calendar_id", using: :btree
    t.index ["is_deleted"], name: "index_report_calendars_on_is_deleted", using: :btree
    t.index ["report_id"], name: "index_report_calendars_on_report_id", using: :btree
  end

  create_table "reports", force: :cascade do |t|
    t.integer  "organization_id"
    t.string   "name"
    t.boolean  "is_deleted",      default: false
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
    t.index ["is_deleted"], name: "index_reports_on_is_deleted", using: :btree
    t.index ["organization_id"], name: "index_reports_on_organization_id", using: :btree
  end

  create_table "snapshots", force: :cascade do |t|
    t.string   "name"
    t.integer  "calendar_id"
    t.boolean  "is_deleted",  default: false
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
    t.integer  "style_id"
    t.string   "notes"
    t.integer  "user_id"
    t.boolean  "pending"
    t.index ["calendar_id"], name: "index_snapshots_on_calendar_id", using: :btree
    t.index ["is_deleted"], name: "index_snapshots_on_is_deleted", using: :btree
    t.index ["style_id"], name: "index_snapshots_on_style_id", using: :btree
  end

  create_table "styles", force: :cascade do |t|
    t.string   "fill_color",      default: "#FFFFFF"
    t.string   "outline_color",   default: "#FFFFFF"
    t.string   "font",            default: "Default"
    t.integer  "font_size",       default: 12
    t.string   "font_style",      default: "normal"
    t.string   "font_weight",     default: "normal"
    t.string   "text_alignment",  default: "left"
    t.string   "text_decoration", default: "none"
    t.integer  "calendar_id"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.integer  "snapshot_id"
    t.string   "text_color",      default: "#000000"
    t.index ["calendar_id"], name: "index_styles_on_calendar_id", using: :btree
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "",    null: false
    t.string   "encrypted_password",     default: "",    null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,     null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email"
    t.integer  "failed_attempts",        default: 0,     null: false
    t.string   "unlock_token"
    t.datetime "locked_at"
    t.datetime "created_at",                             null: false
    t.datetime "updated_at",                             null: false
    t.boolean  "is_super_admin",         default: false
    t.boolean  "is_deleted",             default: false
    t.string   "name",                   default: ""
    t.index ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true, using: :btree
    t.index ["email"], name: "index_users_on_email", unique: true, using: :btree
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
    t.index ["unlock_token"], name: "index_users_on_unlock_token", unique: true, using: :btree
  end

  add_foreign_key "oauth_access_grants", "oauth_applications", column: "application_id"
  add_foreign_key "oauth_access_tokens", "oauth_applications", column: "application_id"
end
