Rails.application.routes.draw do
  use_doorkeeper
  mount FilmCal::Base => '/'

  get 'welcome/status'
  root "welcome#status"

  post 'email_responses/bounce' => 'email_responses#bounce'
  post 'email_responses/complaint' => 'email_responses#complaint'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
