Raven.configure do |config|
  config.sanitize_fields = Rails.application.config.filter_parameters.map(&:to_s)
  config.dsn = 'https://1cafe16f66ea4c7e9e236a7712bb6b90:6b8bae2ef7874e7a885a7329f597afb8@sentry.io/98290'
  config.environments = ['staging', 'production']
end
