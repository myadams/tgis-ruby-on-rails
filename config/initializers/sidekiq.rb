Sidekiq.configure_client do |config|  
  config.redis = { size: 2, url: ENV["REDIS_URL"] }
end