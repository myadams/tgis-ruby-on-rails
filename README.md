# Film Calendar API

## Build Status

* Master: [ ![Codeship Status for SumoCreations/film-calendar-api](https://codeship.com/projects/05eb8700-5a9c-0134-b54a-72b5ec1597e9/status?branch=master)](https://codeship.com/projects/173197)
* Development: [ ![Codeship Status for SumoCreations/film-calendar-api](https://codeship.com/projects/05eb8700-5a9c-0134-b54a-72b5ec1597e9/status?branch=develop)](https://codeship.com/projects/173197)

## Setup

Get your system up and running! This is a standard ruby on rails 5 API application that makes use of:

* Devise (User Authentication)
* Doorkeeper (OAuth)

To get started run the following:

```
bundle install
cp .env.example .env
bundle exec  rake db:setup
rspec
```
## Contributing

We're utilizing gitflow for this project. If you're unfamiliar with gitflow please see this doc from Atlassian:
http://atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow

Essentially all development is merged onto the 'develop' branch. The 'master' branch is reserved for merges that are tagged as releases.

## Deployment

### Staging

We're using continuous integration provided by Codeship. To deploy to staging simply push any commits to the develop branch.

### Production

Deployments to production go out in a similar manner whenever a release is pushed to the master branch.

## Documentation

TODO: Add information about documentation.
